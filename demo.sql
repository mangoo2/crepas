-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 12-11-2020 a las 12:07:59
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `democrepas`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE  PROCEDURE `SP_ADD_PERSONAL` (IN `_NOM` VARCHAR(120), IN `_SEX` INT(1))  NO SQL
INSERT INTO personal (nombre,sexo)VALUES (_NOM,_SEX)$$

CREATE  PROCEDURE `SP_DEL_PERSONAL` (IN `_ID` INT)  NO SQL
UPDATE personal SET estatus=0 where personalId=_ID$$

CREATE  PROCEDURE `SP_GET_ALL_ESTADOS` ()  NO SQL
SELECT * FROM Estado$$

CREATE  PROCEDURE `SP_GET_ALL_PERFILES` ()  NO SQL
SELECT *  FROM Perfiles$$

CREATE  PROCEDURE `SP_GET_ALL_PERSONAL` ()  NO SQL
SELECT per.personalId,per.nombre,per.apellidos,usu.Usuario 
FROM personal as per 
LEFT JOIN usuarios as usu on usu.personalId=per.personalId
WHERE per.tipo=1 AND per.estatus=1$$

CREATE  PROCEDURE `SP_GET_MENUS` (IN `PERFIL` INT)  NO SQL
SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, Perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId=PERFIL$$

CREATE  PROCEDURE `SP_GET_PERSONAL` (IN `_ID` INT)  NO SQL
SELECT *  FROM personal where personalId=_ID$$

CREATE  PROCEDURE `SP_GET_SESSION` (IN `USUA` VARCHAR(10))  NO SQL
SELECT usu.UsuarioID,per.personalId,per.nombre, usu.perfilId, usu.contrasena FROM usuarios as usu,personal as per where per.personalId = usu.personalId and usu.Usuario = USUA$$

CREATE  PROCEDURE `SP_GET_USUARIOS` ()  NO SQL
SELECT usu.UsuarioID,usu.Usuario,per.nombre as perfil FROM usuarios as usu, Perfiles as per WHERE usu.perfilId=per.perfilId$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `categoriaId` int(11) NOT NULL,
  `categoria` varchar(200) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`categoriaId`, `categoria`, `activo`) VALUES
(1, 'Muebles', 1),
(2, 'ssss', 0),
(3, 's', 0),
(4, 'ARTICULOS DE BAÑO', 1),
(5, 'LAMPARAS', 1),
(6, 'ARTESANIAS', 1),
(7, 'CHAROLAS', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `ClientesId` int(11) NOT NULL,
  `Nom` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Calle` varchar(45) DEFAULT NULL,
  `noExterior` varchar(45) DEFAULT NULL,
  `Colonia` varchar(45) DEFAULT NULL,
  `Localidad` varchar(45) DEFAULT NULL,
  `Municipio` varchar(45) DEFAULT NULL,
  `Estado` varchar(45) DEFAULT NULL,
  `Pais` varchar(45) DEFAULT NULL,
  `CodigoPostal` varchar(45) DEFAULT NULL,
  `Correo` varchar(100) NOT NULL,
  `noInterior` varchar(45) NOT NULL,
  `nombrec` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `correoc` varchar(45) NOT NULL,
  `telefonoc` varchar(30) NOT NULL,
  `extencionc` varchar(20) NOT NULL,
  `nextelc` varchar(30) NOT NULL,
  `descripcionc` text NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1' COMMENT '1 activo 0 eliminado'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`ClientesId`, `Nom`, `Calle`, `noExterior`, `Colonia`, `Localidad`, `Municipio`, `Estado`, `Pais`, `CodigoPostal`, `Correo`, `noInterior`, `nombrec`, `correoc`, `telefonoc`, `extencionc`, `nextelc`, `descripcionc`, `activo`) VALUES
(45, 'PUBLICO EN GENERAL', '', '', '', '', '', '', 'Mexico', '', '', '', '', '', '', '', '', '', 1),
(46, 'DULCIBOTANASsx', 'GUADALAJARA', '', 'INDEPENDENCIA', 'PUEBLA', 'PUEBLA', 'PUEBLA', 'Mexico', '72150', 'edreimagdiel@gmail.com', '', 'EDREI', 'edreimagdiel@gmail.com', '2225464434', '', '', '', 1),
(47, 'xxxxxxxx11', 'calle', '1', 'colonia', 'localidad', 'municipio', 'estado', 'mexico', '94140', 'ddd@h.com', '1', 'contacto', 'ccc', '111111111', '11', '222222', 'des', 1),
(48, 'ñkñkñk', 'k', '0', 'k', 'k', 'k', 'k', 'México', '94140', 'cas@hot.com', '9', 'n', 'cas@h.com', '123456789', '123', '123456', 'fghjk', 1),
(49, 'hectoe lagunes loyo', 'conocida', '9', 'conocida', 'conocida', 'conocido', 'veracruz', 'México', '94140', 'lagunes@hotmal.com', '9', 'yo', 'lagunes@hotmal.com', '12356789', '34', '2345678', 'sdñokfsñldkfsd', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE `compras` (
  `id_compra` int(11) NOT NULL,
  `id_proveedor` int(11) NOT NULL,
  `monto_total` double NOT NULL,
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `compras`
--

INSERT INTO `compras` (`id_compra`, `id_proveedor`, `monto_total`, `reg`) VALUES
(1, 23, 300, '2018-05-08 05:55:06'),
(2, 23, 1200, '2018-05-10 16:59:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compra_detalle`
--

CREATE TABLE `compra_detalle` (
  `id_detalle_compra` int(11) NOT NULL,
  `id_compra` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` float NOT NULL,
  `precio_compra` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `compra_detalle`
--

INSERT INTO `compra_detalle` (`id_detalle_compra`, `id_compra`, `id_producto`, `cantidad`, `precio_compra`) VALUES
(1, 1, 63, 1, 300),
(2, 2, 63, 4, 300);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE `estado` (
  `EstadoId` int(11) NOT NULL,
  `Nombre` varchar(50) DEFAULT NULL,
  `Alias` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`EstadoId`, `Nombre`, `Alias`) VALUES
(1, 'AGUASCALIENTES', 'AS'),
(2, 'BAJA CALIFORNIA', 'BC'),
(3, 'BAJA CALIFORNIA SUR', 'BS'),
(4, 'CAMPECHE', 'CC'),
(5, 'COAHUILA', 'CL'),
(6, 'COLIMA', 'CM'),
(7, 'CHIAPAS', 'CS'),
(8, 'CHIHUAHUA', 'CH'),
(9, 'DISTRITO FEDERAL', 'DF'),
(10, 'DURANGO', 'DG'),
(11, 'GUANAJUATO', 'GT'),
(12, 'GUERRERO', 'GR'),
(13, 'HIDALGO', 'HG'),
(14, 'JALISCO', 'JC'),
(15, 'ESTADO DE MEXICO', 'MC'),
(16, 'MICHOACAN', 'MN'),
(17, 'MORELOS', ''),
(18, 'NAYARIT', 'NT'),
(19, 'NUEVO LEON', 'NL'),
(20, 'OAXACA', 'OC'),
(21, 'PUEBLA', 'PL'),
(22, 'QUERETARO', 'QT'),
(23, 'QUINTANA ROO', 'QR'),
(24, 'SAN LUIS POTOSI', 'SP'),
(25, 'SINALOA', 'SL'),
(26, 'SONORA', 'SR'),
(27, 'TABASCO', 'TC'),
(28, 'TAMAULIPAS', 'TS'),
(29, 'TLAXCALA', 'TL'),
(30, 'VERACRUZ', 'VZ'),
(31, 'YUCATAN', 'YN'),
(32, 'ZACATECAS', 'ZS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `insumos`
--

CREATE TABLE `insumos` (
  `insumosId` int(11) NOT NULL,
  `insumo` varchar(300) NOT NULL,
  `existencia` float NOT NULL,
  `operaciones` varchar(300) DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `insumos`
--

INSERT INTO `insumos` (`insumosId`, `insumo`, `existencia`, `operaciones`, `activo`, `reg`) VALUES
(1, 'Queso Filadelfia', 4500, NULL, 1, '2020-11-10 23:58:02'),
(2, 'Nutella', 2500, NULL, 1, '2020-11-10 23:58:02'),
(3, 'Fresa', 1200, NULL, 1, '2020-11-11 00:03:13'),
(4, 'Salami', 2000, NULL, 1, '2020-11-11 00:03:13'),
(5, 'Jamón', 2000, NULL, 1, '2020-11-11 00:03:13'),
(6, 'Cajata', 2000, NULL, 1, '2020-11-11 00:03:13'),
(7, 'Lechera', 1600, NULL, 1, '2020-11-11 00:03:13'),
(8, 'Moras azules', 800, NULL, 1, '2020-11-11 00:03:13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
  `MenuId` int(11) NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL,
  `Icon` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`MenuId`, `Nombre`, `Icon`) VALUES
(1, 'Catálogos', 'fa fa-book'),
(2, 'Operaciones', 'fa fa-folder-open'),
(3, 'Configuración\n', 'fa fa fa-cogs');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu_sub`
--

CREATE TABLE `menu_sub` (
  `MenusubId` int(11) NOT NULL,
  `MenuId` int(11) NOT NULL,
  `Nombre` varchar(50) DEFAULT NULL,
  `Pagina` varchar(120) DEFAULT NULL,
  `Icon` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menu_sub`
--

INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`) VALUES
(1, 1, 'Productos', 'Productos', 'fa fa-barcode'),
(2, 3, 'Categoria', 'Categoria', 'fa fa-cogs'),
(3, 1, 'Personal', 'Personal', 'fa fa-group'),
(4, 2, 'Ventas', 'Ventas', 'fa fa-shopping-cart'),
(5, 2, 'Compras', 'Compras', 'fa fa-cogs'),
(6, 1, 'Clientes', 'Clientes', 'fa fa-user'),
(7, 1, 'Proveedores', 'Proveedores', 'fa fa-truck'),
(8, 2, 'Corte de caja', 'Corte_caja', 'fa fa-cogs'),
(9, 2, 'Lista de ventas', 'ListaVentas', 'fa fa-cogs'),
(10, 2, 'Turno', 'Turno', 'fa fa-cogs'),
(11, 2, 'Lista de turnos', 'ListaTurnos', 'fa fa-cogs'),
(12, 2, 'Lista de compras', 'Listacompras', 'fa fa-shopping-cart'),
(13, 3, 'Config. de ticket', 'Config_ticket', 'fa fa-cogs'),
(14, 1, 'Insumos', 'Insumos', 'fa fa-user');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notas`
--

CREATE TABLE `notas` (
  `id_nota` int(1) NOT NULL,
  `mensaje` text NOT NULL,
  `usuario` varchar(120) NOT NULL,
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `notas`
--

INSERT INTO `notas` (`id_nota`, `mensaje`, `usuario`, `reg`) VALUES
(1, '<p>notass</p>\n', 'Administrador', '2018-05-10 19:01:36');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfiles`
--

CREATE TABLE `perfiles` (
  `perfilId` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `perfiles`
--

INSERT INTO `perfiles` (`perfilId`, `nombre`) VALUES
(1, 'Administrador'),
(2, 'Personal'),
(3, 'Residentes');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfiles_detalles`
--

CREATE TABLE `perfiles_detalles` (
  `Perfil_detalleId` int(11) NOT NULL,
  `perfilId` int(11) NOT NULL,
  `MenusubId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `perfiles_detalles`
--

INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 2, 4),
(7, 2, 5),
(8, 1, 14);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal`
--

CREATE TABLE `personal` (
  `personalId` int(11) NOT NULL,
  `nombre` varchar(300) NOT NULL,
  `apellidos` varchar(300) NOT NULL,
  `fechanacimiento` date NOT NULL,
  `sexo` int(1) NOT NULL,
  `domicilio` varchar(500) NOT NULL,
  `ciudad` varchar(120) NOT NULL,
  `estado` int(11) NOT NULL,
  `codigopostal` int(5) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `celular` varchar(15) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `turno` int(1) NOT NULL,
  `fechaingreso` date NOT NULL,
  `fechabaja` date NOT NULL,
  `sueldo` decimal(10,2) NOT NULL,
  `tipo` int(1) NOT NULL DEFAULT '1' COMMENT '0 administrador 1 normal',
  `estatus` int(1) NOT NULL DEFAULT '1' COMMENT '1 visible 0 eliminado'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `personal`
--

INSERT INTO `personal` (`personalId`, `nombre`, `apellidos`, `fechanacimiento`, `sexo`, `domicilio`, `ciudad`, `estado`, `codigopostal`, `telefono`, `celular`, `correo`, `turno`, `fechaingreso`, `fechabaja`, `sueldo`, `tipo`, `estatus`) VALUES
(1, 'Administrador', '', '0000-00-00', 0, '', '', 0, 0, '', '', '', 0, '0000-00-00', '0000-00-00', '0.00', 0, 1),
(2, 'gerardo', 'bautista', '2018-05-02', 1, 'conocido', 'conocido', 30, 94140, '123456789', '1234567890', 'soporte@mangoo.mx', 3, '2018-05-03', '2018-05-17', '30000.00', 1, 1),
(3, '_NOM', '', '0000-00-00', 0, '', '', 0, 0, '', '', '', 0, '0000-00-00', '0000-00-00', '0.00', 1, 0),
(4, 'Mariana gutierrez', '', '0000-00-00', 2, '', '', 0, 0, '', '', '', 0, '0000-00-00', '0000-00-00', '0.00', 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal_menu`
--

CREATE TABLE `personal_menu` (
  `personalmenuId` int(11) NOT NULL,
  `personalId` int(11) NOT NULL,
  `MenuId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `personal_menu`
--

INSERT INTO `personal_menu` (`personalmenuId`, `personalId`, `MenuId`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 1, 8),
(9, 1, 9),
(10, 1, 10),
(11, 1, 11),
(12, 1, 12),
(13, 1, 13),
(17, 2, 4),
(18, 1, 14);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `productoid` int(11) NOT NULL,
  `codigo` varchar(20) NOT NULL,
  `nombre` varchar(120) NOT NULL,
  `stockok` tinyint(1) NOT NULL DEFAULT '0',
  `stock` float NOT NULL DEFAULT '0',
  `stockmin` float NOT NULL DEFAULT '0',
  `preciocompra` float NOT NULL DEFAULT '0',
  `preciopos` float NOT NULL DEFAULT '0',
  `preciouber` float NOT NULL DEFAULT '0',
  `preciorappi` float NOT NULL DEFAULT '0',
  `preciodid` float NOT NULL DEFAULT '0',
  `img` varchar(120) DEFAULT NULL,
  `activo` int(1) NOT NULL DEFAULT '1' COMMENT '1 actual 0 eliminado',
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`productoid`, `codigo`, `nombre`, `stockok`, `stock`, `stockmin`, `preciocompra`, `preciopos`, `preciouber`, `preciorappi`, `preciodid`, `img`, `activo`, `reg`) VALUES
(4, '3333331', '3331', 0, -69, 0, 301, 337.12, 0, 0, 0, 'public/img/productos/180511-130538pro.png', 0, '2018-05-03 20:04:32'),
(58, '333222222', '33332', 0, 1, 0, 3323, 3655.3, 0, 0, 0, 'public/img/productos/180508-123544pro.png', 1, '2018-05-03 20:05:32'),
(59, '333222222', '33332', 0, 0, 0, 3323, 3655.3, 0, 0, 0, '', 1, '2018-05-03 20:05:46'),
(60, '333222222', '33332', 0, -8, 0, 3323, 3655.3, 0, 0, 0, '', 1, '2018-05-03 20:06:42'),
(61, '333222222', '33332', 0, 2, 0, 3323, 3655.3, 0, 0, 0, '', 0, '2018-05-03 20:06:50'),
(62, '333222222', '33332', 0, 2, 0, 3323, 3655.3, 0, 0, 0, 'public/img/productos/180503-150748pro.jpeg', 1, '2018-05-03 20:07:47'),
(63, '132914187493', 'producto3', 0, 0, 2, 300, 345, 0, 0, 0, 'public/img/productos/180503-151310pro.png', 1, '2018-05-03 20:13:10'),
(64, '5131147160001', 'KLENNERAS RECTANGULAR 13.5X26X7 CM ', 0, 1, 0, 0, 160, 0, 0, 0, '', 1, '2018-05-14 16:11:56'),
(65, '5131147160002', 'KLENNERA CUADRADA 16X16X16 CM', 0, 10, 0, 0, 155, 0, 0, 0, '', 1, '2018-05-14 16:11:56'),
(66, '5131147160003', 'PAPELERO 20X20X23', 0, 8, 0, 0, 450, 0, 0, 0, '', 1, '2018-05-14 16:11:56'),
(67, '5131147160004', 'CEPILLERO RECTANGULAR  5X15X8 CM', 0, 5, 0, 0, 130, 0, 0, 0, '', 1, '2018-05-14 16:11:56'),
(68, '5131147160005', 'CEPILLERO RECTANGULAR CON PERFORACION PARA PASTA 8X15X8 CM', 0, 1, 0, 0, 145, 0, 0, 0, '', 0, '2018-05-14 16:11:56'),
(69, '5131147160006', 'JABONERA RECTANGULAR 15X8X2', 0, 7, 0, 0, 55, 0, 0, 0, '', 1, '2018-05-14 16:11:56'),
(70, '5131147160007', 'JABONERA RECTANGULAR 13X8X2 CM', 0, 5, 0, 0, 48, 0, 0, 0, '', 1, '2018-05-14 16:11:56'),
(71, '5131147160008', 'ALGODONERO 12X8', 0, 2, 0, 0, 120, 0, 0, 0, '', 1, '2018-05-14 16:11:56'),
(72, '5131147160009', 'Q`TIPS CON PERILLA 12X8 CM', 0, 3, 0, 0, 120, 0, 0, 0, '', 1, '2018-05-14 16:11:57'),
(73, '5131147160010', 'VASO JAIBOLERO', 0, 2, 0, 0, 75, 0, 0, 0, '', 1, '2018-05-14 16:11:57'),
(74, '5131147160011', 'ALGODONERO 12X6 CM', 0, 2, 0, 0, 130, 0, 0, 0, '', 1, '2018-05-14 16:11:57'),
(75, '5131147160012', 'CHAROLA RECTANGULAR 15X25X3', 0, 3, 0, 0, 125, 0, 0, 0, '', 1, '2018-05-14 16:11:57'),
(76, '5131147160013', 'CAJA RECTANGULAR 6X10X6 CM', 0, 2, 0, 0, 95, 0, 0, 0, '', 1, '2018-05-14 16:11:57'),
(77, '5131147160014', 'DESPACHADORES CON ATOMIZADOR PLASTICO OVALADO 16 CM', 0, 1, 0, 0, 110, 0, 0, 0, '', 1, '2018-05-14 16:11:57'),
(78, '5131147160015', 'DESPACHADOR CON ATOMIZADOR PLASTICO 13 CM', 0, 2, 0, 0, 105, 0, 0, 0, '', 1, '2018-05-14 16:11:57'),
(79, '5131147160016', 'DESPACHADOR REDONDO CON ATOMIZADOR PLASTICO 11 CM', 0, 1, 0, 0, 90, 0, 0, 0, '', 1, '2018-05-14 16:11:57'),
(80, '5131147160017', 'DESPACHADOR CON ATOMIZADOR PLASTICO OVALADO RECTANGULAR 6X10X11 CM', 0, 1, 0, 0, 110, 0, 0, 0, '', 0, '2018-05-14 16:11:57'),
(81, '5131147160018', 'DESPACHADOR CON ATOMIZADOR PLASTICO PLASTICO CUADRADO 7X7X12 CM', 0, 1, 0, 0, 90, 0, 0, 0, '', 1, '2018-05-14 16:11:57'),
(82, '5131147160019', 'DESPACHADOR CON ATOMIZADOR PLASTICO ESFERA 8 CM', 0, 1, 0, 0, 90, 0, 0, 0, '', 1, '2018-05-14 16:11:57'),
(83, '5131147160020', 'LAMPARA COLGAR CILINDRO 40X12 CM', 0, 2, 0, 0, 250, 0, 0, 0, '', 1, '2018-05-14 16:11:57'),
(84, '5131147160021', 'LAMPARA COLGAR CILINDRO 30X14 CM', 0, 2, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:57'),
(85, '5131147160022', 'LAMPARA COLGAR CILINDRO 20X12 CM', 0, 2, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:57'),
(86, '5131147160023', 'LAMPARA CUBO 40X12X12', 0, 2, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:57'),
(87, '5131147160024', 'LAMPARA COLGAR CILINDRO 30X8 CM', 0, 2, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:57'),
(88, '5131147160025', 'LAMPARA COLGAR CILINDRO 25X10 CM BOCA RUSTICA ', 0, 1, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:57'),
(89, '5131147160026', 'LAMPARA COLGAR CILINDRO 30X12 CM BOCA RUSTICA', 0, 2, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:58'),
(90, '5131147160027', 'LAMPARA COLGAR CILINDRO 40X14 CM BOCA RUSTICA ', 0, 2, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:58'),
(91, '5131147160028', 'LAMPARA DE COLGAR CILINDRO 40X10', 0, 4, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:58'),
(92, '5131147160029', 'LAMPARA DE COLGAR CILINDRO 40X14', 0, 4, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:58'),
(93, '5131147160030', 'LAMPARA DE COLGAR CILINDRO 50X10', 0, 1, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:58'),
(94, '5131147160031', 'LAMPARA DE COLGAR CILINDRO 45X10', 0, 1, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:58'),
(95, '5131147160032', 'LAMPARA DE COLGAR CUBO 50X20X20', 0, 2, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:58'),
(96, '5131147160033', 'LAMPARA DE COLGAR CILINDRO 40X20', 0, 2, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:58'),
(97, '5131147160034', 'LAMPARA HONGO 15 CM', 0, 4, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:58'),
(98, '5131147160035', 'LAMPARA FLOR 12 CM', 0, 14, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:58'),
(99, '5131147160036', 'LAMPARA FLOR 10 CM', 0, 4, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:58'),
(100, '5131147160037', 'LAMPARA HONGO 12', 0, 1, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:58'),
(101, '5131147160038', 'LAMPARA HONGO 10 CM', 0, 2, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:58'),
(102, '5131147160039', 'LAMPARA HONGO 9 CM', 0, 3, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:58'),
(103, '5131147160040', 'LAMPARA ANGEL 20 CM', 0, 10, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:58'),
(104, '5131147160041', 'LAMPARA NAGEK 17 CM', 0, 9, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:58'),
(105, '5131147160042', 'LAMPARA ANGEL 15 CM', 0, 15, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:58'),
(106, '5131147160043', 'LAMPARA ANGEL 13 CM', 0, 5, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:58'),
(107, '5131147160044', 'LAMPARA RECTANGULAR 10X20X33', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:58'),
(108, '5131147160045', 'LAMPARA CILINDRO 12X30', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:59'),
(109, '5131147160046', 'LAMPARA ANGEL 35', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:59'),
(110, '5131147160047', 'LAMPARA CILINDRO ONIX SERPENTINA 12X43 BOCA RUSTICA', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:59'),
(111, '5131147160048', 'LAMPARA CILINDRO 14X30 CM', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:59'),
(112, '5131147160049', 'LAMPARA CILINDRO 20X35 CM', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:59'),
(113, '5131147160050', 'LAMPARA CILINDRO 12X50 CM', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:59'),
(114, '5131147160051', 'LAMPARA CARACOL 35 CM', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:59'),
(115, '5131147160052', 'LAMPARA CUBO 15X15X40 CM', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:59'),
(116, '5131147160053', 'LAMPARA CILINDRO 14X50 CM', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:59'),
(117, '5131147160054', 'LAMPARA CUBO 20X20X70 CM', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:59'),
(118, '5131147160055', 'LAMPARA CUBO 15X15X50 CM', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:59'),
(119, '5131147160056', 'LAMPARA CILINDRO 20X50 CM', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:59'),
(120, '5131147160057', 'LAMPARA CILINDRO 14X60 CM', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:59'),
(121, '5131147160058', 'LAMPARA CILINDRO 14X40 CM', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:59'),
(122, '5131147160059', 'LAMPARA CUBO 20X20X40 CM', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:11:59'),
(123, '5131147160060', 'LAMPARA ELIPTICA 13x25x35 CM ', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:12:00'),
(124, '5131147160061', 'LAMPARA CILINDRO 12X40 CM', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:12:00'),
(125, '5131147160062', 'ROSTRO DE PERFIL 30 CM', 0, 2, 0, 0, 180, 0, 0, 0, '', 1, '2018-05-14 16:12:00'),
(126, '5131147160063', 'ROSTRO DE PERFIL 16 CM', 0, 19, 0, 0, 58, 0, 0, 0, '', 1, '2018-05-14 16:12:00'),
(127, '5131147160064', 'ANGEL FINO 9 CM', 0, 7, 0, 0, 28, 0, 0, 0, '', 1, '2018-05-14 16:12:00'),
(128, '5131147160065', 'ANGEL FINO 7 CM', 0, 6, 0, 0, 26, 0, 0, 0, '', 1, '2018-05-14 16:12:00'),
(129, '5131147160066', 'ANGEL PERA 9 CM', 0, 33, 0, 0, 38, 0, 0, 0, '', 1, '2018-05-14 16:12:00'),
(130, '5131147160067', 'ANGEL PERA 12 CM', 0, 15, 0, 0, 74, 0, 0, 0, '', 1, '2018-05-14 16:12:00'),
(131, '5131147160068', 'ANGEL PERA 14 CM', 0, 10, 0, 0, 88, 0, 0, 0, '', 1, '2018-05-14 16:12:00'),
(132, '5131147160069', 'ALHAJERI CON PALOMA 7X5X3 CM', 0, 31, 0, 0, 30, 0, 0, 0, '', 1, '2018-05-14 16:12:00'),
(133, '5131147160070', 'ALHAJERO LISO 5X4X3 CM', 0, 14, 0, 0, 18, 0, 0, 0, '', 1, '2018-05-14 16:12:00'),
(134, '5131147160071', 'ALHAJERO CON PERILLA 7X5X3 CM', 0, 24, 0, 0, 30, 0, 0, 0, '', 1, '2018-05-14 16:12:00'),
(135, '5131147160072', 'ALHAJERO CON PALOMA 10X7X4 CM ', 0, 7, 0, 0, 34, 0, 0, 0, '', 1, '2018-05-14 16:12:00'),
(136, '5131147160073', 'ALHAJERO CON PERILLA 12X8X4 CM', 0, 3, 0, 0, 42, 0, 0, 0, '', 1, '2018-05-14 16:12:01'),
(137, '5131147160074', 'ALHAJERO CON PALOMAS 12X8X4 CM', 0, 20, 0, 0, 42, 0, 0, 0, '', 1, '2018-05-14 16:12:01'),
(138, '5131147160075', 'ALHAJERO FLOR PASTILLEROS 6X3 CM', 0, 9, 0, 0, 27, 0, 0, 0, '', 1, '2018-05-14 16:12:01'),
(139, '5131147160076', 'ANGEL CONO 10 CM', 0, 34, 0, 0, 32, 0, 0, 0, '', 1, '2018-05-14 16:12:01'),
(140, '5131147160077', 'PALOMAS ENAMORADAS 11 CM', 0, 2, 0, 0, 48, 0, 0, 0, '', 1, '2018-05-14 16:12:01'),
(141, '5131147160078', 'PALOMAS ENAMORADAS 14 CM', 0, 16, 0, 0, 54, 0, 0, 0, '', 1, '2018-05-14 16:12:01'),
(142, '5131147160079', 'PALOMAS ENAMORADAS 20 CM ', 0, 3, 0, 0, 68, 0, 0, 0, '', 1, '2018-05-14 16:12:01'),
(143, '5131147160080', 'IMAGEN MODERNISTA 12 CM', 0, 16, 0, 0, 28, 0, 0, 0, '', 1, '2018-05-14 16:12:01'),
(144, '5131147160081', 'IMAGEN MODERNISTA 15 CM', 0, 28, 0, 0, 40, 0, 0, 0, '', 1, '2018-05-14 16:12:01'),
(145, '5131147160082', 'IMAGEN MODERNISTA 21 CM', 0, 42, 0, 0, 52, 0, 0, 0, '', 1, '2018-05-14 16:12:01'),
(146, '5131147160083', 'IMAGEN MODERNISTA 27 CM', 0, 9, 0, 0, 64, 0, 0, 0, '', 1, '2018-05-14 16:12:01'),
(147, '5131147160084', 'ROSTRO DE PERFIL FINO 9 CM', 0, 4, 0, 0, 34, 0, 0, 0, '', 1, '2018-05-14 16:12:01'),
(148, '5131147160085', 'MADONA FINA DE PERFIL 9 CM', 0, 5, 0, 0, 34, 0, 0, 0, '', 1, '2018-05-14 16:12:01'),
(149, '5131147160086', 'MADONA FINA DE PERFIL 13 CM', 0, 1, 0, 0, 45, 0, 0, 0, '', 1, '2018-05-14 16:12:01'),
(150, '5131147160087', 'MADONA SEMIFINA 20 CM', 0, 9, 0, 0, 120, 0, 0, 0, '', 1, '2018-05-14 16:12:01'),
(151, '5131147160088', 'CRUZ 18 CM', 0, 9, 0, 0, 50, 0, 0, 0, '', 1, '2018-05-14 16:12:01'),
(152, '5131147160089', 'CRUZ 15 CM', 0, 2, 0, 0, 40, 0, 0, 0, '', 1, '2018-05-14 16:12:02'),
(153, '5131147160090', 'CABALLO CON BASE FINO 25 CM', 0, 2, 0, 0, 315, 0, 0, 0, '', 1, '2018-05-14 16:12:02'),
(154, '5131147160091', 'CABALLO CON BASE FINO 40 CM', 0, 1, 0, 0, 790, 0, 0, 0, '', 1, '2018-05-14 16:12:02'),
(155, '5131147160092', 'AGUILA FINA 20 CM', 0, 4, 0, 0, 275, 0, 0, 0, '', 1, '2018-05-14 16:12:02'),
(156, '5131147160093', 'AGUILA FINA 30 CM', 0, 4, 0, 0, 490, 0, 0, 0, '', 1, '2018-05-14 16:12:02'),
(157, '5131147160094', 'AGUILA FINA 12 CM', 0, 4, 0, 0, 95, 0, 0, 0, '', 1, '2018-05-14 16:12:02'),
(158, '5131147160095', 'AGUILA FINA 15 CM', 0, 2, 0, 0, 145, 0, 0, 0, '', 1, '2018-05-14 16:12:02'),
(159, '5131147160096', 'FAMILIA MODERNISTA 13 CM', 0, 2, 0, 0, 370, 0, 0, 0, '', 1, '2018-05-14 16:12:02'),
(160, '5131147160097', 'CAROCAL MODERNISTA 28 CM', 0, 2, 0, 0, 310, 0, 0, 0, '', 1, '2018-05-14 16:12:02'),
(161, '5131147160098', 'TORO Nº 40 CM', 0, 4, 0, 0, 490, 0, 0, 0, '', 1, '2018-05-14 16:12:02'),
(162, '5131147160099', 'RINOCERONTE 20 CM', 0, 2, 0, 0, 260, 0, 0, 0, '', 1, '2018-05-14 16:12:02'),
(163, '5131147160100', 'ELEFANTE ESPECIAL #4', 0, 1, 0, 0, 490, 0, 0, 0, '', 1, '2018-05-14 16:12:02'),
(164, '5131147160101', 'ELEFANTE SEMIFINO 8 CM', 0, 7, 0, 0, 15, 0, 0, 0, '', 1, '2018-05-14 16:12:02'),
(165, '5131147160102', 'ELEFANTE SEMIFINO 16 CM', 0, 4, 0, 0, 28, 0, 0, 0, '', 1, '2018-05-14 16:12:02'),
(166, '5131147160103', 'ELEFANTE SEMIFINO 13 CM', 0, 7, 0, 0, 25, 0, 0, 0, '', 1, '2018-05-14 16:12:02'),
(167, '5131147160104', 'ELEFANTE SEMIFINO 12 CM', 0, 13, 0, 0, 22, 0, 0, 0, '', 1, '2018-05-14 16:12:02'),
(168, '5131147160105', 'ELEFANTE SEMIFINO 10 CM', 0, 8, 0, 0, 18, 0, 0, 0, '', 1, '2018-05-14 16:12:02'),
(169, '5131147160106', 'ALHAJERO RECTANGULAR 14X10X5 CM', 0, 2, 0, 0, 95, 0, 0, 0, '', 1, '2018-05-14 16:12:02'),
(170, '5131147160107', 'ALHAJERO CUADRADO 8X8X5 CM', 0, 4, 0, 0, 46, 0, 0, 0, '', 1, '2018-05-14 16:12:03'),
(171, '5131147160108', 'ALHAJERO RECTANGULAR 6X10X5 CM', 0, 2, 0, 0, 55, 0, 0, 0, '', 1, '2018-05-14 16:12:03'),
(172, '5131147160109', 'TORTUGA GRANDE', 0, 6, 0, 0, 38, 0, 0, 0, '', 1, '2018-05-14 16:12:03'),
(173, '5131147160110', 'TORTUGA MEDIANA', 0, 11, 0, 0, 25, 0, 0, 0, '', 1, '2018-05-14 16:12:03'),
(174, '5131147160111', 'TORTUGA CHICA ', 0, 6, 0, 0, 18, 0, 0, 0, '', 1, '2018-05-14 16:12:03'),
(175, '5131147160112', 'PANTERA Nº 20', 0, 2, 0, 0, 98, 0, 0, 0, '', 1, '2018-05-14 16:12:03'),
(176, '5131147160113', 'PANTERA Nº 15', 0, 4, 0, 0, 67, 0, 0, 0, '', 1, '2018-05-14 16:12:03'),
(177, '5131147160114', 'PANTERA Nº 12', 0, 1, 0, 0, 56, 0, 0, 0, '', 1, '2018-05-14 16:12:03'),
(178, '5131147160115', 'PORTAVASOS ONIX', 0, 5, 0, 0, 70, 0, 0, 0, '', 1, '2018-05-14 16:12:03'),
(179, '5131147160116', 'CANDELERO ROSA', 0, 4, 0, 0, 40, 0, 0, 0, '', 1, '2018-05-14 16:12:03'),
(180, '5131147160117', 'FIGURAS PINTADAS MINI', 0, 5, 0, 0, 22, 0, 0, 0, '', 1, '2018-05-14 16:12:03'),
(181, '5131147160118', 'FIGURAS PINTADAS CHCICAS', 0, 3, 0, 0, 35, 0, 0, 0, '', 1, '2018-05-14 16:12:03'),
(182, '5131147160119', 'FIGURAS PINTADAS MEDIANA', 0, 10, 0, 0, 48, 0, 0, 0, '', 1, '2018-05-14 16:12:03'),
(183, '5131147160120', 'FIGURAS PINTADA GRANDE', 0, 8, 0, 0, 95, 0, 0, 0, '', 1, '2018-05-14 16:12:03'),
(184, '5131147160121', 'PIRAMIDE 26', 0, 2, 0, 0, 90, 0, 0, 0, '', 1, '2018-05-14 16:12:03'),
(185, '5131147160122', 'PIRAMIDE 22', 0, 1, 0, 0, 60, 0, 0, 0, '', 1, '2018-05-14 16:12:03'),
(186, '5131147160123', 'PORTALAPIZ CUBO 6X6X6 CM', 0, 2, 0, 0, 35, 0, 0, 0, '', 1, '2018-05-14 16:12:03'),
(187, '5131147160124', 'PORTALAPIZ CUBO 7X7X7 CM', 0, 3, 0, 0, 45, 0, 0, 0, '', 1, '2018-05-14 16:12:03'),
(188, '5131147160125', 'PORTAVELA ESTRELLA 20 CM', 0, 2, 0, 0, 90, 0, 0, 0, '', 1, '2018-05-14 16:12:03'),
(189, '5131147160126', 'ESTRELLA DE MAR 20 CM', 0, 9, 0, 0, 60, 0, 0, 0, '', 1, '2018-05-14 16:12:04'),
(190, '5131147160127', 'PIRAMIDE HUECA Nº 9', 0, 4, 0, 0, 49, 0, 0, 0, '', 1, '2018-05-14 16:12:04'),
(191, '5131147160128', 'PIRAMIDE HUECA Nº 15', 0, 5, 0, 0, 94, 0, 0, 0, '', 1, '2018-05-14 16:12:04'),
(192, '5131147160129', 'PIRAMIDE HUECA Nº 22', 0, 3, 0, 0, 165, 0, 0, 0, '', 1, '2018-05-14 16:12:04'),
(193, '5131147160130', 'BASE CON 4 MARIPOSAS', 0, 2, 0, 0, 290, 0, 0, 0, '', 1, '2018-05-14 16:12:04'),
(194, '5131147160131', 'CABALLO ELEGANTE Nº 20', 0, 2, 0, 0, 110, 0, 0, 0, '', 1, '2018-05-14 16:12:04'),
(195, '5131147160132', 'CABALLO ELEGANTE Nº 25', 0, 1, 0, 0, 135, 0, 0, 0, '', 1, '2018-05-14 16:12:04'),
(196, '5131147160133', 'CONEJO GRANDE', 0, 16, 0, 0, 30, 0, 0, 0, '', 1, '2018-05-14 16:12:04'),
(197, '5131147160134', 'CONEJO CHICO', 0, 1, 0, 0, 18, 0, 0, 0, '', 1, '2018-05-14 16:12:04'),
(198, '5131147160135', 'ELEFANTE CON PORTALAPIZ ', 0, 2, 0, 0, 28, 0, 0, 0, '', 1, '2018-05-14 16:12:04'),
(199, '5131147160136', 'CARRETAS CON CABALLO CHICAS ', 0, 2, 0, 0, 58, 0, 0, 0, '', 1, '2018-05-14 16:12:04'),
(200, '5131147160137', 'HONGO 8 CM', 0, 28, 0, 0, 20, 0, 0, 0, '', 1, '2018-05-14 16:12:04'),
(201, '5131147160138', 'HONGO 9 CM', 0, 36, 0, 0, 25, 0, 0, 0, '', 1, '2018-05-14 16:12:04'),
(202, '5131147160139', 'HONGO 10 CM', 0, 19, 0, 0, 30, 0, 0, 0, '', 1, '2018-05-14 16:12:04'),
(203, '5131147160140', 'PERRO SALCHICHA GRANDE ', 0, 8, 0, 0, 32, 0, 0, 0, '', 1, '2018-05-14 16:12:04'),
(204, '5131147160141', 'PERRO SALCHIHCA MEDIANO', 0, 14, 0, 0, 28, 0, 0, 0, '', 1, '2018-05-14 16:12:04'),
(205, '5131147160142', 'PERRO SALCHICHA CHICO ', 0, 25, 0, 0, 18, 0, 0, 0, '', 1, '2018-05-14 16:12:04'),
(206, '5131147160143', 'PERRO BOXER CHICO', 0, 14, 0, 0, 18, 0, 0, 0, '', 1, '2018-05-14 16:12:04'),
(207, '5131147160144', 'PERRO BOXER GRANDE', 0, 10, 0, 0, 32, 0, 0, 0, '', 1, '2018-05-14 16:12:04'),
(208, '5131147160145', 'COCHINO GRANDE', 0, 20, 0, 0, 34, 0, 0, 0, '', 1, '2018-05-14 16:12:04'),
(209, '5131147160146', 'COCHINO MEDIANO', 0, 16, 0, 0, 22, 0, 0, 0, '', 1, '2018-05-14 16:12:04'),
(210, '5131147160147', 'GATO MECHE Nº 15', 0, 3, 0, 0, 73, 0, 0, 0, '', 1, '2018-05-14 16:12:04'),
(211, '5131147160148', 'GATO MECHE Nº 12', 0, 8, 0, 0, 55, 0, 0, 0, '', 1, '2018-05-14 16:12:04'),
(212, '5131147160149', 'GATO MECHE Nº 7', 0, 4, 0, 0, 29, 0, 0, 0, '', 1, '2018-05-14 16:12:05'),
(213, '5131147160150', 'ARMADILLO GRANDE', 0, 12, 0, 0, 38, 0, 0, 0, '', 1, '2018-05-14 16:12:05'),
(214, '5131147160151', 'ARMADILLO CHICO', 0, 12, 0, 0, 18, 0, 0, 0, '', 1, '2018-05-14 16:12:05'),
(215, '5131147160152', 'DELFIN CHICO', 0, 16, 0, 0, 18, 0, 0, 0, '', 1, '2018-05-14 16:12:05'),
(216, '5131147160153', 'COCHINO CHICO', 0, 4, 0, 0, 18, 0, 0, 0, '', 1, '2018-05-14 16:12:05'),
(217, '5131147160154', 'RANAS', 0, 4, 0, 0, 12, 0, 0, 0, '', 1, '2018-05-14 16:12:05'),
(218, '5131147160155', 'PATOS', 0, 17, 0, 0, 28, 0, 0, 0, '', 1, '2018-05-14 16:12:05'),
(219, '5131147160156', 'CABALLOS CHICOS', 0, 34, 0, 0, 20, 0, 0, 0, '', 1, '2018-05-14 16:12:05'),
(220, '5131147160157', 'GATOS CHICOS', 0, 16, 0, 0, 28, 0, 0, 0, '', 1, '2018-05-14 16:12:05'),
(221, '5131147160158', 'GATOS MECHE CHICOS', 0, 5, 0, 0, 29, 0, 0, 0, '', 1, '2018-05-14 16:12:05'),
(222, '5131147160159', 'UNICORNIO CHICO', 0, 28, 0, 0, 20, 0, 0, 0, '', 1, '2018-05-14 16:12:05'),
(223, '5131147160160', 'OSOS', 0, 4, 0, 0, 10, 0, 0, 0, '', 1, '2018-05-14 16:12:05'),
(224, '5131147160161', 'PANTERA Nº 40', 0, 1, 0, 0, 290, 0, 0, 0, '', 1, '2018-05-14 16:12:05'),
(225, '5131147160162', 'PANTERA Nº 30', 0, 2, 0, 0, 175, 0, 0, 0, '', 1, '2018-05-14 16:12:05'),
(226, '5131147160163', 'PANTERA Nº 25', 0, 1, 0, 0, 120, 0, 0, 0, '', 1, '2018-05-14 16:12:05'),
(227, '5131147160164', 'MUELA GRANDE', 0, 1, 0, 0, 60, 0, 0, 0, '', 1, '2018-05-14 16:12:05'),
(228, '5131147160165', 'MUELA MEDIANA', 0, 4, 0, 0, 45, 0, 0, 0, '', 1, '2018-05-14 16:12:05'),
(229, '5131147160166', 'MUELA CHICA', 0, 14, 0, 0, 35, 0, 0, 0, '', 1, '2018-05-14 16:12:05'),
(230, '5131147160167', 'ELEFANTE ESPECIAL #5', 0, 5, 0, 0, 660, 0, 0, 0, '', 1, '2018-05-14 16:12:05'),
(231, '5131147160168', 'DAMA CHINA CHICA', 0, 2, 0, 0, 125, 0, 0, 0, '', 1, '2018-05-14 16:12:05'),
(232, '5131147160169', 'DAMA CHINA MEDIANA', 0, 2, 0, 0, 160, 0, 0, 0, '', 1, '2018-05-14 16:12:05'),
(233, '5131147160170', 'DAMA CHINA GRANDE', 0, 2, 0, 0, 235, 0, 0, 0, '', 1, '2018-05-14 16:12:06'),
(234, '5131147160171', 'AJEDREZ INGLES 34X34', 0, 3, 0, 0, 310, 0, 0, 0, '', 1, '2018-05-14 16:12:06'),
(235, '5131147160172', 'AJEDREZ INGLES 27X27', 0, 2, 0, 0, 260, 0, 0, 0, '', 1, '2018-05-14 16:12:06'),
(236, '5131147160173', 'AJEDREZ INGLES 20X20', 0, 3, 0, 0, 210, 0, 0, 0, '', 1, '2018-05-14 16:12:06'),
(237, '5131147160174', 'DOMINO MINIATURA', 0, 28, 0, 0, 78, 0, 0, 0, '', 1, '2018-05-14 16:12:06'),
(238, '5131147160175', 'DOMINO CHICO', 0, 10, 0, 0, 85, 0, 0, 0, '', 1, '2018-05-14 16:12:06'),
(239, '5131147160176', 'DOMINO MEDIANO', 0, 12, 0, 0, 105, 0, 0, 0, '', 1, '2018-05-14 16:12:06'),
(240, '5131147160177', 'DOMINO GRANDE', 0, 1, 0, 0, 125, 0, 0, 0, '', 1, '2018-05-14 16:12:06'),
(241, '5131147160178', 'HERRADURAS DE TEQUILERO ', 0, 2, 0, 0, 350, 0, 0, 0, '', 1, '2018-05-14 16:12:06'),
(242, '5131147160179', 'CENICERO REDONDO 12X3 CM', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:14:59'),
(243, '5131147160180', 'CENICERO REDONDO 15X3 CM', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:14:59'),
(244, '5131147160181', 'CENICERO CUADRADO 12X12X3 CM', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:14:59'),
(245, '5131147160182', 'CHAROLA CONICA CUADRADA ', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:14:59'),
(246, '5131147160183', 'CHAROLA PAÑUELO RECTANGULAR 30X10 CM', 0, 1, 0, 0, 75, 0, 0, 0, '', 1, '2018-05-14 16:14:59'),
(247, '5131147160184', 'CHAROLA COCINA RECTANGULAR 25X7 CM ', 0, 6, 0, 0, 60, 0, 0, 0, '', 1, '2018-05-14 16:14:59'),
(248, '5131147160185', 'CHAROLA ONDEADA 15X15 ', 0, 2, 0, 0, 120, 0, 0, 0, '', 1, '2018-05-14 16:14:59'),
(249, '5131147160186', 'CHAROLA PAÑUELO MARMOL 7X7 CM', 0, 18, 0, 0, 12, 0, 0, 0, '', 1, '2018-05-14 16:14:59'),
(250, '5131147160187', 'CHAROLA PAÑUELO ONIX 7X7 CM', 0, 3, 0, 0, 22, 0, 0, 0, '', 1, '2018-05-14 16:14:59'),
(251, '5131147160188', 'CHOROLAS CUADRADAS 10X10 CM MARMOL', 0, 13, 0, 0, 22, 0, 0, 0, '', 1, '2018-05-14 16:14:59'),
(252, '5131147160189', 'CHAROLA CONICA 30X6 CM', 0, 5, 0, 0, 85, 0, 0, 0, '', 1, '2018-05-14 16:14:59'),
(253, '5131147160190', 'CHAROLA PEPITA 20X12 CM', 0, 1, 0, 0, 120, 0, 0, 0, '', 1, '2018-05-14 16:15:00'),
(254, '5131147160191', 'CHAROLA CONICA RECTANGULAR 25X 6 CM', 0, 2, 0, 0, 60, 0, 0, 0, '', 1, '2018-05-14 16:15:00'),
(255, '5131147160192', 'CHAROLA CANOA PICOS ', 0, 2, 0, 0, 395, 0, 0, 0, '', 1, '2018-05-14 16:15:00'),
(256, '5131147160193', 'CHAROLA CON 3 DIVISIONES 45X15 CM', 0, 3, 0, 0, 330, 0, 0, 0, '', 1, '2018-05-14 16:15:00'),
(257, '5131147160194', 'CHAROLA MARIPOSA BICOLOR', 0, 1, 0, 0, 790, 0, 0, 0, '', 1, '2018-05-14 16:15:00'),
(258, '5131147160195', 'CHAROLA TIPO HOJA 40X20 CM', 0, 2, 0, 0, 550, 0, 0, 0, '', 1, '2018-05-14 16:15:00'),
(259, '5131147160196', 'CHAROLA CONOA CON CORTE 30X12 CM', 0, 2, 0, 0, 210, 0, 0, 0, '', 1, '2018-05-14 16:15:00'),
(260, '5131147160197', 'CHAROLA PEPITA 24X12 CM ', 0, 5, 0, 0, 120, 0, 0, 0, '', 1, '2018-05-14 16:15:00'),
(261, '5131147160198', 'CHAROLA PAÑUELO MARMOL 12X12 CM', 0, 3, 0, 0, 28, 0, 0, 0, '', 1, '2018-05-14 16:15:00'),
(262, '5131147160199', 'CHAROLA PAÑUELO 0NIX 15X15 CM', 0, 2, 0, 0, 60, 0, 0, 0, '', 1, '2018-05-14 16:15:00'),
(263, '5131147160200', 'CHAROLA PAÑUELO ONIX 25X10 CM ', 0, 3, 0, 0, 120, 0, 0, 0, '', 1, '2018-05-14 16:15:00'),
(264, '5131147160201', 'CHAROLA PAÑUELO MARMOL 10X10 CM', 0, 6, 0, 0, 45, 0, 0, 0, '', 1, '2018-05-14 16:15:01'),
(265, '5131147160202', 'CHAROLA PAÑUELO MARMOL 20X20 CM', 0, 2, 0, 0, 60, 0, 0, 0, '', 1, '2018-05-14 16:15:01'),
(266, '5131147160203', 'CHAROLA CONICA ', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:15:01'),
(267, '5131147160204', 'GAJO DE SANDIA 30X20 ', 0, 1, 0, 0, 245, 0, 0, 0, '', 1, '2018-05-14 16:15:01'),
(268, '5131147160205', 'GAJO DE SANDIA 20X15 ', 0, 1, 0, 0, 140, 0, 0, 0, '', 1, '2018-05-14 16:15:01'),
(269, '5131147160206', 'BOWL ARROZ 12X6 CM', 0, -1, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:15:01'),
(270, '5131147160207', 'BOWL RECTO 15X11 CM', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:15:01'),
(271, '5131147160208', 'BOWL RECTO 30X23X13', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:15:01'),
(272, '5131147160209', 'BOWL PICOS 40X23 CM', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:15:01'),
(273, '5131147160210', 'BOWL RECTO 20X14', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:15:01'),
(274, '5131147160211', 'BOWL PICOS 15X11', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:15:02'),
(275, '5131147160212', 'BOWL ARROZ 10X5 CM', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, '2018-05-14 16:15:02'),
(276, '5131147160213', 'BOWL CON CORTE 13X10 CM', 0, 0, 0, 0, 0, 0, 0, 0, '', 0, '2018-05-14 16:15:02'),
(277, '39319181010102', '345345', 1, 10, 0, 10, 10, 10, 10, 10, NULL, 1, '2020-11-11 01:04:10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_insumos`
--

CREATE TABLE `productos_insumos` (
  `insumoId` int(11) NOT NULL,
  `productoId` int(11) NOT NULL,
  `cantidad` float NOT NULL DEFAULT '0',
  `insumo` int(11) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `productos_insumos`
--

INSERT INTO `productos_insumos` (`insumoId`, `productoId`, `cantidad`, `insumo`, `activo`) VALUES
(1, 277, 10, 1, 1),
(2, 277, 10, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE `proveedores` (
  `id_proveedor` int(11) NOT NULL,
  `razon_social` varchar(50) NOT NULL,
  `domicilio` varchar(30) CHARACTER SET utf8 NOT NULL COMMENT 'calle',
  `ciudad` varchar(50) CHARACTER SET utf8 NOT NULL,
  `cp` varchar(8) CHARACTER SET utf8 NOT NULL,
  `id_estado` int(11) NOT NULL,
  `telefono_local` varchar(10) NOT NULL,
  `telefono_celular` varchar(10) NOT NULL,
  `contacto` varchar(100) NOT NULL,
  `email_contacto` varchar(60) NOT NULL,
  `rfc` varchar(13) NOT NULL,
  `fax` varchar(20) NOT NULL,
  `obser` text NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proveedores`
--

INSERT INTO `proveedores` (`id_proveedor`, `razon_social`, `domicilio`, `ciudad`, `cp`, `id_estado`, `telefono_local`, `telefono_celular`, `contacto`, `email_contacto`, `rfc`, `fax`, `obser`, `activo`) VALUES
(23, 'BIMBO S.A DE C.V', 'AAA', 'AAA', '12345', 21, '123', '123', 'AAA', 'AAA', 'HERD890308UAA', '', '', 1),
(24, 'DULCERIA SUSY S.A DE C.V. SUCURSAL 3', '104 PTE NO 1720-A LOC 10 COL. ', 'PUEBLA', '', 21, '', '2227535616', 'HECTOR', '', 'DSU910312LSO', '', 'asdasdasdasdass', 1),
(25, 'laknsldknaklsdnlkasd', '', '', '', 0, '', '', '', '', '', '', '', 1),
(26, 'razon social2', 'domicilio2', 'ciudad2', '94140', 30, '2345678', '123456789', 'contacto', 'email', 'rfc', '123456789', 'obser', 0),
(27, 'razon', 'domi', 'ciudad', '94140', 30, '2345678', '23456789', 'contacto', 'email', 'rfc', '345678', 'obser', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ticket`
--

CREATE TABLE `ticket` (
  `id_ticket` int(11) NOT NULL,
  `titulo` text CHARACTER SET utf8 NOT NULL,
  `mensaje` text CHARACTER SET utf8 NOT NULL,
  `mensaje2` text CHARACTER SET utf8 NOT NULL,
  `fuente` int(1) NOT NULL,
  `tamano` int(2) NOT NULL,
  `margensup` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ticket`
--

INSERT INTO `ticket` (`id_ticket`, `titulo`, `mensaje`, `mensaje2`, `fuente`, `tamano`, `margensup`) VALUES
(1, 'LAURA FASHION', '¡GRACIAS POR SU COMPRA!', 'ESTE TICKET NO ES UNA REPRESENTACIÓN FISCAL', 2, 9, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `turno`
--

CREATE TABLE `turno` (
  `id` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `horaa` time NOT NULL,
  `fechacierre` date NOT NULL,
  `horac` time NOT NULL,
  `cantidad` float NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `user` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `turno`
--

INSERT INTO `turno` (`id`, `fecha`, `horaa`, `fechacierre`, `horac`, `cantidad`, `nombre`, `status`, `user`) VALUES
(59, '2016-10-14', '00:37:05', '0000-00-00', '00:37:45', 100, 'x', 'cerrado', 'user'),
(60, '2016-10-14', '00:39:37', '0000-00-00', '06:11:06', 1, 'b', 'cerrado', 'user'),
(61, '2016-10-24', '00:38:48', '0000-00-00', '00:40:57', 100, 'dia', 'cerrado', 'user'),
(62, '2016-10-24', '00:43:15', '0000-00-00', '11:03:17', 100, '100', 'cerrado', 'user'),
(63, '2017-05-19', '11:03:33', '0000-00-00', '20:00:22', 100, 'aaa', 'cerrado', 'user'),
(64, '2018-01-10', '20:02:43', '0000-00-00', '20:11:01', 0, '', 'cerrado', 'user'),
(65, '2018-01-10', '20:19:13', '0000-00-00', '09:20:55', 0, '', 'cerrado', 'user'),
(66, '2018-01-15', '09:21:24', '0000-00-00', '00:00:00', 0, 'ESMERLDA', 'abierto', 'user'),
(67, '2018-01-15', '09:21:24', '0000-00-00', '00:00:00', 0, 'ESMERLDA', 'abierto', 'user'),
(68, '2018-01-15', '09:21:27', '0000-00-00', '10:01:26', 0, 'ESMERLDA', 'cerrado', 'user'),
(69, '2018-01-15', '10:01:28', '0000-00-00', '00:00:00', 0, '', 'abierto', 'user'),
(70, '2018-01-15', '10:01:43', '0000-00-00', '14:10:45', 500, 'ESMERALDA', 'cerrado', 'user'),
(71, '2018-01-15', '14:11:09', '0000-00-00', '13:49:39', 600, 'marijo', 'cerrado', 'user'),
(72, '2018-01-16', '13:49:46', '0000-00-00', '00:00:00', 0, '', 'abierto', 'user'),
(73, '2018-01-16', '13:50:01', '0000-00-00', '00:00:00', 500, 'MARIJO', 'abierto', 'user'),
(74, '2018-01-16', '13:50:11', '0000-00-00', '13:45:17', 500, 'MARIJO', 'cerrado', 'user'),
(75, '2018-01-17', '13:45:36', '0000-00-00', '14:03:13', 0, '', 'cerrado', 'user'),
(76, '2018-01-17', '14:03:34', '0000-00-00', '14:48:36', 500, 'MARI JOSE', 'cerrado', 'user'),
(77, '2018-01-19', '14:48:47', '0000-00-00', '18:19:52', 500, 'marijo', 'cerrado', 'user'),
(78, '2018-01-22', '18:24:30', '0000-00-00', '08:39:02', 500, 'marijo', 'cerrado', 'user'),
(79, '2018-01-23', '08:39:13', '0000-00-00', '00:00:00', 0, '', 'abierto', 'user'),
(80, '2018-01-23', '08:39:27', '0000-00-00', '14:02:30', 500, 'ESMERALDA', 'cerrado', 'user'),
(81, '2018-01-23', '14:02:40', '0000-00-00', '00:00:00', 0, '', 'abierto', 'user'),
(82, '2018-01-23', '14:02:49', '0000-00-00', '09:41:46', 0, 'MARIJO', 'cerrado', 'user'),
(83, '2018-01-24', '09:44:21', '0000-00-00', '14:04:56', 500, 'ESME', 'cerrado', 'user'),
(84, '2018-01-24', '14:05:03', '0000-00-00', '14:05:20', 0, '', 'cerrado', 'user'),
(85, '2018-01-24', '14:05:30', '0000-00-00', '00:00:00', 0, '', 'abierto', 'user'),
(86, '2018-01-24', '14:05:55', '0000-00-00', '00:00:00', 0, '', 'abierto', 'user'),
(87, '2018-01-24', '14:06:08', '0000-00-00', '14:07:24', 0, 'MARIJO', 'cerrado', 'user'),
(88, '2018-01-24', '14:07:32', '0000-00-00', '00:00:00', 0, '', 'abierto', 'user'),
(89, '2018-01-24', '14:07:44', '0000-00-00', '07:30:20', 500, 'MARIJO', 'cerrado', 'user'),
(90, '2018-01-25', '07:30:32', '0000-00-00', '00:00:00', 500, '', 'abierto', 'user'),
(91, '2018-01-25', '07:30:39', '0000-00-00', '14:10:39', 500, 'esmeralda', 'cerrado', 'user'),
(92, '2018-01-25', '14:10:55', '0000-00-00', '00:00:00', 500, 'MARIJO', 'abierto', 'user'),
(93, '2018-01-25', '14:10:58', '0000-00-00', '07:42:52', 500, 'MARIJO', 'cerrado', 'user'),
(94, '2018-01-26', '07:43:03', '0000-00-00', '11:23:32', 500, 'esme', 'cerrado', 'user'),
(95, '2018-05-08', '12:00:16', '2018-05-11', '17:58:24', 400, 'prueba', 'cerrado', 'user'),
(96, '2018-05-15', '13:29:48', '0000-00-00', '00:00:00', 100, 'xxx', 'abierto', 'user');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `UsuarioID` int(11) NOT NULL,
  `perfilId` int(11) NOT NULL,
  `personalId` int(11) NOT NULL,
  `Usuario` varchar(45) DEFAULT NULL,
  `contrasena` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`UsuarioID`, `perfilId`, `personalId`, `Usuario`, `contrasena`) VALUES
(1, 1, 1, 'admin', '$2y$10$.yBpfsTTEmhYhECki.qy3eL45XaLo3MinuIPgiPsjmXBYItZXHUga'),
(2, 2, 2, 'gerardo', '$2y$10$XE359fjs0SS/bB.L8ogiwefN8we7nm5ZfCDEGqC6gNNZy/06MTPlS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `id_venta` int(11) NOT NULL,
  `id_personal` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `metodo` int(1) NOT NULL COMMENT '1 efectivo, 2 credito, 3 debito',
  `subtotal` double NOT NULL,
  `descuento` double NOT NULL COMMENT '0 % 5 % 7%',
  `descuentocant` double NOT NULL,
  `monto_total` double NOT NULL,
  `pagotarjeta` float NOT NULL,
  `efectivo` float NOT NULL,
  `cancelado` int(11) NOT NULL,
  `hcancelacion` date NOT NULL,
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`id_venta`, `id_personal`, `id_cliente`, `metodo`, `subtotal`, `descuento`, `descuentocant`, `monto_total`, `pagotarjeta`, `efectivo`, `cancelado`, `hcancelacion`, `reg`) VALUES
(1, 1, 45, 1, 1035, 0, 0, 1035, 0, 0, 1, '2018-05-09', '2018-05-07 20:54:14'),
(2, 1, 45, 1, 3430, 0.05, 171.5, 3258.5, 0, 0, 1, '2018-05-15', '2018-05-08 21:29:25'),
(3, 1, 45, 1, 3430, 0.05, 171.5, 3258.5, 0, 0, 1, '2018-05-15', '2018-05-08 21:29:35'),
(4, 1, 45, 1, 7310.6, 0, 0, 7310.6, 0, 0, 0, '0000-00-00', '2018-05-08 23:13:49'),
(5, 1, 45, 1, 690, 0, 0, 690, 0, 0, 0, '0000-00-00', '2018-05-11 17:42:29'),
(6, 1, 45, 1, 116969.6, 0, 0, 116969.6, 0, 0, 0, '0000-00-00', '2018-05-11 21:59:27'),
(7, 1, 45, 1, 690, 0, 0, 690, 0, 0, 0, '0000-00-00', '2018-05-11 22:00:17'),
(8, 1, 45, 1, 1380, 0, 0, 1380, 0, 0, 0, '0000-00-00', '2018-05-11 22:02:06'),
(9, 1, 45, 1, 33759, 0, 0, 33759, 0, 0, 0, '0000-00-00', '2018-05-11 22:03:33'),
(10, 1, 45, 1, 67086.88, 0, 0, 67086.88, 0, 0, 0, '0000-00-00', '2018-05-11 22:06:02'),
(11, 1, 45, 1, 345, 0, 0, 345, 0, 0, 0, '0000-00-00', '2018-05-15 18:55:25'),
(12, 1, 45, 1, 345, 0, 0, 345, 0, 0, 0, '0000-00-00', '2018-05-15 19:04:57'),
(13, 1, 45, 1, 345, 0, 0, 345, 0, 0, 0, '0000-00-00', '2018-05-15 19:07:17'),
(14, 1, 45, 1, 345, 0, 0, 345, 0, 0, 0, '0000-00-00', '2018-05-17 21:06:54'),
(15, 1, 45, 1, 6138, 0, 0, 6138, 5000, 1138, 0, '0000-00-00', '2018-05-24 17:28:45'),
(16, 1, 45, 1, 0, 0, 0, 0, 0, 0, 0, '0000-00-00', '2020-11-04 16:06:47');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta_detalle`
--

CREATE TABLE `venta_detalle` (
  `id_detalle_venta` int(11) NOT NULL,
  `id_venta` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` float NOT NULL,
  `precio` decimal(5,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `venta_detalle`
--

INSERT INTO `venta_detalle` (`id_detalle_venta`, `id_venta`, `id_producto`, `cantidad`, `precio`) VALUES
(1, 1, 63, 3, '345.00'),
(2, 2, 63, 10, '343.00'),
(3, 3, 63, 10, '343.00'),
(4, 4, 59, 2, '999.99'),
(5, 5, 63, 2, '345.00'),
(6, 6, 63, 22, '999.99'),
(7, 6, 60, 10, '999.99'),
(8, 7, 63, 2, '345.00'),
(9, 8, 58, 1, '345.00'),
(10, 8, 63, 3, '345.00'),
(11, 9, 63, 99, '341.00'),
(12, 10, 63, 99, '337.12'),
(13, 10, 4, 100, '337.12'),
(14, 11, 63, 1, '345.00'),
(15, 12, 63, 1, '345.00'),
(16, 13, 63, 1, '345.00'),
(17, 14, 63, 1, '345.00'),
(18, 15, 63, 18, '341.00'),
(19, 16, 269, 1, '0.00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`categoriaId`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`ClientesId`);

--
-- Indices de la tabla `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`id_compra`),
  ADD KEY `constraint_fk_04` (`id_proveedor`);

--
-- Indices de la tabla `compra_detalle`
--
ALTER TABLE `compra_detalle`
  ADD PRIMARY KEY (`id_detalle_compra`),
  ADD KEY `constraint_fk_08` (`id_compra`),
  ADD KEY `constraint_fk_09` (`id_producto`);

--
-- Indices de la tabla `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`EstadoId`);

--
-- Indices de la tabla `insumos`
--
ALTER TABLE `insumos`
  ADD PRIMARY KEY (`insumosId`);

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`MenuId`);

--
-- Indices de la tabla `menu_sub`
--
ALTER TABLE `menu_sub`
  ADD PRIMARY KEY (`MenusubId`),
  ADD KEY `fk_menu_sub_menu_idx` (`MenuId`);

--
-- Indices de la tabla `notas`
--
ALTER TABLE `notas`
  ADD PRIMARY KEY (`id_nota`);

--
-- Indices de la tabla `perfiles`
--
ALTER TABLE `perfiles`
  ADD PRIMARY KEY (`perfilId`);

--
-- Indices de la tabla `perfiles_detalles`
--
ALTER TABLE `perfiles_detalles`
  ADD PRIMARY KEY (`Perfil_detalleId`),
  ADD KEY `fk_Perfiles_detalles_Perfiles1_idx` (`perfilId`),
  ADD KEY `fk_Perfiles_detalles_menu_sub1_idx` (`MenusubId`);

--
-- Indices de la tabla `personal`
--
ALTER TABLE `personal`
  ADD PRIMARY KEY (`personalId`);

--
-- Indices de la tabla `personal_menu`
--
ALTER TABLE `personal_menu`
  ADD PRIMARY KEY (`personalmenuId`),
  ADD KEY `personal_fkpersona` (`personalId`),
  ADD KEY `personal_fkmenu` (`MenuId`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`productoid`);

--
-- Indices de la tabla `productos_insumos`
--
ALTER TABLE `productos_insumos`
  ADD PRIMARY KEY (`insumoId`),
  ADD KEY `insumo_fk_producto` (`productoId`),
  ADD KEY `insumo_fk_insumo` (`insumo`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`id_proveedor`),
  ADD KEY `constraint_fk_27` (`id_estado`);

--
-- Indices de la tabla `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`id_ticket`);

--
-- Indices de la tabla `turno`
--
ALTER TABLE `turno`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`UsuarioID`),
  ADD KEY `fk_usuarios_Perfiles1_idx` (`perfilId`),
  ADD KEY `fk_usuarios_personal1_idx` (`personalId`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`id_venta`),
  ADD UNIQUE KEY `id_venta_UNIQUE` (`id_venta`),
  ADD KEY `constraint_fk_30` (`id_cliente`),
  ADD KEY `constraint_fk_31` (`id_personal`);

--
-- Indices de la tabla `venta_detalle`
--
ALTER TABLE `venta_detalle`
  ADD PRIMARY KEY (`id_detalle_venta`),
  ADD UNIQUE KEY `id_detalle_venta_UNIQUE` (`id_detalle_venta`),
  ADD KEY `constraint_fk_15` (`id_producto`),
  ADD KEY `constraint_fk_16` (`id_venta`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `categoriaId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `ClientesId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT de la tabla `compras`
--
ALTER TABLE `compras`
  MODIFY `id_compra` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `compra_detalle`
--
ALTER TABLE `compra_detalle`
  MODIFY `id_detalle_compra` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `estado`
--
ALTER TABLE `estado`
  MODIFY `EstadoId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de la tabla `insumos`
--
ALTER TABLE `insumos`
  MODIFY `insumosId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `menu`
--
ALTER TABLE `menu`
  MODIFY `MenuId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `menu_sub`
--
ALTER TABLE `menu_sub`
  MODIFY `MenusubId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `notas`
--
ALTER TABLE `notas`
  MODIFY `id_nota` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `perfiles`
--
ALTER TABLE `perfiles`
  MODIFY `perfilId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `perfiles_detalles`
--
ALTER TABLE `perfiles_detalles`
  MODIFY `Perfil_detalleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `personal`
--
ALTER TABLE `personal`
  MODIFY `personalId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `personal_menu`
--
ALTER TABLE `personal_menu`
  MODIFY `personalmenuId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `productoid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=278;

--
-- AUTO_INCREMENT de la tabla `productos_insumos`
--
ALTER TABLE `productos_insumos`
  MODIFY `insumoId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  MODIFY `id_proveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de la tabla `ticket`
--
ALTER TABLE `ticket`
  MODIFY `id_ticket` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `turno`
--
ALTER TABLE `turno`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `UsuarioID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `id_venta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `venta_detalle`
--
ALTER TABLE `venta_detalle`
  MODIFY `id_detalle_venta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `compras`
--
ALTER TABLE `compras`
  ADD CONSTRAINT `fk_compra_proveedor` FOREIGN KEY (`id_proveedor`) REFERENCES `proveedores` (`id_proveedor`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `compra_detalle`
--
ALTER TABLE `compra_detalle`
  ADD CONSTRAINT `fk_compra_compra` FOREIGN KEY (`id_compra`) REFERENCES `compras` (`id_compra`),
  ADD CONSTRAINT `fk_compra_producto` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`productoid`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `menu_sub`
--
ALTER TABLE `menu_sub`
  ADD CONSTRAINT `fk_menu_sub_menu` FOREIGN KEY (`MenuId`) REFERENCES `menu` (`MenuId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `perfiles_detalles`
--
ALTER TABLE `perfiles_detalles`
  ADD CONSTRAINT `fk_Perfiles_detalles_Perfiles1` FOREIGN KEY (`perfilId`) REFERENCES `perfiles` (`perfilId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Perfiles_detalles_menu_sub1` FOREIGN KEY (`MenusubId`) REFERENCES `menu_sub` (`MenusubId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `personal_menu`
--
ALTER TABLE `personal_menu`
  ADD CONSTRAINT `personal_fkmenu` FOREIGN KEY (`MenuId`) REFERENCES `menu_sub` (`MenusubId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `personal_fkpersona` FOREIGN KEY (`personalId`) REFERENCES `personal` (`personalId`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `productos_insumos`
--
ALTER TABLE `productos_insumos`
  ADD CONSTRAINT `insumo_fk_insumo` FOREIGN KEY (`insumo`) REFERENCES `insumos` (`insumosId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `insumo_fk_producto` FOREIGN KEY (`productoId`) REFERENCES `productos` (`productoid`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `fk_usuarios_Perfiles1` FOREIGN KEY (`perfilId`) REFERENCES `perfiles` (`perfilId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`personalId`) REFERENCES `personal` (`personalId`);

--
-- Filtros para la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD CONSTRAINT `fk_venta_cliente` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`ClientesId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_venta_presonal` FOREIGN KEY (`id_personal`) REFERENCES `personal` (`personalId`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `venta_detalle`
--
ALTER TABLE `venta_detalle`
  ADD CONSTRAINT `fk_detalle_producto` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`productoid`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_detalle_venta` FOREIGN KEY (`id_venta`) REFERENCES `ventas` (`id_venta`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
