<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=list_insumos".date('YmdGis').".xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
  <table border="1" id="tabla" class="display table table-hover table-striped table-bordered" width="100%">
    <thead>
     	<tr>
     		<th>ID</th>
		    <th>Insumo</th>
		    <th>Existencia (grm/ml/pza)</th>
		    <th>Stock minimo</th>
		    <th>Conteo Fisico</th>
		    <th>Cantidad Compra</th>
		    <th>ID Proveedor</th>
      	</tr>
    </thead>
    <tbody>
    <?php
    	foreach ($insumos->result() as $i) {
    		$stock=$i->existencia;
    		if($bodega==1){
    			$stock=$i->bodega;
    		}
    		//calculo para cantidad a comprar -- (stock_semanal - stock)
    		echo "<tr>
    			<td>".$i->insumosId."</td>
    			<td>".$i->insumo."</td>
    			<td>".$stock."</td>
    			<td>".$i->stockmin."</td>
    			<td></td>
    			<td></td>
    			<td></td>
    		</tr>";
    	}
    ?>
	</tbody>
</table>


