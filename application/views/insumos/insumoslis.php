<div class="row">
  <div class="col-md-12">
    <h2>Insumos</h2>
    <?php //echo $productosp;?>
  </div>

  <div class="col-md-12">
    <div class="col-md-4"></div>

    <div class="col-md-2">
      <a href="<?php echo base_url(); ?>Insumos/exportStocks" class="btn btn-success"><i class="fa fa-file-excel-o"></i></span> Stocks bajos</a>
    </div>

    <div class="col-md-2">
      <a href="<?php echo base_url(); ?>Insumos/insumosadd" class="btn btn-raised gradient-ibiza-sunset white sidebar-shadow"><i class="fa fa-plus"></i></span> Nuevo</a>
    </div>

    <div class="col-md-2">
      <a href="<?php echo base_url(); ?>Insumos/exportExcel" class="btn btn-success"><i class="fa fa-file-excel-o"></i></span> Exportar</a>
    </div>

    <div class="col-md-2">
      <button type="button" id="upload_excel" title="Cargar Plantilla Insumos" class="btn btn-info white pull-right"><i class="fa fa-upload"></i></span> Cargar Plantilla</button>
    </div>
  </div>
</div>
<!--Statistics cards Ends-->

  <!--Line with Area Chart 1 Starts-->
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-header">
            <h4 class="card-title">Listado de insumos</h4>
        </div>
        
        <div class="card-body">
          <div class="card-block">
              <!--------//////////////-------->
            <table class="table table-striped table-responsive" id="data-tables" style="width: 100%">
              <thead>
                <tr>
                  <th></th>
                  <th>Concepto</th>
                  <th>Existencia (gr / ml /pza)</th>
                  <th>Stock mínimo</th>
                  <th></th>
                </tr>
              </thead>
              <tbody id="tbodyresultadospro">

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

<div class="modal fade text-left" id="modalCarga" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Cargar malla de datos</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <div class="row carga_malla">
          <div class="col-md-12">
            <form id="form_carga_malla" method="POST" enctype="multipart/form-data">
              <div class="col-md-8">
                <div class="form-group">
                  <label>Seleccione un cvs</label>
                  <input type="file" name="inputFile" id="inputFile" class="form-control" > 
                </div>
               </div>
              <div class="col-md-6">
                <div class="form-group">
                  <button type="button" id="aceptar_carga" class="btn bg-aqua"><i class="fa fa-upload"></i> Cargar Malla</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
