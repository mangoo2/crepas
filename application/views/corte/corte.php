<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/print.css" media="print">
<div class="row">
    <div class="col-md-12">
      <h2>Corte de caja </h2>
    </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Corte de caja</h4>
        </div>
        <div class="card-body">
            <div class="card-block">
                <!--------//////////////-------->
                <div class="row inputbusquedas">
                    <div class="col-md-12">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-1">Desde:</label>
                                <div class="col-md-2">
                                    <input id="txtInicio" name="txtInicio" class="form-control date-picker" size="16" type="date" />
                                </div>
                                <label class="control-label col-md-1">Hasta:</label>
                                <div class="col-md-2">
                                    <input id="txtFin" name="txtFin" class="form-control date-picker" size="16" type="date"/>
                                </div>
                                <div class="col-md-2">                      
                                    <div class="checkbox-list">
                                        <label><input type="checkbox" id="chkFecha" value="1"> Fecha actual </label>
                                    </div>
                                </div>
   
                                <div class="col-md-2">
                                    <a href="#" class="btn btn-raised gradient-purple-bliss white" id="btnBuscar">Buscar</a>
                                    <a id="btnImprimir" onclick="imprimir();"><button type="button" class="btn btn-raised gradient-purple-bliss white"   ><i class="fa fa-print"></i></button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">Tipo de Costo:</label>
                                <select class="form-control" id="tipo_costo">
                                    <option value="4">Todos</option>
                                    <option value="0">Mostrador</option>
                                    <option value="1">Uber</option>
                                    <option value="2">Rappi</option>
                                    <option value="3">Didi</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">Sucursal:</label>
                                <select class="form-control" id="id_sucursal">
                                    <option value="0">Elije una opción</option>
                                    <option value="1">Mr Crepé 1</option>
                                    <option value="2">Mr Crepé 2</option>
                                    <option value="3">Mr Crepé 3</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row"><br><br></div>
                <div class="row" id="imprimir">
                    <div class="col-md-12" id="tbCorte">
                        
                    </div>
                    <div class="col-md-12" id="tbCorte2">
                        
                    </div>
                    <div class="col-md-12" id="tbProds">
                        
                    </div>
                    <div class="col-md-12" id="tbTop">
                        
                    </div>
                    <div class="col-md-12" id="tbInsumos">
                        
                    </div>
                    <div class="col-md-6">
                        <p style="font-size: 20px">
                            <span class="col-md-6 text-warning">Total efectivo:</span>
                            <span class="col-md-4" >
                                <span class="text-warning">$</span>
                                <span id="tEfectivo">0.00</span>
                            </span>
                        </p>
                        <p style="font-size: 20px">
                            <span class="col-md-6 text-warning">Total tarjeta crédito:</span>
                            <span class="col-md-4" >
                                <span class="text-warning">$</span>
                                <span id="tCredito">0.00</span>
                            </span>
                        </p>
                        <p style="font-size: 20px">
                            <span class="col-md-6 text-warning">Total tarjeta débito:</span>
                            <span class="col-md-4" >
                                <span class="text-warning">$</span>
                                <span id="tDebito">0.00</span>
                            </span>
                        </p>
                        <p style="font-size: 20px">
                            <span class="col-md-6 text-warning">Total Uber:</span>
                            <span class="col-md-4" >
                                <span class="text-warning">$</span>
                                <span id="tUber">0.00</span>
                            </span>
                        </p>
                        <p style="font-size: 20px">
                            <span class="col-md-6 text-warning">Total Rappi:</span>
                            <span class="col-md-4" >
                                <span class="text-warning">$</span>
                                <span id="tRappi">0.00</span>
                            </span>
                        </p>
                        <p style="font-size: 20px">
                            <span class="col-md-6 text-warning">Total Didi (efectivo):</span>
                            <span class="col-md-4" >
                                <span class="text-warning">$</span>
                                <span id="tDidiefe">0.00</span>
                            </span>
                        </p>
                        <p style="font-size: 20px">
                            <span class="col-md-6 text-warning">Total Didi (plataforma):</span>
                            <span class="col-md-4" >
                                <span class="text-warning">$</span>
                                <span id="tDidipla">0.00</span>
                            </span>
                        </p>
                        <p style="font-size: 20px">
                            <span class="col-md-6 text-warning">Total de gastos:</span>
                            <span class="col-md-4" >
                                <span class="text-warning">$</span>
                                <span id="tGastos">0.00</span>
                            </span>
                        </p>
                        <p style="font-size: 19px">
                            <span class="col-md-6 text-warning">TOTAL:</span>
                            <span class="col-md-4" style="border-top: 2px solid #c0c0c0;">
                                <span class="text-warning">$</span>
                                <b>
                                    <span id="dTotal">0.00</span>
                                </b>
                            </span>
                        </p>

                        <p style="font-size: 19px">
                            <span class="col-md-6 text-warning">Total de utilidad:</span>
                            <span class="col-md-4" >
                                <span class="text-warning">$</span>
                                <b>
                                    <span id="totalutilidades">0.00</span>
                                </b>
                            </span>
                        </p>
                        <p style="font-size: 20px">
                            <span class="col-md-6 text-warning">No. de ventas:</span>
                            <span class="col-md-4" style="border-top: 2px solid #c0c0c0;">
                                <span class="text-warning"> </span>
                                <span id="rowventas">0</span>
                            </span>
                        </p>
                    </div>
                </div>
               
                
                <!--------//////////////-------->
            </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
    function imprimir(){
      window.print();
    }
</script>