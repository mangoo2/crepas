<div class="row">
  <div class="col-md-12">
    <h2>Traslados</h2>
    <?php //echo $productosp;?>
  </div>

  <div class="col-md-12">
    <div class="col-md-10"></div>
    <div class="col-md-2">
      <a href="<?php echo base_url(); ?>Traslados/trasladoadd" class="btn btn-raised gradient-ibiza-sunset white sidebar-shadow"><i class="fa fa-plus"></i></span> Nuevo</a>
    </div>
  </div>
</div>
<!--Statistics cards Ends-->

  <!--Line with Area Chart 1 Starts-->
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-header">
            <h4 class="card-title">Listado de Traslados</h4>
        </div>
        
        <div class="card-body">
          <div class="card-block">
              <!--------//////////////-------->
            <table class="table table-striped table-responsive" id="data-tables" style="width: 100%">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Origen</th>
                  <th>Destino</th>
                  <th>Fecha</th>
                  <th></th>
                </tr>
              </thead>
              <tbody id="tbodyresultadospro">

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

<div class="modal fade text-left" id="modaldet" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Detalle del traslado</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <table class="table table-striped table-responsive" id="table_det" style="width: 100%">
              <thead>
                <tr>
                  <th>Insumo</th>
                  <th>Cantidad</th>
                  <th>Origen</th>
                  <th>Destino</th>
                </tr>
              </thead>
              <tbody id="tbodydet">

              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>