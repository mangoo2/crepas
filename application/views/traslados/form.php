<div class="row">
    <div class="col-md-12">
      <h2>Traslado </h2>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Nuevo Traslado</h4>
            </div>
            <div class="card-body">
                <div class="card-block">
                    <div class="row">
                        <form method="post" role="form" id="form_tras">
                            <div class="col-md-12">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">Origen:</label>
                                        <select class="form-control" id="id_origen" name="id_origen">
                                            <option value="0">Elije una opción</option>
                                            <option value="1">Mr Crepé 1</option>
                                            <option value="2">Mr Crepé 2</option>
                                            <option value="3">Mr Crepé 3</option>
                                            <option value="4">Bodega</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">Destino:</label>
                                        <select class="form-control" id="id_destino" name="id_destino">
                                            <option value="0">Elije una opción</option>
                                            <option value="1">Mr Crepé 1</option>
                                            <option value="2">Mr Crepé 2</option>
                                            <option value="3">Mr Crepé 3</option>
                                            <option value="4">Bodega</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" id="name_search"><i class="fa fa-barcode"></i> Insumos:</label>
                                        <select class="form-control" id="id_insumo"></select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label">Cantidad:</label>
                                        <input type="number" id="cantidad" class="form-control" min="1">
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <button style="margin-top: 50%;" type="button" class="btn btn-info" onclick="addproducto()"><i class="fa fa-plus-square" aria-hidden="true"></i></button>
                                    </div>
                                </div> 
                                <div class="col-md-12">
                                    <br>
                                </div>
                            </div>
                        </form>
                        <div class="col-md-12">
                            <table class="table table-hover table-responsive" id="productos">
                                <thead>
                                    <tr>
                                        <th>Producto</th>
                                        <th>Cantidad</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="class_productos">
                                    
                                </tbody>
                                </table>
                            
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-5">
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="btn btn-success" id="save_traslado"><i class="fa fa-save"></i> Guardar Traslado</button>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>