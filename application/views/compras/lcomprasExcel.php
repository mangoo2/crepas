<?php
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=list_compras".date('YmdGis').".xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<table border="1" id="tabla" class="display table table-hover table-striped table-bordered" width="100%">
	<thead>
		<tr>
			<th>#</th>
			<th>Fecha</th>
			<th>Producto</th>
			<th>Proveedor</th>
			<th>Cantidad</th>
			<th>Precio</th>
		</tr>
	</thead>

	<tbody>
		<?php
			foreach ($list->result() as $item) {
				echo '<tr>
								<td>'.$item->id_detalle_compra.'</td>
								<td>'.$item->reg.'</td>
								<td>'.($item->tipo_prod == 0 ? $item->insumo : $item->nombre).'</td>
								<td>'.$item->razon_social.'</td>
								<td>'.$item->cantidad.'</td>
								<td>'."$". number_format($item->precio_compra,2,".",",").'</td>
						</tr>';
			}
		?>
	</tbody>
</table>