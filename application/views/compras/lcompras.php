<div class="row">
  <div class="col-md-12">
    <h2>Compras</h2>
  </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-md-12">
      <div class="card">
          <div class="card-header">
              <h4 class="card-title">Listado de Compras</h4>
          </div>
          <div class="card-body">
              <div class="card-block">
                  <!--------//////////////-------->
                  <div class="col-md-3">
                    <input type="date" id="productoscin" class="form-control">
                  </div>
                  <div class="col-md-3">
                    <input type="date" id="productoscfin" class="form-control">
                  </div>

                  <div class="col-md-2 ml-md-5">
                    <a class="btn btn-raised gradient-purple-bliss white" id="buscarlproducto">Consultar</a>
                  </div>

                  <div class="col-md-2">
                    <a class="btn btn-success white" id="exportlproducto"> <i class="fa fa-file-excel-o"></i></span> Exportar</a>
                  </div>

                  <div class="col-md-12">
                    <table class="table table-striped table-responsive" id="data-tables" style="width: 100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Fecha</th>
                          <th>Producto</th>
                          <th>Proveedor</th>
                          <th>Cantidad</th>
                          <th>Precio</th>
                        </tr>
                      </thead>
                      <tbody class="tbody_lcompras" id="data-tables">
                        <?php /* foreach ($lcompras->result() as $item){
                        //if($item->id_dcp != $item->id_detalle_compra){ ?>
                         <tr id="trcli_<?php echo $item->id_detalle_compra; ?>">
                            <td><?php echo $item->id_detalle_compra; ?></td>
                            <td><?php echo $item->reg; ?></td>
                            <td><?php echo $item->producto; ?></td>
                            <td><?php echo $item->razon_social; ?></td>
                            <td><?php echo $item->cantidad; ?></td>
                            <td><?php echo "$". number_format($item->precio_compra,2,".",","); ?></td>
                          </tr>
                        <?php //}
                         } */ ?>
                      </tbody>
                    </table>
                  </div>
                  <div class="col-md-12">
                    <div class="col-md-9">
                    </div>
                    <div class="col-md-3">
                      <?php /* echo $this->pagination->create_links() */?>
                    </div>
                  </div>
          <!--------//////////////-------->
              </div>
          </div>
      </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){

    table = $('#data-tables').DataTable();
    loadtable();

    $('#buscarlproducto').click(function(){
      loadtable();
      /*
        params = {};
        params.fechain = $('#productoscin').val();
        params.fechafin = $('#productoscfin').val();
        $.ajax({
            type:'POST',
            url: 'Listacompras/consultar',
            data: params,
            async: false,
            success:function(data){
              console.log("DATA:" + data);
              $('.tbody_lcompras').html('');
              $('.tbody_lcompras').html(data);
            }
        });
      */
    });

    $('#exportlproducto').click(function(){
      var fechaIni = $('#productoscin').val();
      var fechaFin = $('#productoscfin').val();

      if (fechaIni == "") {
        fechaIni = 0;
      }
      if (fechaFin == "") {
        fechaFin = 0;
      }

      if ((fechaIni != 0 && fechaFin == 0) || (fechaIni == 0 && fechaFin != 0) || (fechaIni > fechaFin )){
        swal("Error", "Una de las fechas es incorrecta", "warning");
        return;
      }
        window.open("<?php echo base_url(); ?>Listacompras/exportsExcel/"+fechaIni+"/"+fechaFin, "_blank");

    });


    function loadtable() {
      var fechaIni = $('#productoscin').val();
      var fechaFin = $('#productoscfin').val();

      if (fechaIni == "") {
        fechaIni = 0;
      }
      if (fechaFin == "") {
        fechaFin = 0;
      }

      if ((fechaIni != 0 && fechaFin == 0) || (fechaIni == 0 && fechaFin != 0) || (fechaIni > fechaFin )){
        swal("Error", "Una de las fechas es incorrecta", "warning");
        //table.destroy();
        return;
      }

      table.destroy();
      table = $('#data-tables').DataTable({
        stateSave: true,
        //responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
          "url": "Listacompras/getList",
          type: "post",
          data:{
            fechaIni: fechaIni,
            fechaFin: fechaFin
          },
        },
        "columns": [{
            "data": "id_detalle_compra"
          },
          {
            "data": "reg"
          },
          {
            "data": null,
            render: function (row) {
              var html = row.nombre;

              if(row.tipo_prod == 0)
                html = row.insumo;

              return html;
            }
          },
          {
            "data": "razon_social"
          },
          {
            "data": "cantidad"
          },
          {
            "data": "precio_compra"
          },
        ],
        "order": [
          [0, "desc"]
        ],
        "lengthMenu": [
          [10, 25, 50],
          [10, 25, 50]
        ],
      });
    }
});
</script>