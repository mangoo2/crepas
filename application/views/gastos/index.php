<div class="row">
  <div class="col-md-12">
    <div class="col-md-10">
      <h2>Gastos</h2>
    </div>
    <div class="col-md-2">
      <a href="<?php echo base_url(); ?>Gastos/gastoadd" class="btn btn-raised gradient-ibiza-sunset white sidebar-shadow"><i class="fa fa-plus"></i></span> Nuevo</a>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Listado de gastos</h4>
      </div>

      <div class="card-body">
        <div class="card-block">
          <table class="table table-striped table-responsive" id="data-tables" style="width: 100%">
            <thead>
              <tr>
                <th></th>
                <th>Concepto</th>
                <th>Cantidad</th>
                <th>Monto total</th>
                <th>Personal</th>
                <th>Registro</th>
                <th></th>
              </tr>
            </thead>
            <tbody id="tbodyresultadospro">
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>