<style type="text/css">
  .vd_red {
    font-weight: bold;
    color: red;
    margin-bottom: 5px;
  }

  .vd_green {
    color: #009688;
  }
</style>

<div class="row">
  <div class="col-md-12">
    <h2>Gastos</h2>
  </div>
</div>

<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title"><?php echo $label; ?> gasto</h4>
      </div>
      <div class="card-body">
        <div class="card-block ">

          <form method="post" role="form" id="forminsumo">
            <input type="hidden" name="id" id="idGasto" value="<?php if (isset($g)) echo $g->id;else echo "0"; ?>">

            <div class="form-group">
              <label class="col-sm-2 control-label">Motivo:</label>
              <div class="col-sm-10 controls">
                <input type="text" class="form-control form-control-smr" name="motivo" id="motivoG" value="<?php if (isset($g)) echo $g->motivo;else echo ""; ?>">
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label pt-2">Cantidad:</label>
              <div class="col-sm-4 controls pt-2">
                <input type="number" class="form-control form-control-smr" name="cantidad" id="cantidadG" min="1" value="<?php if (isset($g)) echo $g->cantidad;else echo ""; ?>">
              </div>

              <label class="col-sm-2 control-label pt-2">Monto total:</label>
              <div class="col-sm-4 controls pt-2">
                <input type="number" class="form-control form-control-smr" name="monto" id="montoG" placeholder="$" value="<?php if (isset($g)) echo $g->monto;else echo ""; ?>">
              </div>
            </div>
          </form>

          <div class="row col-md-12 pt-2">
            <div class="col-md-12 ">
              <button class="btn btn_registro btn-raised gradient-purple-bliss white shadow-z-1-hover" onclick="add_form()">Guardar</button>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>