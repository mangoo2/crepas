<style type="text/css">
    :root {
        --color-green: #05a8e1;
        --color-secondary: #b9b9b9;
        --color-button: white;
        --color-black: white;
    }


    .switch-button .switch-button__checkbox {
        display: none;
    }
    .switch-button .switch-button__label {
        background-color: var(--color-secondary);
        width: 3rem;
        height: 1.5rem;
        border-radius: 1rem;
        display: inline-block;
        position: relative;
    }
    .switch-button .switch-button__label:before {
        transition: .2s;
        display: block;
        position: absolute;
        width: 1.5rem;
        height: 1.5rem;
        background-color: var(--color-button);
        content: '';
        border-radius: 50%;
        box-shadow: inset 0px 0px 0px 1px var(--color-black);
    }
    .switch-button .switch-button__checkbox:checked + .switch-button__label {
        background-color: #bd1620;
    }
    .switch-button .switch-button__checkbox:checked + .switch-button__label:before {
        transform: translateX(1.5rem);
    }
</style>

<div class="row">
    <div class="col-md-12">
        <div class="col-md-10"><h2>Ventas </h2></div>
        <div class="col-md-2">
            <a class="btn btn-raised gradient-ibiza-sunset white sidebar-shadow" onclick="modal_gastos()"><i class="fa fa-plus"></i></span> Gastos</a>
        </div>
    </div>

</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Ventas de mostrador</h4>
        </div>
        <div class="card-body">
            <div class="card-block">
                <!--------//////////////-------->
                <div class="row">
                    <div class="col-md-12">
                        <input type="checkbox" name="checkimprimir" id="checkimprimir" checked> <label for="checkimprimir"> Imprimir Ticket</label>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Cliente:</label>
                                <div class=" col-md-10">
                                    <select class="form-control" id="vcliente" name="vcliente">
                                        <?php foreach ($clientedefault->result() as $item){ 
                                            echo '<option value="'.$item->ClientesId.'">'.$item->Nom.'</option>';
                                        } 
                                        ?>

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <br>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Cantidad:</label>
                                <div class=" col-md-2">
                                    <input type="number" name="vcantidad" id="vcantidad" value="1" class="form-control" min="0">
                                </div>
                                <label class="col-md-3 control-label"><i class="fa fa-barcode"></i> Producto/Codigo:</label>
                                <div class=" col-md-5">
                                    <select class="form-control" id="vproducto" name="vproducto"></select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <br>
                        </div>
                        <div class="col-md-12">
                            <table class="table table-hover table-responsive" id="productosv">
                                <thead>
                                    <tr>
                                        <th>Clave</th>
                                        <th>Cantidad</th>
                                        <th>Producto</th>
                                        <th>Precio</th>
                                        <th>Total</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="class_productos">
                                    
                                </tbody>
                            </table>
                            <table class="table table-hover table-responsive" id="productos_combo">
                                <thead>
                                    <tr >
                                        <th style="text-align:center;" colspan="6">DETALLES DE COMBO</th>
                                    </tr>
                                    <tr>
                                        <th>Clave</th>
                                        <th>Cantidad</th>
                                        <th>Producto</th>
                                        <th>Precio</th>
                                        <th>Total</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="class_productos_combo">
                                    
                                </tbody>
                            </table>
                            
                        </div>
                        <div class="col-md-12">
                            <a href="#" class="btn btn-raised gradient-green-tea white sidebar-shadow" onclick="addproducto()">Agregar producto</a>
                            <a href="#" class="btn btn-raised gradient-blackberry white sidebar-shadow" onclick="limpiar()">Limpiar</a>
                            
                        </div>  
                    </div>
                    <div class="col-md-3">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Tipo de Costo:</label>
                                <select class="form-control" id="tipo_costo" name="tipo_costo">
                                    <option value="0">Mostrador</option>
                                    <option value="1">Uber</option>
                                    <option value="2">Rappi</option>
                                    <option value="3">Didi</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Método de pago:</label>
                                <select class="form-control" id="mpago" name="mpago">
                                    <option value="1">Efectivo</option>
                                    <option value="2">Tarjeta de crédito</option>
                                    <option value="3">Tarjeta de débito</option>
                                    <option value="4" disabled>Plataforma</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <?php if($this->session->userdata("usuarioid_tz")==1){ ?>
                            <div class="form-group">
                                <label class="control-label">Descuento %:</label>
                                <input type="text" name="mdescuento" id="mdescuento" class="form-control" onchange="calculartotal()" value="0">
                                <input type="hidden" name="cantdescuento" id="cantdescuento" readonly>
                            </div>
                            <?php } else{
                                echo '<input type="hidden" name="mdescuento" id="mdescuento" class="form-control" onchange="calculartotal()" value="0">
                                <input type="hidden" name="cantdescuento" id="cantdescuento" readonly>';
                            } ?>
                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Ingreso Efectivo:</label>
                                <input type="number" name="vingreso" id="vingreso" value="0" class="form-control" oninput="ingreso()">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Ingreso Tarjeta:</label>
                                <input type="number" name="vingresot" id="vingresot" value="0" class="form-control" oninput="ingreso()" disabled>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Total:</label>
                                <input type="number" name="vtotal" id="vtotal" class="form-control" readonly>
                                <input type="hidden" name="vsbtotal" id="vsbtotal" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Cambio:</label>
                                <input type="number" name="vcambio" id="vcambio" class="form-control" readonly value="">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <a href="#" class="btn btn-raised gradient-flickr white sidebar-shadow">Cancelar</a>
                            <a href="#" class="btn btn-raised gradient-green-tea white sidebar-shadow" id="ingresaventa">Ingresar venta</a>
                        </div>
                    </div>
                    
                    
                </div>
        <!--------//////////////-------->
            </div>
        </div>
    </div>
</div>
</div>
<style type="text/css">
    #iframereporte{
        background: white;
    }
    iframe{
        height: 500px;
        border:0;
        width: 100%;
    }
</style>
<div class="modal fade text-left" id="iframeri" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Ticket</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!--<div class="modal-body">-->
            <div id="iframereporte"></div>
            <!--</div>-->
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade text-left" id="modalturno" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="false" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Nuevo Turno</h4>
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>-->
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Abrir Turno:</label>
                            <div class="col-sm-8 controls">
                                <input type="number"class="input-border-btm form-control" id="cantidadt" name="cantidadt" style="text-transform:uppercase;"  placeholder="$"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"> Nombre del Turno</label>
                            <div class="col-sm-8 controls">
                                <input type="text" class="input-border-btm form-control" id="nombredelturno" name="nombredelturno" style="text-transform:uppercase;" > 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal" id="btnabrirt">Abrir turno</button>
            </div>
        </div>
    </div>
</div>
<?php if ($sturno=='cerrado') { ?>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#modalturno').modal();
        });
    </script>
 <?php } ?>

<!------------------------------------------------>
<div class="modal fade" id="modal_gastos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="false" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Gastos</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Insumo:</label>
                                <div class="col-sm-10 controls">
                                    <div class="switch-button">
                                        <input checked type="checkbox" id="tipo_select" class="switch-button__checkbox">
                                        <label for="tipo_select" class="switch-button__label"></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group" id="lb_insumo">
                                <label class="col-sm-2 control-label">Insumo:</label>
                                <div class="col-sm-10 controls">
                                    <select class="form-control form-control-smr" id="s_insumoG" >
                                        <option disabled selected value="0">Selecciona una opción</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group" id="lb_motivo" style="display: none"> 
                                <label class="col-sm-2 control-label">Motivo:</label>
                                <div class="col-sm-10 controls">
                                    <input type="text" class="form-control form-control-smr" id="motivoG" min="1">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Cantidad:</label>
                                <div class="col-sm-4 controls">
                                    <input type="number" class="form-control form-control-smr" id="cantidadG" min="1">
                                </div>

                                <label class="col-sm-2 control-label">Monto total:</label>
                                <div class="col-sm-4 controls">
                                    <input type="number" class="form-control form-control-smr" id="montoG" placeholder="$">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-raised gradient-flickr white sidebar-shadow" data-dismiss="modal" aria-label="Close">Cerrar</button>
                <button class="btn btn-raised gradient-green-tea white sidebar-shadow" type="button" onclick="add_gasto()">Guardar</button>
            </div>
        </div>
    </div>
</div>