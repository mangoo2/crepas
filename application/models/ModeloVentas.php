<?php
//$a=session_id();
//if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloVentas extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->DB2 = $this->load->database('crepe2_db', TRUE);
        //$this->DB3 = $this->load->database('crepe3_db', TRUE);
    }
    function ingresarventa($uss,$cli,$tipoc,$mpago,$sbtotal,$desc,$descu,$total,$efectivo,$tarjeta,$fecha){
    	$strq = "INSERT INTO ventas(id_personal, id_cliente, tipo_costo, metodo, subtotal, descuento,descuentocant, monto_total,pagotarjeta,efectivo,reg) 
                VALUES ($uss,$cli,$tipoc,$mpago,$sbtotal,$desc,$descu,$total,'$tarjeta','$efectivo','$fecha')";
        $query = $this->db->query($strq);
        $id=$this->db->insert_id();
        //$this->db->close();
        return $id;
    }
    function ingresarventad($idventa,$producto,$cantidad,$precio){
    	$strq = "INSERT INTO venta_detalle(id_venta, id_producto, cantidad, precio) VALUES ($idventa,$producto,$cantidad,$precio)";
        $query = $this->db->query($strq);
        //$this->db->close();

        $strq = "UPDATE productos SET stock=stock-$cantidad WHERE productoid=$producto";
        $query = $this->db->query($strq);
        //$this->db->close();
    }

    function ingresarventadCombo($producto,$cantidad){
        $strq = "UPDATE productos SET stock=stock-$cantidad WHERE productoid=$producto";
        $query = $this->db->query($strq);
        //$this->db->close();
    }

    function regresaVentadCombo($producto,$cantidad){
        $strq = "UPDATE productos SET stock=stock+$cantidad WHERE productoid=$producto";
        $query = $this->db->query($strq);
        //$this->db->close();
    }

    function ingresarventadReceta($idventa,$producto,$cantidad,$precio){
        $strq = "INSERT INTO venta_detalle(id_venta, id_producto, cantidad, precio) VALUES ($idventa,$producto,$cantidad,$precio)";
        $query = $this->db->query($strq);
    }

    function detallesVentaProdCombo($idventa,$producto,$cantidad,$precio){
        $strq = "INSERT INTO venta_detalle_combo(id_venta, id_producto, cantidad, precio) VALUES ($idventa,$producto,$cantidad,$precio)";
        $query = $this->db->query($strq);
    }

    function editaStockReceta($ins,$cant){
        $strq = "UPDATE insumos SET existencia=existencia-$cant WHERE insumosId=$ins";
        $query = $this->db->query($strq);
    }

    function regresaStockReceta($ins,$cant){
        $strq = "UPDATE insumos SET existencia=existencia+$cant WHERE insumosId=$ins";
        $query = $this->db->query($strq);
    }

    function configticket(){
        $strq = "SELECT * FROM ticket";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    function clientepordefecto(){
        $strq = "SELECT * FROM  clientes LIMIT 1";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    function getventas($id){
        $strq = "SELECT * FROM ventas where id_venta=$id";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    /*function getventasd($id){
        $strq = "SELECT IFNULL(vendell.cantidad,0) as cantidad, IFNULL(vendell.precio,0) as precio, pro.nombre, 
                IFNULL(vdc.cantidad,0) as cantidadc, IFNULL(vdc.precio,0) as precioc, pro2.codigo, pro2.nombre as nombrec
        FROM ventas as v
        left join venta_detalle as vendell on vendell.id_venta=v.id_venta
        left join productos as pro on pro.productoid=vendell.id_producto
        left join venta_detalle_combo as vdc on vdc.id_venta=v.id_venta
        left join productos as pro2 on pro2.productoid=vdc.id_producto
        where v.id_venta=$id";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }*/

    function getventasd($id){
        $strq = "SELECT IFNULL(vendell.cantidad,0) as cantidad, IFNULL(vendell.precio,0) as precio, pro.nombre, pro.codigo, pro.tipo, vendell.id_producto
        FROM ventas as v
        left join venta_detalle as vendell on vendell.id_venta=v.id_venta
        left join productos as pro on pro.productoid=vendell.id_producto
        where v.id_venta=$id order by tipo asc";
        $query = $this->db->query($strq);
        return $query;
    }

    function getventasd2($id){
        $strq = "SELECT IFNULL(sum(vendell.cantidad),0) as cantidad, IFNULL(vendell.precio,0) as precio, pro.nombre, pro.codigo, pro.tipo, vendell.id_producto
        FROM ventas as v
        left join venta_detalle as vendell on vendell.id_venta=v.id_venta
        left join productos as pro on pro.productoid=vendell.id_producto and tipo=1
        where v.id_venta=$id and pro.tipo=1";
        $query = $this->db->query($strq);
        return $query;
    }

    /*function getventasdCombo($id){
        $strq = "SELECT pro2.nombre as nombrec, pro3.codigo, GROUP_CONCAT(vdc.cantidad,' ',pro2.nombre SEPARATOR '<br>') as det_combo, 
        pro3.nombre as nombrec, IFNULL(vd.cantidad,0) as cantidadc, IFNULL(vd.precio,0) as precioc
        FROM venta_detalle as vd
        join venta_detalle_combo as vdc on vdc.id_venta=vd.id_venta
        join productos as pro2 on pro2.productoid=vdc.id_producto
        join productos as pro3 on pro3.productoid=vd.id_producto and pro3.tipo=1
        where vd.id_venta=$id
        group by vd.id_detalle_venta";
        $query = $this->db->query($strq);
        return $query;
    }*/

    function getventasdCombo($id){
        $strq = "SELECT pro2.nombre as nombrec, GROUP_CONCAT(vdc.cantidad,' ',pro2.nombre SEPARATOR '<br>') as det_combo 
        FROM venta_detalle_combo as vdc
        join productos as pro2 on pro2.productoid=vdc.id_producto and pro2.tipo=0
        where vdc.id_venta=$id
        group by vdc.id_venta";
        $query = $this->db->query($strq);
        return $query;
    }

    function ingresarcompra($uss,$prov,$total,$tipo=1){
        $strq = "INSERT INTO compras(id_proveedor, monto_total, tipo) VALUES ($prov,$total,$tipo)";
        $query = $this->db->query($strq);
        $id=$this->db->insert_id();
        //$this->db->close();
        return $id;
    }
    function ingresarcomprad($idcompra,$producto,$cantidad,$precio){
        $strq = "INSERT INTO compra_detalle(id_compra, id_producto, cantidad, precio_compra) VALUES ($idcompra,$producto,$cantidad,$precio)";
        $query = $this->db->query($strq);

        $strq = "UPDATE productos SET stock=stock+$cantidad WHERE productoid=$producto";
        $query = $this->db->query($strq);
    }

    function ingresarcompradIns($idcompra,$producto,$cantidad,$precio,$tipo){
        $strq = "INSERT INTO compra_detalle(id_compra, id_producto, cantidad, precio_compra, tipo_prod) VALUES ($idcompra,$producto,$cantidad,$precio,$tipo)";
        $query = $this->db->query($strq);
    }

    function ingresarStockInsumo($ins,$cant){
        $strq = "UPDATE insumos SET existencia=existencia+$cant WHERE insumosId=$ins";
        $query = $this->db->query($strq);
        $this->db->close();
    }

    function ingresarStockInsumoGlobal($ins,$cant){
        //hay que validar que existe el producto en bodega central, si no, hay que ingresarlo con esa cantidad
        //$strq = "UPDATE insumos SET bodega=bodega+$cant WHERE insumosId=$ins"; //es a existencia para mrcrepe2 y crepe3
        $strq = "UPDATE insumos SET bodega=bodega+$cant WHERE insumosId=$ins";
        $query = $this->db->query($strq);
    }

    function turnos(){
        $strq = "SELECT * FROM turno ORDER BY id DESC LIMIT 1";
        $query = $this->db->query($strq);
        //$this->db->close();
        $status='cerrado';
        foreach ($query->result() as $row) {
            $status =$row->status;
        }
        return $status;
    }
    function turnoss(){
        $strq = "SELECT * FROM turno ORDER BY id DESC LIMIT 1";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    function abrirturno($cantidad,$nombre,$fecha,$horaa){
        $strq = "INSERT INTO turno(fecha, horaa, cantidad, nombre, status, user) VALUES ('$fecha','$horaa','$cantidad','$nombre','abierto','user')";
        $query = $this->db->query($strq);
        //$this->db->close();
    }
    function cerrarturno($id,$horaa){
        $fechac=date('Y-m-d');
        $strq = "UPDATE turno SET horac='$horaa',fechacierre='$fechac', status='cerrado' WHERE id=$id";
        $query = $this->db->query($strq);
        //$this->db->close();
    }
    function corte($inicio,$fin,$tipo,$id_suc){
        $where = "";
        if($tipo!=4){
            $where="and tipo_costo=$tipo";
        }if($tipo==""){
            $where = "";
        }

        $strq = "SELECT cl.ClientesId,cl.Nom, v.reg, v.id_venta, v.id_cliente, v.id_personal, v.monto_total,v.pagotarjeta,v.efectivo, v.subtotal, p.personalId, concat(p.nombre,p.apellidos) as vendedor, tipo_costo, prod.preciocompra, IFNULL(vd.precio,0) as precio, prod.preciocomp_uber, prod.preciocomp_rappi, prod.preciocomp_didi, v.metodo, IFNULL(vd.cantidad,0) as cantidad, IFNULL(vdc.precio,0) as precioc, IFNULL(vdc.cantidad,0) as cantidadc, prod2.preciocompra as preciocomprac, prod2.preciocomp_uber as preciocomp_uberc, prod2.preciocomp_rappi as preciocomp_rappic, prod2.preciocomp_didi as preciocomp_didic
                FROM ventas as v 
                left join personal as p on v.id_personal=p.personalId 
                left join venta_detalle as vd on vd.id_venta=v.id_venta
                left join venta_detalle_combo as vdc on vdc.id_venta=v.id_venta
                left join productos as prod on prod.productoid=vd.id_producto
                left join productos as prod2 on prod2.productoid=vdc.id_producto
                join clientes as cl on cl.ClientesId=v.id_cliente
                WHERE v.reg between '$inicio 00:00:00' and '$fin 23:59:59' and v.cancelado=0 $where
                group by v.id_venta";
        if($id_suc==1){
            $query = $this->db->query($strq);
        }if($id_suc==2){
            $query = $this->DB2->query($strq);
        }if($id_suc==3){
            $query = $this->DB3->query($strq);
        }
        return $query;
    }
    function consultarturnoname($inicio,$fin){
        $strq = "SELECT * FROM turno where fecha>='$inicio' and fechacierre<='$fin'";
        $query = $this->db->query($strq);
        //$this->db->close();
        //$nombre=$cfecha.'/'.$chora;
        return $query;
    }
    function cortesum($inicio,$fin,$tipo,$id_suc){
        $where = "";
        if($tipo!=4){
            $where="and tipo_costo=$tipo";
        }
        
        $strq = "SELECT sum(monto_total) as total , sum(subtotal) as subtotal, sum(descuentocant) as descuento 
        ,IFNULL(SUM(CASE WHEN metodo = 1 and tipo_costo=0 THEN (monto_total) ELSE 0 END), 0) AS tEfectivo,
        IFNULL(SUM(CASE WHEN metodo = 2 THEN (pagotarjeta-descuentocant) ELSE 0 END), 0) AS tCredito,
        IFNULL(SUM(CASE WHEN metodo = 3 THEN (pagotarjeta-descuentocant) ELSE 0 END), 0) AS tDebito,
        IFNULL(SUM(CASE WHEN metodo = 4 AND tipo_costo = 1 THEN monto_total ELSE 0 END), 0) AS tUber,
        IFNULL(SUM(CASE WHEN metodo = 4 AND tipo_costo = 2 THEN monto_total ELSE 0 END), 0) AS tRappi,
        IFNULL(SUM(CASE WHEN metodo = 1 AND tipo_costo = 3 THEN monto_total ELSE 0 END), 0) AS tDidi_efect,
        IFNULL(SUM(CASE WHEN metodo = 4 AND tipo_costo = 3 THEN monto_total ELSE 0 END), 0) AS tDidi_plata
                FROM ventas 
                where reg between '$inicio 00:00:00' and '$fin 23:59:59' and cancelado=0 $where";
        if($id_suc==1){
            $query = $this->db->query($strq);
        }if($id_suc==2){
            $query = $this->DB2->query($strq);
        }if($id_suc==3){
            $query = $this->DB3->query($strq);
        }
        return $query;
    }

    function corteProds($inicio,$fin,$tipo,$id_suc){
        $where = "";
        if($tipo!=4){
            $where="and tipo_costo=$tipo";
        }if($tipo==""){
            $where = "";
        }

        $strq = "SELECT v.id_venta, IFNULL(vendell.cantidad,0) as cantidad, IFNULL(vendell.precio,0) as precio, pro.nombre, pro.codigo, pro.tipo, vendell.id_producto
            FROM ventas as v
            left join venta_detalle as vendell on vendell.id_venta=v.id_venta
            left join productos as pro on pro.productoid=vendell.id_producto
            WHERE v.reg between '$inicio 00:00:00' and '$fin 23:59:59' and v.cancelado=0 $where
            order by tipo asc";

        if($id_suc==1){
            $query = $this->db->query($strq);
        }if($id_suc==2){
            $query = $this->DB2->query($strq);
        }if($id_suc==3){
            $query = $this->DB3->query($strq);
        }
        return $query->result();
    }

    function corte_top($inicio,$fin,$tipo,$id_suc){
        $where = "";
        if($tipo!=4){
            $where="and tipo_costo=$tipo";
        }if($tipo==""){
            $where = "";
        }
        /*$strq = "SELECT pr.nombre as producto,sum(vd.cantidad) as cantidad
                FROM ventas as v 
                LEFT join venta_detalle as vd on vd.id_venta=v.id_venta
                left join productos as pr on pr.productoid=vd.id_producto
                WHERE $where 
                v.reg between '$inicio 00:00:00' and '$fin 23:59:59' and v.cancelado=0
                GROUP by vd.id_producto
                ORDER BY cantidad DESC
                limit 10 ";*/
        $strq="select producto, sum(cantidad) as cantidad from  (
                SELECT pr.nombre as producto,sum(vd.cantidad) as cantidad, vd.id_producto
                    FROM ventas as v 
                    inner join venta_detalle as vd on vd.id_venta=v.id_venta
                    inner join productos as pr on pr.productoid=vd.id_producto
                    WHERE  
                    v.reg between '$inicio 00:00:00' and '$fin 23:59:59' and v.cancelado=0
                    GROUP by vd.id_producto
                union 
                SELECT pro2.nombre as producto, sum(vdc.cantidad) as cantidad, vdc.id_producto
                    FROM ventas as v 
                    inner join venta_detalle_combo as vdc on vdc.id_venta=v.id_venta
                    inner join productos as pro2 on pro2.productoid=vdc.id_producto
                    WHERE  
                    v.reg between '$inicio 00:00:00' and '$fin 23:59:59' and v.cancelado=0
                    GROUP by vdc.id_producto
                ) as datos
                GROUP by id_producto
                ORDER BY cantidad DESC
                limit 10";
        if($id_suc==1){
            $query = $this->db->query($strq);
        }if($id_suc==2){
            $query = $this->DB2->query($strq);
        }if($id_suc==3){
            $query = $this->DB3->query($strq);
        }
        return $query->result();
    }

    function corte_insumos($inicio,$fin,$tipo,$id_suc){
        $where = "";
        if($tipo!=4){
            $where="and tipo_costo=$tipo";
        }if($tipo==""){
            $where = "";
        }

        $strq = "SELECT v.id_venta, sum(pi.cantidad) as cantidad, i.insumo
            FROM ventas as v
            inner join venta_detalle_combo as vdc on vdc.id_venta=v.id_venta
            inner join productos_insumos as pi on pi.productoid=vdc.id_producto
            inner join insumos as i on i.insumosId=pi.insumo
            WHERE v.reg between '$inicio 00:00:00' and '$fin 23:59:59' and v.cancelado=0 $where
            group by pi.insumo";

        if($id_suc==1){
            $query = $this->db->query($strq);
        }if($id_suc==2){
            $query = $this->DB2->query($strq);
        }if($id_suc==3){
            $query = $this->DB3->query($strq);
        }
        return $query->result();
    }

    function get_tot_gasto($inicio,$fin,$tipo,$id_suc){
        $where = "";
        if($tipo!=4){
            $where="and tipo_costo=$tipo";
        }if($tipo==""){
            $where = "";
        }

        $strq = "SELECT sum(monto) as tot_gastos
            FROM gastos
            WHERE reg between '$inicio 00:00:00' and '$fin 23:59:59' and activo=1";

        if($id_suc==1){
            $query = $this->db->query($strq);
        }if($id_suc==2){
            $query = $this->DB2->query($strq);
        }if($id_suc==3){
            $query = $this->DB3->query($strq);
        }
        return $query->result();
    }

    function filas() {
        $strq = "SELECT COUNT(*) as total FROM ventas";
        $query = $this->db->query($strq);
        //$this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function total_paginados($por_pagina,$segmento) {
        //$consulta = $this->db->get('productos',$por_pagina,$segmento);
        //return $consulta;
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        $strq = "SELECT ven.id_venta,ven.reg, cli.Nom as cliente, concat(per.nombre,' ',per.apellidos) as vendedor,ven.metodo,ven.subtotal, ven.monto_total,ven.cancelado, ven.tipo_costo
                FROM ventas as ven 
                inner join personal as per on per.personalId=ven.id_personal
                inner join clientes as cli on cli.ClientesId=ven.id_cliente
                ORDER BY ven.id_venta DESC
                LIMIT $por_pagina $segmento";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    function filastur() {
        $strq = "SELECT COUNT(*) as total FROM turno";
        $query = $this->db->query($strq);
        //$this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function cancalarventa($id){
        $fecha = date('Y-m-d');
        $strq = "UPDATE ventas SET cancelado=1,hcancelacion='$fecha' WHERE `id_venta`=$id";
        $query = $this->db->query($strq);
        //$this->db->close();
    }
    function ventadetalles($id){
        $strq = "SELECT venta_detalle.*, p.stockok, p.tipo 
            FROM venta_detalle 
            join productos p on p.productoid=venta_detalle.id_producto
            where id_venta=$id";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    function regresarpro($id,$can){
        $strq = "UPDATE productos set stock=stock+$can where productoid=$id";
        $query = $this->db->query($strq);
        //$this->db->close();
    }
    function filasturnos() {
        $strq = "SELECT COUNT(*) as total FROM turno";
        $query = $this->db->query($strq);
        //$this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function total_paginadosturnos($por_pagina,$segmento) {
        //$consulta = $this->db->get('productos',$por_pagina,$segmento);
        //return $consulta;
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        $strq = "SELECT * FROM turno ORDER BY id DESC
                LIMIT $por_pagina $segmento";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    function consultarturno($id){
        $strq = "SELECT * FROM turno where id=$id";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    function consultartotalturno($fecha,$horaa,$horac,$fechac){
        $strq = "SELECT sum(monto_total) as total FROM ventas where reg between '$fecha $horaa' and '$fechac $horac' and cancelado=0";
        $query = $this->db->query($strq);
        //$this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        }
        return $total;
    }
    function consultartotalturno2($fecha,$horaa,$horac,$fechac){
        $strq = "SELECT sum(p.preciocompra*vd.cantidad) as preciocompra 
                from venta_detalle as vd
                inner join productos as p on vd.id_producto=p.productoid
                inner join ventas as v on vd.id_venta=v.id_venta
                where v.cancelado=0 and v.reg>='$fecha $horaa' and v.reg<='$fechac $horac'";
        $query = $this->db->query($strq);
        //$this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->preciocompra;
        }
        return $total;
    }
    function consultartotalturnopro($fecha,$horaa,$horac,$fechac){
        $strq = "SELECT p.nombre as producto,vd.cantidad, vd.precio
                from venta_detalle as vd
                inner join productos as p on vd.id_producto=p.productoid
                inner join ventas as v on vd.id_venta=v.id_venta
                where v.cancelado=0 and v.reg>='$fecha $horaa' and v.reg<='$fechac $horac'";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    function consultartotalturnopromas($fecha,$horaa,$horac,$fechac){
        $strq = "SELECT p.nombre as producto, sum(vd.cantidad) as total 
                from venta_detalle as vd 
                inner join productos as p on vd.id_producto=p.productoid 
                inner join ventas as v on vd.id_venta=v.id_venta 
                where v.cancelado=0 and v.reg>='$fecha $horaa' and v.reg<='$fechac $horac' GROUP BY producto ORDER BY `total` DESC LIMIT 1";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    function filaslcompras(){
        $strq = "SELECT COUNT(*) as total FROM compra_detalle";
        $query = $this->db->query($strq);
        //$this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function total_paginadoslcompras($por_pagina,$segmento) {
        //$consulta = $this->db->get('productos',$por_pagina,$segmento);
        //return $consulta;
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        /*$prestrq = "SELECT compdll.id_detalle_compra,compdll.cantidad,compdll.precio_compra,compdll.tipo_prod
                FROM compra_detalle as compdll 
                ORDER BY compdll.id_detalle_compra DESC
                LIMIT $por_pagina $segmento";
        $prequery = $this->db->query($prestrq);

        foreach ($prequery->result() as $key) {
            $tipo_prod = $key->tipo_prod;
            
            
            if($tipo_prod==1){
                $prod = "pro.nombre as producto";
                $join = "inner join productos as pro on pro.productoid=compdll.id_producto";
            }if($tipo_prod==0){
                $prod = "ins.insumo as producto";
                $join = "inner join insumos as ins on ins.insumosId=compdll.id_producto";
            }
            log_message('error', 'tipo_prod: '.$tipo_prod);
            $strq = "SELECT compdll.id_detalle_compra,comp.reg, ".$prod.", prov.razon_social,compdll.cantidad,compdll.precio_compra 
                    FROM compra_detalle as compdll 
                    inner join compras as comp on comp.id_compra=compdll.id_compra 
                    ".$join." 
                    inner join proveedores as prov on prov.id_proveedor=comp.id_proveedor 
                    ORDER BY compdll.id_detalle_compra DESC
                    LIMIT $por_pagina $segmento";
            
        }*/

        $strq = "SELECT compdll.id_detalle_compra,compdll.id_detalle_compra as id_dcp, comp.reg, pro.nombre as producto, prov.razon_social,compdll.cantidad,compdll.precio_compra 
                    FROM compra_detalle as compdll 
                    inner join compras as comp on comp.id_compra=compdll.id_compra 
                    inner join productos as pro on pro.productoid=compdll.id_producto
                    inner join proveedores as prov on prov.id_proveedor=comp.id_proveedor 
                    where tipo_prod=1

                    union 
                    SELECT compdll2.id_detalle_compra,compdll2.id_detalle_compra as id_dci, comp.reg, ins.insumo as producto, prov.razon_social,compdll2.cantidad,compdll2.precio_compra 
                    FROM compra_detalle as compdll2 
                    inner join compras as comp on comp.id_compra=compdll2.id_compra 
                    inner join insumos as ins on ins.insumosId=compdll2.id_producto 
                    inner join proveedores as prov on prov.id_proveedor=comp.id_proveedor 
                    where tipo_prod=0
                    ORDER BY id_detalle_compra DESC ";

        if($por_pagina != 0){
            $strq .= "LIMIT $por_pagina $segmento";
        }

        $query = $this->db->query($strq);
        return $query;
    }
    function lcomprasconsultar($inicio,$fin, $por_pagina, $segmento) {
        
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }

        $prestrq = "SELECT compdll.id_detalle_compra,compdll.cantidad,compdll.precio_compra,compdll.tipo_prod
                FROM compra_detalle as compdll 
                ORDER BY compdll.id_detalle_compra DESC ";

        if($por_pagina != 0){
            $prestrq .= "LIMIT $por_pagina $segmento";
        }

        $prequery = $this->db->query($prestrq);

        foreach ($prequery->result() as $key) {
            $tipo_prod = $key->tipo_prod;
        }
        if($tipo_prod==1){
            $prod = "pro.nombre as producto";
            $join = "inner join productos as pro on pro.productoid=compdll.id_producto";
        }else{
            $prod = "ins.insumo as producto";
            $join = "inner join insumos as ins on ins.insumosId=compdll.id_producto";
        }

        $strq = "SELECT compdll.id_detalle_compra,comp.reg,$prod,prov.razon_social,compdll.cantidad,compdll.precio_compra 
                FROM compra_detalle as compdll 
                inner join compras as comp on comp.id_compra=compdll.id_compra 
                $join
                inner join proveedores as prov on prov.id_proveedor=comp.id_proveedor 
                where comp.reg>='$inicio 00:00:00' and comp.reg<='$fin 23:59:59'
                ORDER BY compdll.id_detalle_compra DESC";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    function ventassearch($search){
        $strq = "SELECT ven.id_venta,ven.reg, cli.Nom as cliente, concat(per.nombre,' ',per.apellidos) as vendedor,ven.metodo,ven.subtotal, ven.monto_total,ven.cancelado, ven.tipo_costo
                FROM ventas as ven 
                inner join personal as per on per.personalId=ven.id_personal
                inner join clientes as cli on cli.ClientesId=ven.id_cliente
                where ven.id_venta like '%".$search."%' or 
                      ven.reg like '%".$search."%' or
                      cli.Nom like '%".$search."%' or
                      per.nombre like '%".$search."%' or
                      ven.monto_total like '%".$search."%'
                ";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    public function search_insumo($search) {
        $strq = "SELECT insumosId, insumo
                FROM insumos
                WHERE activo = 1
                AND (insumo LIKE '%$search%')";
        $query = $this->db->query($strq);
        return $query->result();
    }
    

    function get_result($params){
        $columns = array( 
            0=>'cd.id_detalle_compra',
            1=>'cd.cantidad',
            2=>'cd.precio_compra',
            3=>'c.reg',
            4=>'prov.razon_social',
            5=>'p.nombre',
            6=>'i.insumo',
            7=>'cd.tipo_prod',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('compra_detalle cd');
        $this->db->join('compras c', 'cd.id_compra = c.id_compra', 'left');
        $this->db->join('proveedores prov', 'c.id_proveedor = prov.id_proveedor', 'left');
        $this->db->join('insumos i', 'cd.id_producto = i.insumosId', 'left');
        $this->db->join('productos p', 'cd.id_producto = p.productoid', 'left');
        //$this->db->where('activo',1);

        if ($params["fechaIni"] != 0 && $params["fechaFin"] != 0) {
            $this->db->where('c.reg BETWEEN ' . '"' . $params["fechaIni"] . ' 00:00:00"' . ' AND ' . '"' . $params["fechaFin"] . ' 23:59:59"');
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_result($params){
        $columns = array( 
            0=>'cd.id_detalle_compra',
            1=>'cd.cantidad',
            2=>'cd.precio_compra',
            3=>'c.reg',
            4=>'prov.razon_social',
            5=>'p.nombre',
            6=>'i.insumo',
            7=>'cd.tipo_prod',
        );

        $this->db->select('COUNT(1) as total');
        $this->db->from('compra_detalle cd');
        $this->db->join('compras c', 'cd.id_compra = c.id_compra', 'left');
        $this->db->join('proveedores prov', 'c.id_proveedor = prov.id_proveedor', 'left');
        $this->db->join('insumos i', 'cd.id_producto = i.insumosId', 'left');
        $this->db->join('productos p', 'cd.id_producto = p.productoid', 'left');
        //$this->db->where('activo',1);

        if ($params["fechaIni"] != 0 && $params["fechaFin"] != 0) {
            $this->db->where('c.reg BETWEEN ' . '"' . $params["fechaIni"] . ' 00:00:00"' . ' AND ' . '"' . $params["fechaFin"] . ' 23:59:59"');
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    function get_list($fechaIni, $fechaFin){
        $this->db->select('cd.id_detalle_compra, cd.cantidad, cd.precio_compra, c.reg, prov.razon_social, p.nombre, i.insumo, cd.tipo_prod');
        $this->db->from('compra_detalle cd');
        $this->db->join('compras c', 'cd.id_compra = c.id_compra', 'left');
        $this->db->join('proveedores prov', 'c.id_proveedor = prov.id_proveedor', 'left');
        $this->db->join('insumos i', 'cd.id_producto = i.insumosId', 'left');
        $this->db->join('productos p', 'cd.id_producto = p.productoid', 'left');
        //$this->db->where('activo',1);

        if ($fechaIni != 0 && $fechaFin != 0) {
            $this->db->where('c.reg BETWEEN ' . '"' . $fechaIni . ' 00:00:00"' . ' AND ' . '"' . $fechaFin . ' 23:59:59"');
        }

        $this->db->order_by('cd.id_detalle_compra','desc');

        $query = $this->db->get();
        return $query;
    }

}