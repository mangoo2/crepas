<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloTraslados extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function getlistTraslado($params){
        $columns = array(
            0=>'id',
            1=>'id_origen',
            2=>'id_destino',
            3=>'fecha'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('traslados');
        $this->db->where(array('estatus'=>1));
  
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    function getTotTraslado($params){
        $columns = array(
            0=>'id',
            1=>'id_origen',
            2=>'id_destino',
            3=>'fecha'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('traslados');
        $this->db->where(array('estatus'=>1));

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                    $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $query=$this->db->get();
        return $query->row()->total;
    }

    function updateStock($cant,$id,$opera){
        $strq = "UPDATE insumos SET existencia=existencia $opera $cant WHERE insumosId=$id";
        $this->db->query($strq);
    }

    function updateStockBodega($cant,$id,$opera){
        $strq = "UPDATE insumos SET bodega=bodega $opera $cant WHERE insumosId=$id";
        $this->db->query($strq);
    }

    public function get_detalleTras($id){
        $this->db->select("td.*, i.insumo");
        $this->db->from("traslados_detalles td");
        $this->db->join("insumos i","i.insumosId=td.id_insumo","left");
        $this->db->where("td.id_traslado",$id);
        $this->db->where("td.estatus",1);
        $query=$this->db->get();
        return $query->result();
    }

}