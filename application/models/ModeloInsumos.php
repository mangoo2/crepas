<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloInsumos extends CI_Model {
    public function __construct() {
        parent::__construct();
        //$this->DB2 = $this->load->database('crepe2_db', TRUE);
        //$this->DB3 = $this->load->database('crepe3_db', TRUE);
    }

    function getlistinsumos($params){
        $columns = array(
            0=>'insumosId',
            1=>'insumo',
            2=>'existencia',
            3=>'operaciones',
            4=>'stockmin',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('insumos');
        $this->db->where('activo',1);
        //$this->db->where('(de_bodega = 0 or existencia >= 0)');
        
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    function getlistinsumost($params){
        $columns = array(
            0=>'insumosId',
            1=>'insumo',
            2=>'existencia',
            3=>'operaciones',
            4=>'stockmin',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('insumos');
        $this->db->where('activo',1);
        //$this->db->where('(de_bodega = 0 or existencia >= 0)');
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                    $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $query=$this->db->get();
        return $query->row()->total;
    }

    function insumoSearch($ins,$id_origen=0){
        $where="";
        if($id_origen==1){
            $where=" and de_bodega=0 or de_bodega=1 and existencia>0";
        }if($id_origen==4){
            $where=" and de_bodega>0 ";
        }
        $strq = "SELECT *, insumos.insumo as nom_insumo 
            FROM insumos 
            /*join productos_insumos ps on ps.insumo=insumos.insumosId
            join productos p on p.productoid=ps.productoId*/
            where insumos.activo=1 and de_bodega=0 and insumos.insumo like '%".$ins."%'
            or insumos.activo=1 and de_bodega=1 and existencia>0 and insumos.insumo like '%".$ins."%'";
        $query = $this->db->query($strq);
        return $query;
    }

    function insumoSearchCrepe2($ins){
        $strq = "SELECT *, insumos.insumo as nom_insumo, existencia as existencia2
            FROM insumos 
            where insumos.activo=1 and insumos.insumo like '%".$ins."%'";
        $query = $this->DB2->query($strq);
        return $query;
    }

    function getselectwherenC2($table,$where){
        $this->DB2->select('*');
        $this->DB2->from($table);
        $this->DB2->where($where);
        $query=$this->DB2->get(); 
        return $query;
    }

    function getselectwherenOrWhereC2($table,$where,$or_where){
        $this->DB2->select('*');
        $this->DB2->from($table);
        $this->DB2->where($where);
        $this->DB2->or_where($or_where);
        $query=$this->DB2->get(); 
        return $query;
    }

    function updateStockC2($cant,$id,$opera){
        $strq = "UPDATE insumos SET existencia=existencia $opera $cant WHERE insumosId=$id";
        $this->DB2->query($strq);
    }

    function updateStockBodegaC2($cant,$id,$opera){
        $strq = "UPDATE insumos SET bodega=bodega $opera $cant WHERE insumosId=$id";
        $this->DB2->query($strq);
    }

    function InsertC2($Tabla,$data){
        $this->DB2->insert($Tabla, $data);
        $id=$this->DB2->insert_id();
        return $id;
    }

    ///////////////////////////////////////////////////////////////////////

    function insumoSearchCrepe3($ins){
        $strq = "SELECT *, insumos.insumo as nom_insumo, existencia as existencia3
            FROM insumos 
            where insumos.activo=1 and insumos.insumo like '%".$ins."%'";
        $query = $this->DB3->query($strq);
        return $query;
    }

    function getselectwherenC3($table,$where){
        $this->DB3->select('*');
        $this->DB3->from($table);
        $this->DB3->where($where);
        $query=$this->DB3->get(); 
        return $query;
    }

    function getselectwherenOrWhereC3($table,$where,$or_where){
        $this->DB3->select('*');
        $this->DB3->from($table);
        $this->DB3->where($where);
        $this->DB3->or_where($or_where);
        $query=$this->DB3->get(); 
        return $query;
    }

    function updateStockC3($cant,$id,$opera){
        $strq = "UPDATE insumos SET existencia=existencia $opera $cant WHERE insumosId=$id";
        $this->DB3->query($strq);
    }

    function updateStockBodegaC3($cant,$id,$opera){
        $strq = "UPDATE insumos SET bodega=bodega $opera $cant WHERE insumosId=$id";
        $this->DB3->query($strq);
    }

    function InsertC3($Tabla,$data){
        $this->DB3->insert($Tabla, $data);
        $id=$this->DB3->insert_id();
        return $id;
    }

    /* **********************************************************/

    function insumoSearch2($ins){
        $strq = "SELECT * 
            FROM productos p
            where activo=1 and nombre like '%".$ins."%'
            or activo=1 and codigo like '%".$ins."%'";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function getInsumo($id){
        $strq = "SELECT * FROM insumos where insumosId=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    /* ***************LISTADO DE INSUMO DE BODEGA******************* */
    function get_result($params){
        $columns = array( 
            0=>'insumosId',
            1=>'insumo',
            2=>'existencia',
            3=>'bodega',
            4=>'stockmin',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('insumos');
        $this->db->where('activo_bodega',1);
        //$this->db->where('(de_bodega = 1 or bodega >= 0)');

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_result($params){
        $columns = array( 
            0=>'insumosId',
            1=>'insumo',
            2=>'existencia',
            3=>'bodega',
            4=>'stockmin',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('insumos');
        $this->db->where('activo_bodega',1);
        //$this->db->where('(de_bodega = 1 or bodega >= 0)');

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    /*function sumaStock($cant,$id,$conteo){
        $strq = "UPDATE insumos SET conteo_fisico=$conteo, existencia=existencia+$cant WHERE insumosId=$id";
        $this->db->query($strq);
    }*/

    function sumaStock($cant,$id,$conteo,$stockmin){
        $strq = "UPDATE insumos SET conteo_fisico=$conteo, existencia=$cant WHERE insumosId=$id";
        $this->db->query($strq);
    }

    /*function sumaStockBodega($cant,$id,$conteo){
        $strq = "UPDATE insumos SET conteo_fisco_bod=$conteo, bodega=bodega+$cant WHERE insumosId=$id";
        $this->db->query($strq);
    }*/
    function sumaStockBodega($cant,$id,$conteo,$stockmin){
        $strq = "UPDATE insumos SET conteo_fisco_bod=$conteo, bodega=$cant WHERE insumosId=$id";
        $this->db->query($strq);
    }

    function inferiorAStockMinimo(){
        $strq = "SELECT * 
            FROM insumos i
            WHERE activo = 1
            /*AND de_bodega = 0*/
            AND (stockmin = 0  OR stockmin >= existencia)";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    function inferiorAStockMinimoBodega(){
        $strq = "SELECT * 
            FROM insumos i
            WHERE activo_bodega = 1
            /*AND de_bodega = 1*/
            AND (stockmin = 0  OR stockmin >= bodega)";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
}