<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ModeloGastos extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    function get_result($params)
    {
        $columns = array(
            0 => 'g.id',
            1 => 'i.insumo',
            2 => 'g.motivo',
            3 => 'g.cantidad',
            4 => 'g.monto',
            5 => 'g.tipo',
            6 => 'CONCAT(p.nombre,p.apellidos) AS personal',
            7 => 'g.modalFlag',
            8 => 'g.reg'
        );

        $columnsss = array(
            0 => 'g.id',
            1 => 'i.insumo',
            2 => 'g.motivo',
            3 => 'g.cantidad',
            4 => 'g.monto',
            5 => 'g.tipo',
            6 => 'CONCAT(p.nombre,p.apellidos)',
            7 => 'g.modalFlag',
            8 => 'g.reg'
        );
        $select = "";
        foreach ($columns as $c) {
            $select .= "$c, ";
        }
        $this->db->select($select);
        $this->db->from('gastos g');
        $this->db->join('insumos i', 'g.insumoId = i.insumosId', 'left');
        $this->db->join('usuarios u', 'g.usuarioId = u.UsuarioID', 'left');
        $this->db->join('personal p', 'u.personalId = p.personalId', 'left');
        $this->db->where('g.activo', 1);

        if (!empty($params['search']['value'])) {
            $search = $params['search']['value'];
            $this->db->group_start();
            foreach ($columnsss as $c) {
                $this->db->or_like($c, $search);
            }
            $this->db->group_end();
        }
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'], $params['start']);
        $query = $this->db->get();
        return $query;
    }

    public function total_result($params)
    {
        $columns = array(
            0 => 'g.id',
            1 => 'i.insumo',
            2 => 'g.motivo',
            3 => 'g.cantidad',
            4 => 'g.monto',
            5 => 'g.tipo',
            6 => 'CONCAT(p.nombre,p.apellidos)',
            7 => 'g.modalFlag',
            8 => 'g.reg'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('gastos g');
        $this->db->join('insumos i', 'g.insumoId = i.insumosId', 'left');
        $this->db->join('usuarios u', 'g.usuarioId = u.UsuarioID', 'left');
        $this->db->join('personal p', 'u.personalId = p.personalId', 'left');
        $this->db->where('g.activo', 1);

        if (!empty($params['search']['value'])) {
            $search = $params['search']['value'];
            $this->db->group_start();
            foreach ($columns as $c) {
                $this->db->or_like($c, $search);
            }
            $this->db->group_end();
        }
        $query = $this->db->get();
        return $query->row()->total;
    }

    public function search_insumo($search)
    {
        $strq = "SELECT insumosId, insumo
                FROM insumos
                WHERE activo = 1
                AND (insumo LIKE '%$search%')";
        $query = $this->db->query($strq);
        return $query->result();
    }

}