<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Insumos extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->library('excel');
        $this->load->helper('url');
        $this->load->model('ModeloInsumos');
        $this->load->model('ModeloCatalogos');
        date_default_timezone_set('America/Mexico_City');
    }
	public function index(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('insumos/insumoslis');
        $this->load->view('templates/footer');
        $this->load->view('insumos/insumoslisjs');
	}
    public function insumosadd($id=0){
        
        $insumosId=0;
        $insumo='';
        $existencia='';
        $stockmin='';
        $operaciones='';
        $data['label']='Nuevo Insumo';
        $resultpro=$this->ModeloCatalogos->getselectwheren('insumos',array('insumosId'=>$id));
        foreach ($resultpro->result() as $item) {
            $insumosId=$item->insumosId;
            $insumo=$item->insumo;
            $existencia=$item->existencia;
            $stockmin=$item->stockmin;
            $data['label']='Editar Insumo';
        }

        $data['insumosId']=$insumosId;
        $data['insumo']=$insumo;
        $data['existencia']=$existencia;
        $data['stockmin']=$stockmin;

        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('insumos/insumoadd',$data);
        $this->load->view('templates/footer');
        $this->load->view('insumos/insumoaddjs');
    }
    function insadd(){
        $params=$this->input->post();
        $insumosId=$params['insumosId'];
        unset($params['insumosId']);
        
        if ($insumosId>0) {
            $this->ModeloCatalogos->updateCatalogo('insumos',$params,array('insumosId'=>$insumosId));
        }else{
            $params["de_bodega"]=0;
            $insumosId=$this->ModeloCatalogos->Insert('insumos',$params);
        }
    }
    
    public function deleteinsumos(){
        $id = $this->input->post('id');
        $this->ModeloCatalogos->updateCatalogo('insumos',array('activo'=>0),array('insumosId'=>$id));
    }
    
    public function getlistinsumo() {
        $params = $this->input->post();
        $getdata = $this->ModeloInsumos->getlistinsumos($params);
        $totaldata= $this->ModeloInsumos->getlistinsumost($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function uploadExcel() {  
        $configUpload['upload_path'] = FCPATH.'fileExcel/';
        $configUpload['allowed_types'] = 'xls|xlsx|csv';
        $configUpload['max_size'] = '5000';
        $this->load->library('upload', $configUpload);
        $this->upload->do_upload('inputFile');  
        $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
        $file_name = $upload_data['file_name']; //uploded file name
        $extension=$upload_data['file_ext'];    // uploded file extension

        //$objReader =PHPExcel_IOFactory::createReader('Excel5');     //For excel 2003 
        //$objReader= PHPExcel_IOFactory::createReader('Excel2007'); // For excel 2007  
        $objReader= PHPExcel_IOFactory::createReader('CSV'); // For excel 2007  
        $objReader->setReadDataOnly(true);          

        $objPHPExcel=$objReader->load(FCPATH.'fileExcel/'.$file_name);      
        $totalrows=$objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel         
        $objWorksheet=$objPHPExcel->setActiveSheetIndex(0); 

        $array_insert=array();
        for($i=2;$i<=$totalrows;$i++){
            $insumosId= $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();           
            $insumo= $objWorksheet->getCellByColumnAndRow(1,$i)->getValue(); //Excel Column 1
            $existencia= $objWorksheet->getCellByColumnAndRow(2,$i)->getValue(); //Excel Column 2
            $stockmin=$objWorksheet->getCellByColumnAndRow(3,$i)->getValue(); //Excel Column 3
            $conteo=$objWorksheet->getCellByColumnAndRow(4,$i)->getValue(); //Excel Column 4
            $cant_compra=$objWorksheet->getCellByColumnAndRow(5,$i)->getValue(); //Excel Column 5
            $id_proveedor=$objWorksheet->getCellByColumnAndRow(6,$i)->getValue(); //Excel Column 6

            //log_message('error', 'cant_compra: '.$cant_compra);
            //log_message('error', 'id_proveedor: '.$id_proveedor);
            if(intval($cant_compra)>=0 && intval($id_proveedor>0)){
                //log_message('error', 'insumosId con proveedor: '.$insumosId);
                //log_message('error', 'id_proveedor: '.$id_proveedor);
                $id_prov=$id_proveedor;
                $compra=$cant_compra;
                if($cant_compra==""){
                    $compra=0;
                }
                //$nva_cant=$existencia+$compra;
                $nva_cant=$conteo+$compra;
                $this->ModeloInsumos->sumaStock($nva_cant,$insumosId,$conteo,$stockmin);
                $arrayinsert = array(
                            'id_compra'=>0,
                            'id_producto'=>$insumosId,
                            'tipo_prod'=>0,
                            'cantidad'=>$cant_compra
                          );
                $array_insert[]=$arrayinsert;
            }
        }
        if(count($array_insert)>0){
            $id_compra=$this->ModeloCatalogos->Insert('compras',array("id_proveedor"=>$id_prov,"tipo"=>1,"reg"=>date("Y-m-d H:i:s")));
            //log_message('error', 'id_compra: '.$id_compra);
            //array_push ($array_insert['id_compra'],$id_compra);
            //array_push ($array_insert['id_compra']=$id_compra,$array_insert);
            //$array_insert=array_merge($array_insert,array("id_compra"=>$id_compra));
            //array_push($array_insert, array('id_compra'=>$id_compra));
            //array_push ($array_insert,['id_compra'=>$id_compra]);
            //$this->array_push_assoc($array_insert, array('id_compra'=>$id_compra));
            //$this->array_push_assoc($array_insert, ['id_compra'=>$id_compra]);

            //$other = array( 'id_compra' =>$id_compra );
            //arrary_merge( $other, $array_insert);
            //array_replace($array_insert,array(0=>$id_compra));
            for($j = 0 ; $j < sizeof($array_insert); $j++){
              $array_insert[$j]['id_compra'] = $id_compra;
            }
            //log_message('error', "array_insert: ".print_r($array_insert));
            $this->ModeloCatalogos->insert_batch('compra_detalle',$array_insert);
        } 

        unlink('fileExcel/'.$file_name); 
        $output = [];
        echo json_encode($output);
    }
    
    function array_push_assoc(array &$arrayDatos, array $values){
        $arrayDatos = array_merge_recursive($arrayDatos, $values);
    }

    function exportExcel(){
        $data["bodega"]=0;
        //$data["insumos"]=$this->ModeloCatalogos->getselectwherenOrwhere2('insumos',array('activo'=>1,"de_bodega"=>0),"(de_bodega = 0 or existencia >= 0)");
        $data["insumos"]=$this->ModeloCatalogos->getselectwheren('insumos',array('activo'=>1));
        $this->load->view('insumos/list_excel',$data);
    } 

    function exportStocks(){
        $data["bodega"]=0;
        $data["insumos"] = $this->ModeloInsumos->inferiorAStockMinimo();
        
        $this->load->view('insumos/list_stocks',$data);
    } 

}
