<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Ticket extends CI_Controller {
	function __construct(){
        parent::__construct();
        $this->load->helper('url');
        //$this->load->model('Solicitud');
        $this->load->model('ModeloVentas');
    }
	public function index(){
		$id=$this->input->get('id');
		
		if (isset($_GET['vcambio'])) {
			$data['vcambio']=$_GET['vcambio'];
			$data['vpagacon']=$_GET['vpagacon'];
			$data['metodo']=$_GET['metodo'];
		}else{
			$data['vcambio']='';
			$data['vpagacon']='';
			$data['metodo']='';
		}
        $data["id"]=$id;
		$data['configticket']=$this->ModeloVentas->configticket();
		$data['getventas']=$this->ModeloVentas->getventas($id);
		$data['getventasd']=$this->ModeloVentas->getventasd($id);
		$this->load->view('Reportes/ticket',$data);
	        
	}

	public function tablaTest(){
		$html='<table width="100%" border="1" >
            <tr>
              <td colspan="3" align="center"><img src="https://mrcrepe.mangoo.systems/public/img/ops.png" width="100px" ></td>
            </tr>
            <tr>
              <th colspan="3" align="center" style="font-size: 9; font-family: Open Sans;">Los mejores haciendo crepas!</th>
            </tr>
            <tr>
                <th colspan="3" align="center" ></th>
            </tr>
            <tr>
                <th colspan="3" align="center" style="font-size: 9; font-family: Open Sans;">¡GRACIAS POR SU COMPRA!</th>
            </tr>
            <tr>
                <th colspan="3" align="center" style="font-size: 9; font-family: Open Sans;">Fecha: 08-06-2023</th>
            </tr>
            <tr>
                <th colspan="3" align="center" style="font-size: 9; ;font-family: Open Sans;">Hora: 21:56</th>
            </tr>
            <tr>
                <th colspan="3" align="center" style="font-size: 9; ;font-family: Open Sans;">No ticket: 569</th>
            </tr>
            <tr>
                <th colspan="3" align="center" style="font-size: 9"></th>
            </tr>
          </table>';
         echo $html;
	}
}