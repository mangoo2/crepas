<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ListaVentas extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloVentas');
        $this->load->model('ModeloCatalogos');
    }
    public function index(){
            $pages=10; //Número de registros mostrados por páginas
            $this->load->library('pagination'); //Cargamos la librería de paginación
            $config['base_url'] = base_url().'ListaVentas/view/'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
            $config['total_rows'] = $this->ModeloVentas->filas();//calcula el número de filas
            $config['per_page'] = $pages; //Número de registros mostrados por páginas  
            $config['num_links'] = 3; //Número de links mostrados en la paginación
            $config['first_link'] = 'Primera';//primer link
            $config['last_link'] = 'Última';//último link
            $config["uri_segment"] = 3;//el segmento de la paginación
            $config['next_link'] = 'Siguiente';//siguiente link
            $config['prev_link'] = 'Anterior';//anterior link
            $this->pagination->initialize($config); //inicializamos la paginación 
            $pagex = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["ventas"] = $this->ModeloVentas->total_paginados($pagex,$config['per_page']);
            
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            //$this->load->view('Personal/Personal',$data);
            $this->load->view('ventas/listaventas',$data);
            $this->load->view('templates/footer');
    }
    function cancalarventa(){
        $id = $this->input->post('id');
        $this->ModeloVentas->cancalarventa($id);

        $resultado=$this->ModeloVentas->ventadetalles($id);
        foreach ($resultado->result() as $item){
            $cantidad=$item->cantidad;
            if($item->stockok==1){ //producto simple de existencia
                $this->ModeloVentas->regresarpro($item->id_producto,$item->cantidad);
            }
            if($item->tipo==1){ //combo  
                $getpro=$this->ModeloCatalogos->getselectwheren("venta_detalle_combo",array("id_venta"=>$id));
                foreach ($getpro->result() as $gp ) {
                    $getprocom=$this->ModeloCatalogos->getselectwheren("productos",array("productoid"=>$gp->id_producto));//manda prod de combo
                    foreach ($getprocom->result() as $gpc) {
                        if($gpc->stockok==1 && $gpc->tipo==0){ //productos de empaque
                            $this->ModeloVentas->regresaVentadCombo($gpc->productoid,$gp->cantidad); //UPDATE productos
                        }else if($gpc->stockok==0){ //productos de receta
                            $get_recete=$this->ModeloCatalogos->getselectwheren("productos_insumos",array("productoid"=>$gp->id_producto,"activo"=>1));
                            foreach ($get_recete->result() as $key ) {
                                $cant=$key->cantidad;
                                $insumo=$key->insumo;
                                //$tot_cant = $cantidad*$cant;    
                                //log_message('error', 'cant: '.$cant);         

                                $this->ModeloVentas->regresaStockReceta($insumo,$cant);
                            }
                        }
                    }
                }
                /*$get_recete=$this->ModeloCatalogos->getselectwheren("productos_insumos",array("productoid"=>$item->id_producto,"activo"=>1));
                foreach ($get_recete->result() as $key ) {
                    $cant=$key->cantidad;
                    $insumo=$key->insumo;
                    $tot_cant = $cantidad*$cant;    
                    //log_message('error', 'cant: '.$cant);         

                    $this->ModeloVentas->regresaStockReceta($insumo,$tot_cant);
                }*/
            }else{ //producto simple de receta (sin stock)
                $get_recete=$this->ModeloCatalogos->getselectwheren("productos_insumos",array("productoid"=>$item->id_producto,"activo"=>1));
                foreach ($get_recete->result() as $key ) {
                    $cant=$key->cantidad;
                    $insumo=$key->insumo;
                    $tot_cant = $cantidad*$cant;    
                    //log_message('error', 'cant: '.$cant);         
                    $this->ModeloVentas->regresaStockReceta($insumo,$tot_cant);
                }
            }
        }
        $getpro=$this->ModeloCatalogos->getselectwheren("venta_detalle_combo",array("id_venta"=>$id));
        foreach ($getpro->result() as $gp ) {
            $getprocom=$this->ModeloCatalogos->getselectwheren("productos",array("productoid"=>$gp->id_producto));
            foreach ($getprocom->result() as $gpc) {
                if($gpc->stockok==1 && $gpc->tipo==0){
                    $this->ModeloVentas->regresaVentadCombo($gpc->productoid,$gp->cantidad); //UPDATE productos
                }
            }
        }
    }
    function buscarvent(){
        $buscar = $this->input->post('buscar');
        $resultado=$this->ModeloVentas->ventassearch($buscar);
        foreach ($resultado->result() as $item){ ?>
            <tr id="trven_<?php echo $item->id_venta; ?>">
                  <td><?php echo $item->id_venta; ?></td>
                  <td><?php echo $item->reg; ?></td>
                  <td><?php echo $item->vendedor; ?></td>
                  <td><?php echo "$".number_format($item->monto_total,2,".",","); ?></td>
                  <td><?php if($item->tipo_costo==0) echo "MOSTRADOR"; 
                      if($item->tipo_costo==1) echo "UBER";
                      if($item->tipo_costo==2) echo "RAPPI"; 
                      if($item->tipo_costo==3) echo "DIDI";   ?></td>
                  <td><?php echo $item->cliente; ?></td>
                  <td><?php if($item->cancelado==1){ echo '<span class="badge badge-danger">Cancelado</span>';} ?></td>
                  <td>
                    <button class="btn btn-raised gradient-blackberry white sidebar-shadow" onclick="ticket(<?php echo $item->id_venta; ?>)" title="Ticket" data-toggle="tooltip" data-placement="top">
                      <i class="fa fa-book"></i>
                    </button>
                    <button class="btn btn-raised gradient-flickr white sidebar-shadow" onclick="cancelar(<?php echo $item->id_venta; ?>,<?php echo "$".number_format($item->monto_total,2,".",","); ?>)" title="Cancelar" data-toggle="tooltip" data-placement="top" <?php if($item->cancelado==1){ echo 'disabled';} ?>>
                      <i class="fa fa-times"></i>
                    </button>
                  </td>
          </tr>
        <?php }
    }
       
}
