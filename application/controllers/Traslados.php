<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Traslados extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloTraslados');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloInsumos');
        date_default_timezone_set('America/Mexico_City');
        $this->fecha=date("Y-m-d");
        $this->fechac=date("Y-m-d H:i:s");
        $this->usuarioid=$this->session->userdata("usuarioid_tz");
    }
	public function index(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('traslados/index');
        $this->load->view('templates/footer');
        $this->load->view('traslados/indexjs');
	}
    public function trasladoadd(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('traslados/form');
        $this->load->view('templates/footer');
        $this->load->view('traslados/formjs');
    }

    public function searchInsumo(){
        $ins = $this->input->get('search');
        $id_origen = $this->input->get('id_origen');
        if($id_origen==1){ //crepe1
            $results=$this->ModeloInsumos->insumoSearch($ins,$id_origen);
        }
        if($id_origen==2){ //crepe2
            $results=$this->ModeloInsumos->insumoSearchCrepe2($ins);
        }
        if($id_origen==3){ //crepe3
            $results=$this->ModeloInsumos->insumoSearchCrepe3($ins);
        }
        if($id_origen==4){ //bodega
            $results=$this->ModeloInsumos->insumoSearch($ins,$id_origen);
        }
        echo json_encode($results->result());
    }

    function save_traslado(){
        $data=$this->input->post();
        $data["fecha"]=$this->fecha;
        $data["reg"]=$this->fechac;
        $data["id_usuario"]=$this->usuarioid;
        $id=$this->ModeloCatalogos->Insert('traslados',$data);
        echo $id;
    }
    
    function ingresarDetalles(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        $this->db->trans_start();
            for ($i=0;$i<count($DATA);$i++) { 
                $id_traslado = $DATA[$i]->id_traslado;
                $id_insumo = $DATA[$i]->id_insumo;
                $insumo = $DATA[$i]->insumo;
                $id_origen = $DATA[$i]->id_origen;
                $id_destino = $DATA[$i]->id_destino;
                $cant = $DATA[$i]->cant;
                if($id_origen==1 || $id_origen==4){
                    if($id_destino==1 || $id_destino==2 || $id_destino==3){
                        //$where=array("insumo"=>$insumo,"de_bodega"=>"0","activo"=>1);
                        //$or_where=array("insumo"=>$insumo,"de_bodega"=>"1","activo"=>1);
                        $where=array("insumo"=>$insumo,"activo"=>1);
                    }if($id_destino==4){
                        //$where=array("insumo"=>$insumo,"de_bodega"=>"1","activo"=>1);
                        //$or_where=array("insumo"=>$insumo,"de_bodega"=>"0","activo"=>1);
                        $where=array("insumo"=>$insumo,"activo"=>1);
                    }
                    if($id_destino==1 || $id_destino==4){
                        $get_ins=$this->ModeloCatalogos->getselectwheren("insumos",$where);
                        //$get_ins=$this->ModeloCatalogos->getselectwherenOrwhere("insumos",$where,$or_where);
                    }if($id_destino==2){
                        $get_ins=$this->ModeloInsumos->getselectwherenC2("insumos",$where);
                        //$get_ins=$this->ModeloInsumos->getselectwherenOrWhereC2("insumos",$where,$or_where);
                    }if($id_destino==3){
                        $get_ins=$this->ModeloInsumos->getselectwherenC3("insumos",$where);
                        //$get_ins=$this->ModeloInsumos->getselectwherenOrWhereC3("insumos",$where,$or_where);
                    }
                    
                    if($get_ins->num_rows()>0){
                        $get_ins=$get_ins->row();
                        if($id_origen==1){//actualiza existencia de insumo -- viene de crepas1
                            $this->ModeloTraslados->updateStock($cant,$id_insumo,"-"); //resta a crepas 1
                            //$this->ModeloTraslados->updateStockBodega($cant,$id_insumo,"+"); //suma a bodega
                            if($id_destino==2){//actualiza existencia de crepas1
                                $this->ModeloInsumos->updateStockC2($cant,$get_ins->insumosId,"+");
                            }if($id_destino==3){ //actualiza existencia de crepas3
                                $this->ModeloInsumos->updateStockC3($cant,$get_ins->insumosId,"+");
                            }if($id_destino==4){ //actualiza existencia de bodega
                                $this->ModeloTraslados->updateStockBodega($cant,$get_ins->insumosId,"+");
                            }
                        }else{ //actualiza existencia de bodega
                            $this->ModeloTraslados->updateStockBodega($cant,$id_insumo,"-"); //resta a bodega
                            //$this->ModeloTraslados->updateStock($cant,$id_insumo,"+"); //suma a crepas 1
                            if($id_destino==1){//actualiza existencia de crepas1
                                $this->ModeloTraslados->updateStock($cant,$get_ins->insumosId,"+");
                            }if($id_destino==2){ //actualiza existencia de crepas3
                                $this->ModeloInsumos->updateStockC2($cant,$get_ins->insumosId,"+");
                            }if($id_destino==3){ //actualiza existencia de bodega
                                $this->ModeloInsumos->updateStockC3($cant,$get_ins->insumosId,"+");
                            }
                        }
                        $this->ModeloCatalogos->Insert('traslados_detalles',array("id_traslado"=>$id_traslado,"id_insumo"=>$id_insumo,"id_origen"=>$id_origen,"id_destino"=>$id_destino,"cant"=>$cant,"fecha_reg"=>$this->fechac,"id_insumo_destino"=>$get_ins->insumosId));
                    }else{ //no existe el insumo del destino
                        if($id_origen==4){
                            //$this->ModeloCatalogos->Insert('insumos',array("insumo"=>$insumo,"existencia"=>$cant,"reg"=>$this->fechac));
                            $this->ModeloTraslados->updateStockBodega($cant,$id_insumo,"-"); //resta a bodega
                            if($id_destino==1){//alta de insumo
                                $id_insumo_dest=$this->ModeloCatalogos->Insert('insumos',array("insumo"=>$insumo,"existencia"=>$cant,"reg"=>$this->fechac));
                            }if($id_destino==2){ //alta de insumo en crepas3
                                log_message('error', "InsertC2 - id_destino: ".$id_destino);
                                log_message('error', "InsertC2 - id_origen: ".$id_origen);
                                $id_insumo_dest=$this->ModeloInsumos->InsertC2('insumos',array("insumo"=>$insumo,"existencia"=>$cant,"reg"=>$this->fechac));
                            }if($id_destino==3){ //alta de insumo en bodega
                                $id_insumo_dest=$this->ModeloInsumos->InsertC3('insumos',array("insumo"=>$insumo,"existencia"=>$cant,"reg"=>$this->fechac));
                            }
                        }else if($id_origen==1){ //alta de insumo
                            //$this->ModeloCatalogos->Insert('insumos',array("insumo"=>$insumo,"bodega"=>$cant,"de_bodega"=>1,"reg"=>$this->fechac));
                            $this->ModeloTraslados->updateStock($cant,$id_insumo,"-"); //resta a crepas 1
                            if($id_destino==2){//alta de insumo
                                //log_message('error', "InsertC2 - id_destino: ".$id_destino);
                                $id_insumo_dest=$this->ModeloInsumos->InsertC2('insumos',array("insumo"=>$insumo,"existencia"=>$cant,"reg"=>$this->fechac));
                            }if($id_destino==3){ //alta de insumo en crepas3
                                $id_insumo_dest=$this->ModeloInsumos->InsertC3('insumos',array("insumo"=>$insumo,"existencia"=>$cant,"reg"=>$this->fechac));
                            }if($id_destino==4){ //alta de insumo en bodega
                                $id_insumo_dest=$this->ModeloCatalogos->Insert('insumos',array("insumo"=>$insumo,"bodega"=>$cant,"de_bodega"=>1,"reg"=>$this->fechac));
                            }
                        }
                        $this->ModeloCatalogos->Insert('traslados_detalles',array("id_traslado"=>$id_traslado,"id_insumo"=>$id_insumo,"id_origen"=>$id_origen,"id_destino"=>$id_destino,"cant"=>$cant,"fecha_reg"=>$this->fechac,"id_insumo_destino"=>$id_insumo_dest));
                    }
                }else{
                    if($id_origen==2){
                        if($id_destino!=4){
                            $where=array("insumo"=>$insumo,"activo"=>1);
                        }if($id_destino==4){
                            $where=array("insumo"=>$insumo,"de_bodega"=>"1","activo"=>1);
                        }
                        //$get_ins=$this->ModeloInsumos->getselectwherenC2("insumos",$where);
                        if($id_destino==1){
                            $get_ins=$this->ModeloCatalogos->getselectwheren("insumos",$where);
                        }if($id_destino==3){
                            $get_ins=$this->ModeloInsumos->getselectwherenC3("insumos",$where);
                        }
                        if($get_ins->num_rows()>0){
                            $get_ins=$get_ins->row();
                            //actualiza existencia de insumo
                            $this->ModeloInsumos->updateStockC2($cant,$id_insumo,"-");

                            if($id_destino==1){//actualiza existencia de crepas1
                                $this->ModeloTraslados->updateStock($cant,$get_ins->insumosId,"+");
                            }if($id_destino==3){ //actualiza existenacia de crepas3
                                $this->ModeloInsumos->updateStockC3($cant,$get_ins->insumosId,"+");
                            }/*if($id_destino==4){ //actualiza existenacia de bodega
                                $this->ModeloTraslados->updateStockBodega($cant,$get_ins->insumosId,"+");
                            }*/
                            $this->ModeloInsumos->InsertC2('traslados_detalles',array("id_traslado"=>$id_traslado,"id_insumo"=>$id_insumo,"id_origen"=>$id_origen,"id_destino"=>$id_destino,"cant"=>$cant,"fecha_reg"=>$this->fechac,"id_insumo_destino"=>$get_ins->insumosId));

                            $this->ModeloCatalogos->Insert('traslados_detalles',array("id_traslado"=>$id_traslado,"id_insumo"=>$id_insumo,"id_origen"=>$id_origen,"id_destino"=>$id_destino,"cant"=>$cant,"fecha_reg"=>$this->fechac,"id_insumo_destino"=>$get_ins->insumosId));
                        }else{ //no existe insumo en destino
                            if($id_destino==1){//alta de insumo
                                $id_insumo_dest=$this->ModeloCatalogos->Insert('insumos',array("insumo"=>$insumo,"existencia"=>$cant,"reg"=>$this->fechac));
                            }if($id_destino==3){ //alta de insumo en crepas3
                                $id_insumo_dest=$this->ModeloInsumos->InsertC3('insumos',array("insumo"=>$insumo,"existencia"=>$cant,"reg"=>$this->fechac));
                            }/*if($id_destino==4){ //alta de insumo en bodega
                                $id_insumo_dest=$this->ModeloCatalogos->Insert('insumos',array("insumo"=>$insumo,"bodega"=>$cant,"de_bodega"=>1,"reg"=>$this->fechac));
                            }*/
                            $this->ModeloInsumos->InsertC2('traslados_detalles',array("id_traslado"=>$id_traslado,"id_insumo"=>$id_insumo,"id_origen"=>$id_origen,"id_destino"=>$id_destino,"cant"=>$cant,"fecha_reg"=>$this->fechac,"id_insumo_destino"=>$id_insumo_dest));

                            $this->ModeloCatalogos->Insert('traslados_detalles',array("id_traslado"=>$id_traslado,"id_insumo"=>$id_insumo,"id_origen"=>$id_origen,"id_destino"=>$id_destino,"cant"=>$cant,"fecha_reg"=>$this->fechac,"id_insumo_destino"=>$id_insumo_dest));
                        }
                    }else if($id_origen==3){
                        
                        //$get_ins=$this->ModeloInsumos->getselectwherenC3("insumos",array("insumo"=>$insumo,"de_bodega"=>"0","activo"=>1));
                        if($id_destino!=4){
                            $where=array("insumo"=>$insumo,"activo"=>1);
                        }if($id_destino==4){
                            $where=array("insumo"=>$insumo,"de_bodega"=>"1","activo"=>1);
                        }
                        //$get_ins=$this->ModeloInsumos->getselectwherenC3("insumos",$where);
                        if($id_destino==1){
                            $get_ins=$this->ModeloCatalogos->getselectwheren("insumos",$where);
                        }if($id_destino==2){
                            $get_ins=$this->ModeloInsumos->getselectwherenC2("insumos",$where);
                        }
                        if($get_ins->num_rows()>0){
                            $get_ins=$get_ins->row();
                            $this->ModeloInsumos->updateStockC3($cant,$id_insumo,"-");

                            if($id_destino==1){//actualiza existencia de crepas1
                                $this->ModeloTraslados->updateStock($cant,$get_ins->insumosId,"+");
                            }if($id_destino==2){ //actualiza existencia de crepas3
                                $this->ModeloInsumos->updateStockC2($cant,$get_ins->insumosId,"+");
                            }/*if($id_destino==4){ //actualiza existencia de bodega
                                $this->ModeloTraslados->updateStockBodega($cant,$get_ins->insumosId,"+");
                            }*/
                            $this->ModeloInsumos->InsertC3('traslados_detalles',array("id_traslado"=>$id_traslado,"id_insumo"=>$id_insumo,"id_origen"=>$id_origen,"id_destino"=>$id_destino,"cant"=>$cant,"fecha_reg"=>$this->fechac,"id_insumo_destino"=>$get_ins->insumosId));

                            $this->ModeloCatalogos->Insert('traslados_detalles',array("id_traslado"=>$id_traslado,"id_insumo"=>$id_insumo,"id_origen"=>$id_origen,"id_destino"=>$id_destino,"cant"=>$cant,"fecha_reg"=>$this->fechac,"id_insumo_destino"=>$get_ins->insumosId));
                        }else{
                            if($id_destino==1){//alta de insumo
                                $id_insumo_dest=$this->ModeloCatalogos->Insert('insumos',array("insumo"=>$insumo,"existencia"=>$cant,"reg"=>$this->fechac));
                            }if($id_destino==2){ //alta de insumo en crepas2
                                $id_insumo_dest=$this->ModeloInsumos->InsertC2('insumos',array("insumo"=>$insumo,"existencia"=>$cant,"reg"=>$this->fechac));
                            }/*if($id_destino==4){ //alta de insumo en bodega
                                $id_insumo_dest=$this->ModeloCatalogos->Insert('insumos',array("insumo"=>$insumo,"bodega"=>$cant,"de_bodega"=>1,"reg"=>$this->fechac));
                            }*/
                            $this->ModeloInsumos->InsertC3('traslados_detalles',array("id_traslado"=>$id_traslado,"id_insumo"=>$id_insumo,"id_origen"=>$id_origen,"id_destino"=>$id_destino,"cant"=>$cant,"fecha_reg"=>$this->fechac,"id_insumo_destino"=>$id_insumo_dest));

                            $this->ModeloCatalogos->Insert('traslados_detalles',array("id_traslado"=>$id_traslado,"id_insumo"=>$id_insumo,"id_origen"=>$id_origen,"id_destino"=>$id_destino,"cant"=>$cant,"fecha_reg"=>$this->fechac,"id_insumo_destino"=>$id_insumo_dest));
                        }
                    }
                }            
            }
        $this->db->trans_complete();
    }

    public function delete(){
        $id = $this->input->post('id');
        $this->db->trans_start();
            $this->ModeloCatalogos->updateCatalogo('traslados',array('activo'=>0),array('id'=>$id));
            $get_tras=$this->ModeloCatalogos->getselectwhereRow("traslados",array("id"=>$id));
            //$id_origen = $get_tras->id_origen;
            $get_det=$this->ModeloCatalogos->getselectwheren("traslados_detalles",array("id_traslado"=>$id));
            foreach ($get_det->result() as $d) { //traer detalles y regreserles el stock
                if($d->id_origen==1){
                    $this->ModeloTraslados->updateStock($d->cant,$d->id_insumo,"+");
                }else if($d->id_origen==4){ //actualiza existenacia de bodega
                    $this->ModeloTraslados->updateStockBodega($d->cant,$d->id_insumo,"+");
                }else if($d->id_origen==2){ //actualiza a crepas2
                    $this->ModeloInsumos->updateStockC2($d->cant,$d->id_insumo,"+");
                }else if($id_origen==3){ //actualiza a crepas3
                    $this->ModeloInsumos->updateStockC3($cant,$id_insumo,"+");
                } 

                if($d->id_destino==1){
                    $this->ModeloTraslados->updateStock($d->cant,$d->id_insumo_destino,"-");
                }else if($d->id_destino==4){ //actualiza existenacia de bodega
                    $this->ModeloTraslados->updateStockBodega($d->cant,$d->id_insumo_destino,"-");
                }else if($d->id_destino==2){ //actualiza a crepas2
                    $this->ModeloInsumos->updateStockC2($d->cant,$d->id_insumo_destino,"-");
                }else if($id_destino==3){ //actualiza a crepas3
                    $this->ModeloInsumos->updateStockC3($cant,$id_insumo_destino,"-");
                } 
            }
        $this->db->trans_complete();
    }
    
    public function getDataTraslado() {
        $params = $this->input->post();
        $getdata = $this->ModeloTraslados->getlistTraslado($params);
        $totaldata= $this->ModeloTraslados->getTotTraslado($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function detalleTraslados(){
        $id = $this->input->post('id');
        $result=$this->ModeloTraslados->get_detalleTras($id);
        $json_data = array("data" => $result);
        echo json_encode($result);
    }
    
}
