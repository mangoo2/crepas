<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Insumos_almacen extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('excel');
        $this->load->model('ModeloInsumos');
        $this->load->model('ModeloCatalogos');
    }
    public function index()
    {
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('insumosAlmacen/index');
        $this->load->view('templates/footer');
        $this->load->view('insumosAlmacen/indexjs');
    }

    public function insumosadd($id=0){
        $insumosId=0;
        $insumo='';
        $bodega='';
        $stockmin='';
        $operaciones='';
        $data['label']='Nuevo Insumo';
        $resultpro=$this->ModeloCatalogos->getselectwheren('insumos',array('insumosId'=>$id));
        foreach ($resultpro->result() as $item) {
            $insumosId=$item->insumosId;
            $insumo=$item->insumo;
            $bodega=$item->bodega;
            $stockmin=$item->stockmin;
            $data['label']='Editar Insumo';
        }

        $data['insumosId']=$insumosId;
        $data['insumo']=$insumo;
        $data['bodega']=$bodega;
        $data['stockmin']=$stockmin;

        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('insumosAlmacen/form',$data);
        $this->load->view('templates/footer');
        $this->load->view('insumosAlmacen/formjs');
    }
    
    function insadd(){
        $params=$this->input->post();
        $insumosId=$params['insumosId'];
        unset($params['insumosId']);
        //log_message('error','DATA Almacen: '.json_encode($params));
        if ($insumosId>0) {
            $this->ModeloCatalogos->updateCatalogo('insumos',$params,array('insumosId'=>$insumosId));
        }else{
            $params["de_bodega"]=1;
            $insumosId=$this->ModeloCatalogos->Insert('insumos',$params);
        }
    }

    public function deleteinsumos()
    {
        $id = $this->input->post('id');
        $this->ModeloCatalogos->updateCatalogo('insumos',array('activo_bodega'=>0),array('insumosId'=>$id));
    }

    public function getlistinsumo()
    {
        $params = $this->input->post();
        $getdata = $this->ModeloInsumos->get_result($params);
        $totaldata = $this->ModeloInsumos->total_result($params);
        $json_data = array(
            "draw"            => intval($params['draw']),
            "recordsTotal"    => intval($totaldata),
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           => $this->db->last_query()
        );
        echo json_encode($json_data);
    }

    function uploadExcel() {  
        $configUpload['upload_path'] = FCPATH.'fileExcel/';
        $configUpload['allowed_types'] = 'xls|xlsx|csv';
        $configUpload['max_size'] = '5000';
        $this->load->library('upload', $configUpload);
        $this->upload->do_upload('inputFile');  
        $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
        $file_name = $upload_data['file_name']; //uploded file name
        $extension=$upload_data['file_ext'];    // uploded file extension

        //$objReader =PHPExcel_IOFactory::createReader('Excel5');     //For excel 2003 
        //$objReader= PHPExcel_IOFactory::createReader('Excel2007'); // For excel 2007  
        $objReader= PHPExcel_IOFactory::createReader('CSV'); // For excel 2007  
        $objReader->setReadDataOnly(true);          

        $objPHPExcel=$objReader->load(FCPATH.'fileExcel/'.$file_name);      
        $totalrows=$objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel         
        $objWorksheet=$objPHPExcel->setActiveSheetIndex(0); 

        $array_insert=array();
        for($i=2;$i<=$totalrows;$i++){
            $insumosId= $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();           
            $insumo= $objWorksheet->getCellByColumnAndRow(1,$i)->getValue(); //Excel Column 1
            $existencia= $objWorksheet->getCellByColumnAndRow(2,$i)->getValue(); //Excel Column 2
            $stockmin=$objWorksheet->getCellByColumnAndRow(3,$i)->getValue(); //Excel Column 3
            $conteo=$objWorksheet->getCellByColumnAndRow(4,$i)->getValue(); //Excel Column 4
            $cant_compra=$objWorksheet->getCellByColumnAndRow(5,$i)->getValue(); //Excel Column 5
            $id_proveedor=$objWorksheet->getCellByColumnAndRow(6,$i)->getValue(); //Excel Column 6

            if(intval($cant_compra)>=0 && intval($id_proveedor>0)){
                $id_prov=$id_proveedor;
                $compra=$cant_compra;
                if($cant_compra==""){
                    $compra=0;
                }
                //$nva_cant=$existencia+$compra;
                $nva_cant=$conteo+$compra;
                //$this->ModeloInsumos->sumaStockBodega($nva_cant,$insumosId,$conteo);
                $this->ModeloInsumos->sumaStockBodega($nva_cant,$insumosId,$conteo,$stockmin);
                $arrayinsert = array(
                            'id_compra'=>0,
                            'id_producto'=>$insumosId,
                            'tipo_prod'=>0,
                            'cantidad'=>$cant_compra
                          );
                $array_insert[]=$arrayinsert;
            }
        }
        if(count($array_insert)>0){
            $id_compra=$this->ModeloCatalogos->Insert('compras',array("id_proveedor"=>$id_prov,"tipo"=>1,"reg"=>date("Y-m-d H:i:s")));
            for($j = 0 ; $j < sizeof($array_insert); $j++){
              $array_insert[$j]['id_compra'] = $id_compra;
            }
            //log_message('error', "array_insert: ".print_r($array_insert));
            $this->ModeloCatalogos->insert_batch('compra_detalle',$array_insert);
        } 
        unlink('fileExcel/'.$file_name); 
        $output = [];
        echo json_encode($output);
    }
    
    function exportExcel(){
        $data["bodega"]=1;
        $data["insumos"]=$this->ModeloCatalogos->getselectwheren('insumos',array('activo_bodega'=>1));
        //$data["insumos"]=$this->ModeloCatalogos->getselectwherenOrwhere2('insumos',array('activo_bodega'=>1,"de_bodega"=>1),"(de_bodega = 1 or bodega >= 0)");
        $this->load->view('insumosAlmacen/list_excel',$data);
    } 

    function exportStocks(){
        $data["bodega"]=1;
        $data["insumos"] = $this->ModeloInsumos->inferiorAStockMinimoBodega();
        $this->load->view('insumosAlmacen/list_stocks',$data);
    } 

}
