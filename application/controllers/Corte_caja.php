<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Corte_caja extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        //$this->load->model('Personal/ModeloPersonal');
        //$this->load->model('Usuarios/ModeloUsuarios');
        $this->load->model('ModeloVentas');
    }
	public function index(){
            //$data['personal']=$this->ModeloPersonal->getpersonal();
            //carga de vistas
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            //$this->load->view('Personal/Personal',$data);

            $this->load->view('corte/corte');
            $this->load->view('templates/footer');
            $this->load->view('corte/jscorte');

	}
    function corte(){
        $inicio = $this->input->post('fecha1');
        $fin = $this->input->post('fecha2');
        $tipo = $this->input->post('tipo');
        $id_sucursal = $this->input->post('id_sucursal');
        $resultadoc=$this->ModeloVentas->corte($inicio,$fin,$tipo,$id_sucursal);
        $resultadocs=$this->ModeloVentas->cortesum($inicio,$fin,$tipo,$id_sucursal);
        $productos=$this->ModeloVentas->corteProds($inicio,$fin,$tipo,$id_sucursal);
        $get_top=$this->ModeloVentas->corte_top($inicio,$fin,$tipo,$id_sucursal);
        $get_insumos=$this->ModeloVentas->corte_insumos($inicio,$fin,$tipo,$id_sucursal);

        $get_gasto=$this->ModeloVentas->get_tot_gasto($inicio,$fin,$tipo,$id_sucursal);

        $table="<table style='font-size:12px' class='table table-striped table-bordered table-hover' id='sample_2'>
                    <thead>
                        <tr><th style='text-align:center' colspan='11'>TABLA DE VENTAS</th></tr>
                        <tr>
                            <th>No. venta</th>
                            <th>Tipo</th>
                            <th>Cajero</th>
                            <th>Cliente</th>
                            <th>Fecha</th>
                            <th>Subtotal</th>
                            <th>Total</th>
                            <th>Efectivo</th>
                            <th>Tarjeta</th>
                            <th>Tipo Tarjeta</th>
                            <th>Plataforma</th>
                        </tr>
                    </thead>
                    <tbody>";
        $rowventas=0;
        $utilidad=0; $supertot=0;
        //log_message('error','ResultaDoc: '.json_encode($resultadoc->result()));
        //log_message('error','ResultaDocS: '.json_encode($resultadocs->result()));
        foreach ($resultadoc->result() as $fila) { 
            if($fila->tipo_costo==0)
                $tipoc="Mostrador";
            if($fila->tipo_costo==1)
                $tipoc="Uber";
            if($fila->tipo_costo==2)
                $tipoc="Rappi";
            if($fila->tipo_costo==3)
                $tipoc="Didi";

            if($fila->metodo==1&&$fila->pagotarjeta>0)
                $tipoT="N/A";
            elseif($fila->metodo==2&&$fila->pagotarjeta>0)
                $tipoT="Crédito";
            elseif($fila->metodo==3&&$fila->pagotarjeta>0)
                $tipoT="Débito";
            elseif($fila->metodo==4&&$fila->pagotarjeta>0)
                $tipoT="N/A";
            else
                $tipoT="N/A";

            $table .= "<tr>
                        <td>".$fila->id_venta."</td>
                        <td>".$tipoc."</td>
                        <td>".$fila->vendedor."</td>
                        <td>".$fila->Nom."</td>
                        <td>".$fila->reg."</td>
                        <td>$ ".number_format($fila->subtotal,2,'.',',')."</td>
                        <td>$ ".number_format($fila->monto_total,2,'.',',')."</td>";
                        if($fila->efectivo>0 && $fila->pagotarjeta>0){
                            $table .= "<td>$ ".number_format($fila->efectivo,2,'.',',')."</td>
                                <td>$ ".number_format($fila->pagotarjeta,2,'.',',')."</td>";
                        }else if($fila->efectivo>0 && $fila->pagotarjeta==0){
                            $table .= "<td>$ ".number_format($fila->monto_total,2,'.',',')."</td>
                                <td>$ ".number_format($fila->pagotarjeta,2,'.',',')."</td>";
                        }else if($fila->efectivo==0 && $fila->pagotarjeta>0){
                            $table .= "<td>$ ".number_format($fila->efectivo,2,'.',',')."</td>
                                <td>$ ".number_format($fila->pagotarjeta,2,'.',',')."</td>";
                        }
                        else if($fila->efectivo==0 && $fila->pagotarjeta==0){
                            $table .= "<td>$ ".number_format($fila->efectivo,2,'.',',')."</td>
                                <td>$ ".number_format($fila->pagotarjeta,2,'.',',')."</td>";
                        }
                        $table .= "<td>".$tipoT."</td>";
                        if($fila->metodo==4){
                            $table .= " <td>$ ".number_format($fila->monto_total,2,'.',',')."</td>";
                        }else{
                            $table .= " <td>$ 0.00</td>";
                        }
                        $table .= " </tr>";
                        $rowventas++;
                        if($fila->cantidad==0){ //producto de combo
                            $cantidad=$fila->cantidadc;
                            $precio=$fila->precioc;
                            $preciocompra=$fila->preciocomprac;
                            $preciocomp_uber=$fila->preciocomp_uberc;
                            $preciocomp_rappi=$fila->preciocomp_rappic;
                            $preciocomp_didi=$fila->preciocomp_didic;
                        }else{
                            $cantidad=$fila->cantidad;
                            $precio=$fila->precio;
                            $preciocompra=$fila->preciocompra;
                            $preciocomp_uber=$fila->preciocomp_uber;
                            $preciocomp_rappi=$fila->preciocomp_rappi;
                            $preciocomp_didi=$fila->preciocomp_didi;
                        }
                        if($fila->tipo_costo==0){   
                            $utilidad=$utilidad+($precio-$preciocompra)*$cantidad;
                        }else if($fila->tipo_costo==1){
                            $utilidad=$utilidad+($precio-$preciocomp_uber)*$cantidad;
                        }else if($fila->tipo_costo==2){
                            $utilidad=$utilidad+($precio-$preciocomp_rappi)*$cantidad;
                        }
                        else if($fila->tipo_costo==3){
                            $utilidad=$utilidad+($precio-$preciocomp_didi)*$cantidad;
                        }
        }
        $table.="</tbody> </table>";

        $table_prods="<table class='table table-striped table-bordered table-hover' id='table_prods'>
                    <thead>
                        <tr><th style='text-align:center' colspan='4'>TABLA DE PRODUCTOS VENDIDOS</th></tr>
                        <tr>
                            <th>No. venta</th>
                            <th>Cantidad</th>
                            <th>Producto</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>";
        
        foreach ($productos as $p) { 
            $aux=0;
            $codigo=$p->codigo;
            if($p->tipo==0){ //normal
                $cantidad=$p->cantidad;
                $precio=$p->precio;
                $nombre=$p->nombre;

                $table_prods.='<tr>
                    <td>'.$p->id_venta.'</td> 
                    <td>'.$cantidad.'</td>
                    <td>'.$nombre.'</td>
                    <td>$'.number_format($precio*$cantidad,2,".",",").'</td>
                </tr>';
            }else{
                $aux++;
                $getventasd2=$this->ModeloVentas->getventasd2($p->id_venta);
                foreach ($getventasd2->result() as $row){
                    $cantidad=$row->cantidad;
                    $precio=$row->precio;
                    $detcombo=$this->ModeloVentas->getventasdCombo($p->id_venta);
                    if($aux==1){
                        foreach ($detcombo->result() as $dc){
                            $nombre=$codigo.": <br>".$dc->det_combo;
                        }
                    }
                    $table_prods.='<tr>
                        <td>'.$p->id_venta.'</td> 
                        <td>'.$cantidad.'</td>
                        <td>'.$nombre.'</td>
                        <td>$'.number_format($precio*$cantidad,2,".",",").'</td>
                    </tr>';
                }
            }
        }
        $table_prods.="</tbody> </table>";

        $table_top="<table class='table table-striped table-bordered table-hover' id='table_tops'>
                    <thead>
                        <tr><th colspan='4' style='text-align:center'>TABLA DE MÁS VENDIDOS</th></tr>
                        <tr>
                            <th>Producto</th>
                            <th>Cantidad</th>
                        </tr>
                    </thead>
                    <tbody>";
        foreach ($get_top as $p) { 
            $cantidad=$p->cantidad;
            $nombre=$p->producto;

            $table_top.='<tr> 
                <td>'.$nombre.'</td>
                <td>'.$cantidad.'</td>
            </tr>';
        }
        $table_top.="</tbody> </table>";

        $table_insumos="<table class='table table-striped table-bordered table-hover' id='table_ins'>
                    <thead>
                        <tr><th colspan='4' style='text-align:center'>INSUMOS UTILIZADOS</th></tr>
                        <tr>
                            <th>No. Venta</th>
                            <th>Insumo</th>
                            <th>Cantidad</th>
                        </tr>
                    </thead>
                    <tbody>";
        foreach ($get_insumos as $p) { 
            $table_insumos.='<tr>
                <td>'.$p->id_venta.'</td> 
                <td>'.$p->insumo.'</td>
                <td>'.$p->cantidad.'</td>
            </tr>';
        }
        $table_insumos.="</tbody> </table>";

        $table2="<table class='table table-striped table-bordered table-hover' id='table_turnos'>
                    <thead>
                        <tr><th style='text-align:center' colspan='4'>TABLA DE TURNOS</th></tr>
                        <tr>
                            <th>Turno</th>
                            <th>Apertura</th>
                            <th>Cierre</th>
                            <th>Utilidad</th>
                        </tr>
                    </thead>
                    <tbody>";
        $respuestaturno=$this->ModeloVentas->consultarturnoname($inicio,$fin);
        $obedt=0;
        foreach ($respuestaturno->result() as $fila) { 
            if ($fila->fechacierre=='0000-00-00') {
                $horac =date('H:i:s');
                $fechac =date('Y-m-d');
            }else{
                $horac =$fila->horac;
                $fechac =$fila->fechacierre;
            }
            $fecha =$fila->fecha;
            $horaa =$fila->horaa; 
            $totalventas=$this->ModeloVentas->consultartotalturno($fecha,$horaa,$horac,$fechac);
            $totalpreciocompra=$this->ModeloVentas->consultartotalturno2($fecha,$horaa,$horac,$fechac);

            $obed =$totalventas-$totalpreciocompra;
            $table2 .= "<tr>
                            <td>".$fila->nombre."</td>
                            <td>".$fila->horaa."</td>
                            <td>".$fila->horac."</td>
                            <td>".number_format($obed,2,'.',',')."</td>
                        </tr>";
            $obedt=$obedt+$obed;
        }
        $table2.="</tbody> </table>";

        $total=0;
        $subtotal=0;
        $tGastos=0;
        $tEfectivo=0;
        $tCredito=0;
        $tDebito=0;
        $tUber=0;
        $tRappi=0;
        $tDidi_efect=0; $tDidi_plata=0;
        foreach ($resultadocs->result() as $fila) {
            $total = $fila->total;
            $subtotal = $fila->subtotal;
            $descuento = $fila->descuento;
            if($tipo==4){//todos
                $tEfectivo = round($fila->tEfectivo,2);
                $tCredito = round($fila->tCredito,2);
                $tDebito = round($fila->tDebito,2);
                $tUber = round($fila->tUber,2);
                $tRappi = round($fila->tRappi,2);
                $tDidi_efect = round($fila->tDidi_efect,2);
                $tDidi_plata = round($fila->tDidi_plata,2);
            }
            if($tipo==0){ //Mostrador
                $tEfectivo = round($fila->tEfectivo,2);
                $tCredito = round($fila->tCredito,2);
                $tDebito = round($fila->tDebito,2);
            }if($tipo==1){ //uber
                $tUber = round($fila->tUber,2);
            }if($tipo==2){ //rappi
                $tRappi = round($fila->tRappi,2);
            }if($tipo==3){ //didi
                $tDidi_efect = round($fila->tDidi_efect,2);
                $tDidi_plata = round($fila->tDidi_plata,2);
                /*$tEfectivo = round($fila->tEfectivo,2);
                $tCredito = round($fila->tCredito,2);
                $tDebito = round($fila->tDebito,2);*/
            }
        }

        foreach ($get_gasto as $g) {
            $tGastos = $g->tot_gastos;
        }
        $supertot=round($tEfectivo+$tCredito+$tDebito+$tUber+$tRappi+$tDidi_efect+$tDidi_plata,2)-round($tGastos,2);
        $supertot = number_format($supertot,2,'.',',');

        $total = round($total,2);
        $subtotal = round($subtotal,2);
        $total=number_format($total,2,'.',',');
        $utilidad=number_format($utilidad,2,'.',',');

        //$tGastos = round($total - $utilidad,2);

        $tEfectivo = number_format($tEfectivo,2,'.',',');
        $tCredito = number_format($tCredito,2,'.',',');
        $tDebito = number_format($tDebito,2,'.',',');
        $tUber = number_format($tUber,2,'.',',');
        $tRappi = number_format($tRappi,2,'.',',');
        $tDidi_efect = number_format($tDidi_efect,2,'.',',');
        $tDidi_plata = number_format($tDidi_plata,2,'.',',');

        $tGastos = number_format($tGastos,2,'.',',');
        //$obedt=number_format($obedt,2,'.',',');
        //$utilidad=number_format($utilidad,2,'.',',');
        $array = array("tabla"=>$table,
                        "tabla2"=>$table2,
                        "table_prods"=>$table_prods,
                        "table_top"=>$table_top,
                        "table_insumos"=>$table_insumos,
                        "dTotal"=>"".$supertot."",
                        "totalventas"=>$rowventas,
                        "dImpuestos"=>0,
                        "totalutilidad"=>"".$utilidad."",
                        "descuento"=>$descuento,
                        "dSubtotal"=>"".$subtotal."",

                        "tEfectivo"=>"".$tEfectivo."",
                        "tCredito"=>"".$tCredito."",
                        "tDebito"=>"".$tDebito."",
                        "tUber"=>"".$tUber."",
                        "tRappi"=>"".$tRappi."",
                        "tDidi_efect"=>"".$tDidi_efect."",
                        "tDidi_plata"=>"".$tDidi_plata."",

                        "tGastos"=>"".$tGastos."",
                    );
            echo json_encode($array);
    }
    

       
    
}
