<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Gastos extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloGastos');
        $this->load->model('ModeloCatalogos');
        $this->idusuario = $this->session->userdata('usuarioid_tz');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
    }

    public function index()
    {
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('gastos/index');
        $this->load->view('templates/footer');
        $this->load->view('gastos/indexjs');
    }

    public function gastoadd($id = 0)
    {
        $data['label'] = 'Alta';

        if ($id > 0) {
            $data['label'] = 'Edición';
            $data['g'] = $this->ModeloCatalogos->getselectwhereRow('gastos', array('id' => $id, 'activo' => 1));
            //log_message('error','G: '.json_encode($data['g']));
        }

        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('gastos/form', $data);
        $this->load->view('templates/footer');
        $this->load->view('gastos/formjs');
    }

    public function insert()
    {
        $datos = $this->input->post();
        $id = $datos['id'];
        unset($datos['id']);
        $id_reg = 0;
        //log_message('error', 'DATOSB: ' . json_encode($datos));

        if ($id > 0) {
            $this->ModeloCatalogos->updateCatalogo('gastos', $datos, array('id' => $id));
            $id_reg = $id;
        } else {
            $datos['modalFlag'] = 0;
            $datos['insumoId'] = 0;
            $datos['tipo'] = 0;
            $datos['usuarioId'] = $this->idusuario;
            $datos['reg'] = $this->fechahoy;
            $id_reg = $this->ModeloCatalogos->Insert('gastos', $datos);
        }
        echo $id_reg;
    }


    public function getlistado()
    {
        $params = $this->input->post();
        $getdata = $this->ModeloGastos->get_result($params);
        $totaldata = $this->ModeloGastos->total_result($params);
        $json_data = array(
            "draw"            => intval($params['draw']),
            "recordsTotal"    => intval($totaldata),
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           => $this->db->last_query()
        );
        echo json_encode($json_data);
    }

    public function search_insumo()
    {
        $search = $this->input->get('search');
        $results = $this->ModeloGastos->search_insumo($search);
        echo json_encode($results);
    }

    public function deletegasto()
    {
        $id = $this->input->post('id');
        $this->ModeloCatalogos->updateCatalogo('gastos', array('activo' => 0), array('id' => $id));
        /*
        $datos = $this->ModeloCatalogos->getselectwhereRow('gastos', array('id' => $id));

        log_message('error', 'DAtosDelete: ' . json_encode($datos));

        if ($datos->tipo > 0) {
            //Actualizar gasto
            $this->ModeloCatalogos->updateCatalogo('gastos', array('activo' => 0), array('id' => $id));
            //Descontar Stock   - operador - cantidad - id -
            $this->ModeloGastos->actualizarStockInsumo('+', abs($datos->cantidad), $datos->insumoId);
        } else {
            $this->ModeloCatalogos->updateCatalogo('gastos', array('activo' => 0), array('id' => $id));
        }
        */
    }
}
