var base_url = $('#base_url').val();
$(document).ready(function () {
    loadtable();
});

function loadtable(){
    table = $('#data-tables').DataTable({
        "bProcessing": true,
        "serverSide": true,
        destroy:true,
        "ajax": {
            "url": base_url+"Traslados/getDataTraslado",
            type: "post",
        },
        "columns": [
            {"data": "id"},
            {"data": null,
                render:function(data,type,row){
                    var html="";
                    if(row.id_origen==1)
                        html="Mr Crepé";
                    else if(row.id_origen==2)
                        html="Mr Crepé 2";
                    else if(row.id_origen==3)
                        html="Mr Crepé 3";
                    else if(row.id_origen==4)
                        html="Bodega";
                    return html;
                }
            }, 
            {"data": null,
                render:function(data,type,row){
                    var html="";
                    if(row.id_destino==1)
                        html="Mr Crepé";
                    else if(row.id_destino==2)
                        html="Mr Crepé 2";
                    else if(row.id_destino==3)
                        html="Mr Crepé 3";
                    else if(row.id_destino==4)
                        html="Bodega";
                    return html;
                }
            }, 
            {"data": "fecha"},
            {"data": null,
                render:function(data,type,row){
                    var html='<div class="btn-group mr-1 mb-1">\
                                  <button type="button" class="btn btn-raised btn-outline-warning"><i class="fa fa-cog"></i></button>\
                                  <button type="button" class="btn btn-raised btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
                                      <span class="sr-only">Toggle Dropdown</span>\
                                  </button>\
                                  <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(84px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">\
                                      <button class="dropdown-item" onclick="viewDet('+row.id+')">Detalles</button>\
                                      <a class="dropdown-item" onclick="insumodelete('+row.id+');"href="#">Eliminar</a>\
                                  </div>\
                              </div>';
                    return html;
                }
            },            
        ],
        "order": [[ 0, "desc" ]],
    });
}

function viewDet(id){
    $("#modaldet").modal("show");
    table2 = $('#table_det').DataTable({
        destroy:true,
        "ajax": {
            "url": base_url+"Traslados/detalleTraslados",
            "dataSrc": "",
            type: "POST",
            data:{id:id},
        },
        "columns": [
            {"data": "insumo"},
            {"data": "cant"},
            {"data": "id_origen",
                render:function(data,type,row){
                    var html="";
                    if(row.id_origen==1){
                        html="Crepé 1";
                    }else if(row.id_origen==2){
                        html="Crepé 2";
                    }else if(row.id_origen==3){
                        html="Crepé 3";
                    }else if(row.id_origen==4){
                        html="Bodega";
                    }
                    return html;
                }
            },
            {"data": "id_destino",
                render:function(data,type,row){
                    var html="";
                    if(row.id_destino==1){
                        html="Crepé 1";
                    }else if(row.id_destino==2){
                        html="Crepé 2";
                    }else if(row.id_destino==3){
                        html="Crepé 3";
                    }else if(row.id_destino==4){
                        html="Bodega";
                    }
                    return html;
                }
            },
        ],
        order: [
            [0, "desc"]
        ],
    });
}

function insumodelete(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar este Insumo? El cambio será irreversible',
        type: 'red',
        typeAnimated: true,
        buttons: {
            confirmar: function(){
                    $.ajax({
                        type:'POST',
                        url: base_url+'Traslados/delete',
                        data: {id:id},
                        async: false,
                        statusCode:{
                            404: function(data){
                                toastr.error('Error!', 'No Se encuentra el archivo');
                            },
                            500: function(){
                                toastr.error('Error', '500');
                            }
                        },
                        success:function(data){
                            toastr.success('Hecho!', 'Eliminado Correctamente');
                            loadtable();   
                        }
                    });
                
            },
            cancelar: function (){
            }
        }
    });
}