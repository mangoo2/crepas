var base_url = $('#base_url').val();
var table_combo='<table class="table table-hover table-responsive" id="productos_combo">\
                    <thead>\
                        <tr >\
                            <th style="text-align:center;" colspan="6">DETALLES DE COMBO</th>\
                        </tr>\
                        <tr>\
                            <th>Clave</th>\
                            <th>Cantidad</th>\
                            <th>Producto</th>\
                            <th>Precio</th>\
                            <th>Total</th>\
                            <th></th>\
                        </tr>\
                    </thead>\
                    <tbody id="class_productos_combo"></tbody>\
                </table>';
$(document).ready(function() {	

	$('#vcliente').select2({
		width: 'resolve',
		minimumInputLength: 3,
		minimumResultsForSearch: 10,
		placeholder: 'Buscar un cliente',
	  	ajax: {
	    	url: base_url+'Ventas/searchcli',
	    	dataType: "json",
	    	data: function (params) {
	      	var query = {
	        	search: params.term,
	        	type: 'public'
	      	}
	      	return query;
	    },
	    processResults: function(data){
	    	var clientes=data;
	    	var itemscli = [];
	    	data.forEach(function(element) {
                itemscli.push({
                    id: element.ClientesId,
                    text: element.Nom
                });
            });
            return {
                results: itemscli
            };	    	
	    },  
	  }
	});

	$('#vproducto').select2({
		width: 'resolve',
		minimumInputLength: 3,
		minimumResultsForSearch: 10,
		placeholder: 'Buscar un Producto',
	  	ajax: {
	    	url: base_url+'Ventas/searchproducto',
	    	dataType: "json",
	    	data: function (params) {
	      	var query = {
	        	search: params.term,
	        	type: 'public'
	      	}
	      	return query;
	    },
	    processResults: function(data){
	    	var clientes=data;
	    	var itemscli = [];
	    	data.forEach(function(element) {
                itemscli.push({
                    id: element.productoid,
                    text: element.codigo+' / '+element.nombre,
                    tipo: element.tipo
                });
            });
            return {
                results: itemscli
            };	    	
	    },  
	  }
	}).on('select2:select', function (e) {
	    var data = e.params.data;
	    //console.log(data);
	    addproducto(data.tipo); //manda el tipo 0=normal, 1 = combo
	});
	$('#vproducto').select2('open').on('focus');
	$('#ingresaventa').click(function(event) {
            addventas();
            
    });
    $('#btnabrirt').click(function(){
			$.ajax({
				type:'POST',
				url:'Ventas/abrirturno',
				data:{
					cantidad: $('#cantidadt').val(),
					nombre: $('#nombredelturno').val()
				},
				async:false,
				success:function(data){
					toastr.success('Turno Abierto','Hecho!');
					$('#modalturno').modal('hide');
					
				}
			});
		
	});
	$( "#vingreso" ).keypress(function(e) {
	  	if(e.which == 13) {
       		addventas();
    	}
	});
	$( "#vingresot" ).keypress(function(e) {
	  	if(e.which == 13) {
       		addventas();
    	}
	});

	$("#mpago").on("change",function (){
		if($("#mpago").val() == 1){
			$("#vingresot").attr("disabled",true);
			$("#vingreso").attr("disabled",false);
		}

		if($("#mpago").val() == 2 || $("#mpago").val() == 3){
			$("#vingresot").attr("disabled",false);
			$("#vingreso").attr("disabled",true);
		}

		if($("#mpago").val() == 4){
			$("#vingresot").attr("disabled",true);
			$("#vingreso").attr("disabled",true);
		}
	});

	$("#tipo_costo").on("change",function (){
		$("#tipo_costo").attr("disabled",true);
		if($("#tipo_costo").val()!=0){
			$("#mpago").val(4);
			$("#mpago").attr("disabled",true);
			$("#mdescuento").attr("disabled",true);
			$("#vingreso").attr("disabled",true);
			$("#vingresot").attr("disabled",true);
			$("#mpago option[value='4']").attr("disabled", false);
		}else{
			$("#mpago").val(1);
			$("#mpago").attr("disabled",false);
			$("#mdescuento").attr("disabled",false);
			$("#vingreso").attr("disabled",false);
			$("#vingresot").attr("disabled",false);
		}
		if($("#tipo_costo").val()==3){
			$("#mpago").val(1);
			$("#mpago").attr("disabled",false);
			$("#vingreso").attr("disabled",false);
			$("#vingresot").attr("disabled",false);
			$("#mpago option[value='2']").attr("disabled", true);
			$("#mpago option[value='3']").attr("disabled", true);
			$("#mpago option[value='4']").attr("disabled", false);
		}
	});

	$("#vingreso").on("change",function (){
		calculartotal();
	});
	$("#vingresot").on("change",function (){
		calculartotal();
	});

	$('#s_insumoG').select2({
		width: '100%',
		minimumInputLength: 3,
		minimumResultsForSearch: 10,
		placeholder: 'Buscar un Insumo',
		dropdownParent: $('#modal_gastos'),
		ajax: {
				url: base_url + 'Ventas/search_insumo',
				dataType: "json",
				data: function(params) {
						var query = {
								search: params.term,
								type: 'public'
						}
						return query;
				},
				processResults: function(data) {
						var itemsCli = [];
						data.forEach(function(element) {
								//console.log("element -> " + element);
								itemsCli.push({
										id: element.insumosId,
										text: element.insumo
								});
						});
						return {
								results: itemsCli
						};
				},
		}
	}).on('select2:select', function(e) {
			var data = e.params.data;
	});

	typeSelect();
	$("#tipo_select").on("change",function(){
		typeSelect();
	})
});

function cambioProducto(aux,cont,id_subp){
	//console.log("id_subp: "+id_subp);
	var nvo_cod = $(".prod_sel"+aux+"_"+cont+" option:selected").data("cod");
	$("#cod_prod"+aux+"_"+cont).html(nvo_cod); 
	var cant_tot = $("#cant_prod"+aux+"_"+cont).val();
	var cant_cuentap = 0; var band_cta=0; var cont_tabla=0;
	var TABLAp = $("#productos_combo tbody > .de_producto_"+cont);
	TABLAp.each(function(){    
		cont_tabla++;     
        if($(this).find("select[class*='prod_sel"+cont_tabla+"_"+cont+"'] option:selected").val()!=""){ //!= vacio y misma clase aux y cont
        	cant_cuentap++;
        }
        if(cant_cuentap==cant_tot){
    		band_cta=1;
    	}
    });
    //console.log("cant_cuentap: "+cant_cuentap);
    //console.log("band_cta: "+band_cta); 

	var cant_cuenta = 0; var cont_tabla2=0;
	var TABLA = $("#productos_combo tbody > .de_producto_"+cont);
	TABLA.each(function(){  
		cont_tabla2++;       
        if($(this).find("select[class*='prod_sel"+cont_tabla2+"_"+cont+"'] option:selected").val()!=""){
        	cant_cuenta++;
        }
        if(cant_cuenta==cant_tot && $(this).find("select[class*='prod_sel"+cont_tabla2+"_"+cont+"'] option:selected").val()=="" || band_cta==1 && $(this).find("select[class*='prod_sel"+cont_tabla2+"_"+cont+"'] option:selected").val()==""){
    		$(this).find("select[class*='prod_sel"+cont_tabla2+"_"+cont+"']").attr("disabled",true);
    	}else if(cant_cuenta<cant_tot && $(this).find("select[class*='prod_sel"+cont_tabla2+"_"+cont+"'] option:selected").val()==""){
    		$(this).find("select[class*='prod_sel"+cont_tabla2+"_"+cont+"']").attr("disabled",false);
    	}
        //console.log("nombre_prod: "+$(this).find("select[id*='nombre_prod'] option:selected").val());    
    });  

    /*$.ajax({
        type:'POST',
        url: base_url+'Ventas/prodComboSelect',
        data: {
            id_producto: id_subp
        },
        success:function(data){
        	//console.log(data);
        }
    }); */ 
    //console.log("cant_cuenta: "+cant_cuenta);   
}
var count_combo=0;
function addproducto(tipo){ //recibe tipo de prod, 0=normal, 1 = combo
	//console.log("tipo: "+tipo);
	if(tipo==0){
		if ($('#vcantidad').val()>0) {
			$.ajax({
		        type:'POST',
		        url: base_url+'Ventas/addproducto',
		        data: {
		            cant: $('#vcantidad').val(),
		            prod: $('#vproducto').val(),
		            tipo: $('#tipo_costo').val()
	            },
	            async: false,
	            statusCode:{
	                404: function(data){
	                    toastr.error('Error!', 'No Se encuentra el archivo');
	                },
	                500: function(){
	                    toastr.error('Error', '500');
	                }
	            },
	            success:function(data){
	            	//console.log(data);
	                $('#class_productos').html(data);
	                $("#tipo_costo").attr("disabled",true);
	            }
	        });
			$('#vcantidad').val(1);
			$('#vproducto').html('');
		    $("#vproducto").val(0).change();
		    $('#vproducto').select2('open').on('focus');
		}
	}else{
		if ($('#vcantidad').val()>0) {
			//$('#vcantidad').val(1);
			$.ajax({
		        type:'POST',
		        url: base_url+'Ventas/addproductoCombo',
		        data: {
		            cant: $('#vcantidad').val(),
		            prod: $('#vproducto').val(),
		            tipo: $('#tipo_costo').val(),
		            count_combo: count_combo
		            },
		            async: false,
		            statusCode:{
		                404: function(data){
		                    toastr.error('Error!', 'No Se encuentra el archivo');
		                },
		                500: function(){
		                    toastr.error('Error', '500');
		                }
		            },
		            success:function(data){
		            	//console.log(data);
		                $('#productos_combo').append(data);
		                $("#tipo_costo").attr("disabled",true);
		                count_combo++;
		            }
		        });
			$('#vcantidad').val(1);
			$('#vproducto').html('');
		    $("#vproducto").val(0).change();
		    $('#vproducto').select2('open').on('focus');
		}

	}
    calculartotal();
}
function calculartotal(){
	var addtp = 0;
	$(".vstotal").each(function() {
        var vstotal = $(this).val();
        addtp += Number(vstotal);
    });
    //var descuento1=100/$('#mdescuento').val();
    $('#vsbtotal').val(addtp);
    var descuento= parseFloat($('#mdescuento').val())/100;
    //console.log("descuento %: "+descuento);
    var cantdescuento = addtp*descuento;
    $('#cantdescuento').val(cantdescuento);
    //console.log("cantdescuento: "+cantdescuento);
    var total=parseFloat(addtp)-parseFloat(cantdescuento);
    $('#vtotal').val(Number(total).toFixed(2));
    ingreso();
}
function ingreso(){
	var ingresoe= $('#vingreso').val();
	var ingresot= $('#vingresot').val();
	if(ingresoe==""){
		ingresoe=0;
	}
	if(ingresot==""){
		ingresot=0;
	}
	var ingreso=parseFloat(ingresoe)+parseFloat(ingresot);
	var totals=$('#vtotal').val();

	if(ingreso>0)
		var cambio = parseFloat(ingreso)-parseFloat(totals);
	//if (cambio<0) {
	//	cambio=0;
	//}
	//console.log("ingreso: "+ingreso);
	//console.log("cambio: "+cambio);
	$('#vcambio').val(cambio);
}
function limpiar(){
	$('#class_productos').html('');
	$.ajax({
        type:'POST',
        url: base_url+'Ventas/productoclear',
        async: false,
        statusCode:{
            404: function(data){
            	toastr.error('Error!', 'No Se encuentra el archivo');
        	},
        	500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            	
        }
	});
}
function deletepro(id,tipo){
	$.ajax({
        type:'POST',
        url: base_url+'Ventas/deleteproducto',
        data: {
            idd: id
        },
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
        	$('.producto_'+id).remove();
        	if(tipo==1){
        		$('.de_producto_'+id).remove();
        	}
        	calculartotal();
        }
    });
}
function addventas(){
	//if (/*$('#vcambio').val()>=0 || */parseFloat($("#vingreso").val())!=0 || parseFloat($("#vingresot").val()!=0)) {
	var TABLACT = $("#productos_combo tbody > #producto_tabla");
	var con_ele=0; var cont_tot_prod=0; var cont_prod=0;
	TABLACT.each(function(){   
        cont_prod = parseInt($(this).find("input[id*='cant_prod']").val());
        cont_tot_prod = cont_tot_prod + cont_prod;
        //console.log("cont_prod en tabla: "+cont_prod);
    });
    var TABLACT2 = $("#productos_combo tbody > #de_producto");
	var con_ele=0;  
	TABLACT2.each(function(){      
        if($(this).find("select[id*='nombre_prod'] option:selected").val()!=""){ //!= vacio y misma clase aux y cont
        	con_ele++;
        }
    });
    //console.log("con_ele: "+con_ele);
    //console.log("cont_tot_prod: "+cont_tot_prod);
    if($("#productosv tbody > tr").length==0 && $("#productos_combo tbody > tr").length==0){
		toastr.error('Error', 'Agregue al menos un Producto');
		return;
	}
    if(con_ele==cont_tot_prod){
    	if($("#tipo_costo option:selected").val()==3 && $("#mpago option:selected").val()==1 && Number($("#vingreso").val())==0){
    		toastr.error('Error', 'Ingrese un monto en efectivo del pago');
    		return;
    	}
		if (parseFloat($('#vcambio').val())>=0 && $("#tipo_costo").val()==0 || $("#tipo_costo").val()!=0 /*||
			$('#vcambio').val()=="" && $("#tipo_costo").val()!=4*/) {
			/*console.log("vcambio: "+$('#vcambio').val());
			console.log("vingreso: "+$('#vingreso').val());
			console.log("vingresot: "+$('#vingresot').val());
			console.log("tipo_costo: "+$('#tipo_costo').val());*/

			var vcambio = $('#vcambio').val();
			var efec = $('#vingreso').val();
			var tar = $('#vingresot').val();
			if(efec==""){
				efec=0;
			}
			if(tar==""){
				tar=0;
			}
			var vpagacon = parseFloat(efec)+parseFloat(tar);
			var vingresot =$('#vingresot').val()==''?0:$('#vingresot').val();
			var vtotal = $('#vtotal').val();
			var metodo = $('#mpago').val();
			//var efectivo = parseFloat(vtotal)-parseFloat(vingresot);
			var efectivo = $('#vingreso').val();
			if($('#mpago').val()==4){
				//vingresot = parseFloat(vtotal);
			}

	        $.ajax({
	            type:'POST',
	            url: base_url+'Ventas/ingresarventa',
	            data: {
	            	uss: $('#ssessius').val(),
	                cli: $('#vcliente').val(),
	                tipo_costo:$('#tipo_costo').val(),
	                mpago: $('#mpago').val(),
	                desc: $('#mdescuento').val(),
	                descu: $('#cantdescuento').val(),
	                sbtotal:$('#vsbtotal').val(),
	                total: vtotal,
	                efectivo: efectivo,
	                tarjeta: vingresot
	            },
	            async: false,
	            statusCode:{
	                404: function(data){
	                    toastr.error('Error!', 'No Se encuentra el archivo');
	                },
	                500: function(){
	                    toastr.error('Error', '500');
	                }
	            },
	            success:function(data){
	            	var idventa=data;
	            	var band_combo=0;
	            	if($("#productos_combo tbody > tr").length>0){
		            	var DATA2  = [];
	    				var TABLA2   = $("#productos_combo tbody > tr"); //falta guardar los combos 
	    				TABLA2.each(function(){         
			                item2 = {};
			                item2 ["idventa"] = idventa;
			                item2 ["producto"] = $(this).find("input[id*='vsproid']").val();
			                item2 ["cantidad"] = $(this).find("input[id*='vscanti']").val();
			                item2 ["precio"] = $(this).find("input[id*='vsprecio']").val();
			                item2 ["tipo_prod"] = $(this).find("input[id*='tipo_prod']").val();
			                item2 ["de_prod"] = $(this).find("input[id*='de_prod']").val();
			                if($(this).find("input[id*='de_prod']").val()==1){
			                	item2 ["id_prod_combo"] = $(this).find("select[id*='nombre_prod'] option:selected").val();
			                }else{
			                	item2 ["id_prod_combo"] = 0;
			                }
			                DATA2.push(item2);
			            });
	    				INFO2 = new FormData();
	    				aInfo2 = JSON.stringify(DATA2);
	    				INFO2.append('data', aInfo2);
	    				//console.log("aInfo2: "+aInfo2);

			            $.ajax({
			                data: INFO2,
			                type: 'POST',
			                url : base_url+'Ventas/ingresarventaproCombo',
			                processData: false, 
			                contentType: false,
			                async: false,
		                    statusCode:{
		                        404: function(data){
		                            toastr.error('Error!', 'No Se encuentra el archivo');
		                        },
		                        500: function(){
		                            toastr.error('Error', '500');
		                        }
		                    },
			                success: function(data){
			                	$('#productos_combo').html('');
			                	$('#productos_combo').html(table_combo);
			                	band_combo=1;
			                }
			            });
			        }
			        if($("#productos_combo tbody > tr").length>0){
    					if(band_combo==1){
    						saveProds(idventa);
    					}else{
    						setTimeout(function(){ 
    							saveProds(idventa);
				            }, 1500);	
    					}
    				}else{
    					saveProds(idventa);
    				}
	            	
		            setTimeout(function(){ 
		                limpiar(); 
		                checkprint = document.getElementById("checkimprimir").checked;
						if (checkprint==true) {
							$("#iframeri").modal();
							//$('#iframereporte').html('<iframe src="Visorpdf?filex=Ticket&iden=id&id='+idventa+'"></iframe>');	
							$('#iframereporte').html('<iframe src="'+base_url+'Ticket?id='+idventa+'&vcambio='+vcambio+'&vpagacon='+vpagacon+'&metodo='+metodo+'"></iframe>');		
						}else{
							toastr.success( 'Venta Realizada','Hecho!');
						}
		            }, 2500);
		            
				    $("#vcliente").val(45).change();
				    $('#vtotal').val('');
				    $('#vcambio').val(0);
				    $('#vingreso').val('');
				    $('#vingresot').val('');
				    $("#tipo_costo").attr("disabled",false);
				    $('#tipo_costo').val(0);
				    $("#mpago").attr("disabled",false);
					$("#mdescuento").attr("disabled",false);
					$("#vingreso").attr("disabled",false);
					$("#vingresot").attr("disabled",false);
					$('#mpago').val(1);
					$("#mpago option[value='2']").attr("disabled", false);
					$("#mpago option[value='3']").attr("disabled", false);
					$("#mpago option[value='4']").attr("disabled", true);
	            }
	        });
	    }else{
	    	toastr.error('No se puede realizar la venta debido a que no ha ingresado el saldo para liquidar la venta','Error!');
	    }
	}else{
		toastr.error('Seleccione productos del combo a vender','Error!');
	}
}

function saveProds(idventa){
	var DATA  = [];
	var TABLA   = $("#productosv tbody > tr");
	TABLA.each(function(){         
        item = {};
        item ["idventa"] = idventa;
        item ["producto"] = $(this).find("input[id*='vsproid']").val();
        item ["cantidad"] = $(this).find("input[id*='vscanti']").val();
        item ["precio"] = $(this).find("input[id*='vsprecio']").val();
        item ["tipo_prod"] = $(this).find("input[id*='tipo_prod']").val();
        item ["de_prod"] = $(this).find("input[id*='de_prod']").val();
        if($(this).find("input[id*='de_prod']").val()==1){
        	item ["id_prod_combo"] = $(this).find("select[id*='nombre_prod'] option:selected").val();
        }else{
        	item ["id_prod_combo"] = 0;
        }
        DATA.push(item);
    });
	INFO  = new FormData();
	aInfo   = JSON.stringify(DATA);
	INFO.append('data', aInfo);
	//console.log("aInfo: "+aInfo);
    $.ajax({
        data: INFO,
        type: 'POST',
        url : base_url+'Ventas/ingresarventapro',
        processData: false, 
        contentType: false,
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success: function(data){

        }
    });
}

function modal_gastos(){
	$('#modal_gastos').modal("show");
}

function typeSelect(){
	//
	if($('#tipo_select').is(':checked')){
		//console.log('YES checked');
		$('#lb_insumo').css('display','block');
		$('#lb_motivo').css('display','none');
	}else{
		//console.log('NOT checked');
		$('#lb_insumo').css('display','none');
		$('#lb_motivo').css('display','block');
	} 
}

function add_gasto(){
	//console.log("addGasto");
	tipoG = $('#tipo_select').is(':checked');//True-Insumo   False-Motivo
	insumoG = $('#s_insumoG option:selected').val();
	motivoG = $('#motivoG').val();
	cantidadG = $('#cantidadG').val();
	montoG = $('#montoG').val();
	tipoG = tipoG ? '1' : '0';
/*
	console.log("tipoG: "+tipoG);
	console.log("insumoG: "+insumoG);
	console.log("motivoG: "+motivoG);
	console.log("cantidadG: "+cantidadG);
	console.log("montoG: "+montoG);
*/
	if(cantidadG == '' || montoG == ''){
		toastr.error('Los campos no pueden estar vacios','Error!');
		return;
	}

	if(cantidadG <= 0 || montoG <= 0){
		toastr.error('Revise sus datos','Error!');
		return;
	}
	
	if(tipoG == 1 && insumoG == 0){
		toastr.error('Seleccione un insumo','Error!');
		return;
	}

	if(tipoG == 0 && motivoG == ''){
		toastr.error('Motivo no puede estar vacio','Error!');
		return;
	}

	$.ajax({
		data:{
			tipo: tipoG,
			insumoId: insumoG,
			motivo: motivoG,
			cantidad: cantidadG,
			monto: montoG,
		},
		type: 'POST',
		url : base_url+'Ventas/ingresargasto',
		async: false,
		statusCode:{
			404: function(data){
				toastr.error('Error!', 'No Se encuentra el archivo');
			},
			500: function(){
				toastr.error('Error', '500');
			}
		},
		success: function(data){
			insumoG = $('#s_insumoG').val('0').trigger('change');
			motivoG = $('#motivoG').val('');
			cantidadG = $('#cantidadG').val('');
			montoG = $('#montoG').val('');
			$('#modal_gastos').modal("hide");
			toastr.success('Gasto registrado','Exito!');
		}
	});

}