$(document).ready(function(){
	$('#chkFecha').change(function(){
		if(document.getElementById("chkFecha").checked){
			var hoy = new Date();
	        var dd = hoy.getDate();
	        var mm = hoy.getMonth()+1;
	        var yyyy = hoy.getFullYear();
	        
	        dd = addZero(dd);
	        mm = addZero(mm);
			fecha = yyyy+'-'+mm+'-'+dd;
			$('#txtInicio').val(fecha);
			$('#txtFin').val(fecha);
			$('#txtInicio').attr('disabled','disabled');
			$('#txtFin').attr('disabled','disabled');	
		}
		else{
			$('#txtInicio').val('');
			$('#txtFin').val('');
			$('#txtInicio').removeAttr('disabled');
			$('#txtFin').removeAttr('disabled');
		}
	});
	$('#btnBuscar').click(function(){
		params = {};
		params.fecha1 = $('#txtInicio').val();
		params.fecha2 = $('#txtFin').val();
		params.tipo = $('#tipo_costo option:selected').val();
		params.id_sucursal = $('#id_sucursal option:selected').val();
		if($('#id_sucursal option:selected').val()==0){
			toastr.error('Elije una sucursal', 'Error');
			return;
		}
		if(params.fecha1 != '' && params.fecha2 != ''){
			$.ajax({
				type:'POST',
				url:'Corte_caja/corte',
				data:params,
				async:false,
				success:function(data){
					//console.log(data);
					var array = $.parseJSON(data);
					$('#tEfectivo').html(array.tEfectivo);
					$('#tCredito').html(array.tCredito);
					$('#tDebito').html(array.tDebito);
					$('#tUber').html(array.tUber);
					$('#tRappi').html(array.tRappi);
					$('#tDidiefe').html(array.tDidi_efect);
					$('#tDidipla').html(array.tDidi_plata);
					$('#tGastos').html(array.tGastos);

					$('#tbCorte').html(array.tabla);
					$('#tbCorte2').html(array.tabla2);

					$('#tbProds').html(array.table_prods);
					$('#tbTop').html(array.table_top);
					$('#tbInsumos').html(array.table_insumos);

					$('#rowventas').html(array.totalventas);//Ventas
					$('#totalutilidades').html(array.totalutilidad);//Utilidad
					/*$('#cRealizadas').html(array.cRealizadas);
					$('#cContado').html(array.cContado);
					$('#cCredito').html(array.cCredito);
					$('#cTerminal').html(array.cTerminal);
					$('#cCheque').html(array.cCheque);
					$('#cTransferencia').html(array.cTransferencia);
					$('#cPagos').html(array.cPagos);
					$('#dContado').html('$ '+array.dContado);
					$('#dCredito').html('$ '+array.dCredito);
					$('#dTerminal').html('$ '+array.dTerminal);
					$('#dCheque').html('$ '+array.dCheque);
					$('#dTransferencia').html('$ '+array.dTransferencia);
					$('#dPagos').html('$ '+array.dPagos);*/
					$('#dTotal').html(''+array.dTotal);//TOTAL
                    //$('#dImpuestos').html(''+array.dImpuestos);
                    $('#dSubtotal').html(''+array.dSubtotal);
                    $('#sample_2').DataTable({dom: 'Bfrtip', responsive:true,
                                buttons: [
                                        //{extend: 'excel',title: 'Reporte de saldos'},
                                        {extend: 'excel'},
                                        {extend: 'pdf'},
                                        {extend:'print'},
                                        'pageLength'
                                ],
                                "order": [[ 0, "desc" ]] });
                    $('#table_turnos').DataTable({dom: 'Bfrtip', responsive:true,
                                buttons: [
                                        {extend: 'excel'},
                                        {extend: 'pdf'},
                                        {extend:'print'},
                                        'pageLength'
                                ],
                                "order": [[ 0, "desc" ]] });

                    $('#table_prods').DataTable({dom: 'Bfrtip', responsive:true,
                                buttons: [
                                        {extend: 'excel'},
                                        {extend: 'pdf'},
                                        {extend:'print'},
                                        'pageLength'
                                ],
                                "order": [[ 0, "desc" ]] });
                    $('#table_tops').DataTable({dom: 'Bfrtip', responsive:true,
                                buttons: [
                                        {extend: 'excel'},
                                        {extend: 'pdf'},
                                        {extend:'print'},
                                ],
                                "order": [[ 1, "desc" ]] });
                   	$('#table_ins').DataTable({dom: 'Bfrtip', responsive:true,
                                buttons: [
                                        {extend: 'excel'},
                                        {extend: 'pdf'},
                                        {extend:'print'},
                                        'pageLength'
                                ],
                                "order": [[ 0, "desc" ]] });
				}
			});
		}
		else{
			toastr.error('No existen fechas validas', 'Error');
			
		}
	});
});

function addZero(i) {
    if (i < 10) {
        i = '0' + i;
    }
    return i;
}