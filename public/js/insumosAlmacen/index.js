var base_url = $('#base_url').val();
var table;

$(document).ready(function () {
	table = $('#data-tables').DataTable();
	loadtable();

	$("#upload_excel").on("click",function(){
        //console.log("click upload");
        $("#modalCarga").modal("show");
        $("#inputFile").fileinput({
            showCaption: false,
            showUpload: false,// quita el boton de upload
            //rtl: true,
            allowedFileExtensions: ["csv","xls","xlsx"],
            browseLabel: 'Seleccionar Archivo',
            uploadUrl: base_url+'Insumos_almacen/uploadExcel',
            maxFilePreviewSize: 5000,
            previewFileIconSettings: {
                'csv': '<i class="fa fa-file-excel-o text-success"></i>',
                'xls': '<i class="fa fa-file-excel-o text-success"></i>',
                'xlsx': '<i class="fa fa-file-excel-o text-success"></i>'
            },
            /*uploadExtraData: function (previewId, index) {
                var info = {
                        tipo_malla:$('#tipo_malla option:selected').val()
                        };
                return info;
            }*/
        }).on('filebatchuploadcomplete', function(event, files, extra) {
            //$("#modalCarga").modal("hide");
            
            swal("Éxito", "Carga de insumo realizado correctamente", "success");
            $('#inputFile').fileinput('clear');
            loadtable();
            unblockUI();
        });
    });

    $("#aceptar_carga").on("click",function(){
        if($("#inputFile").val()!=""){
            $('#inputFile').fileinput('upload');
            $("#modalCarga").modal("hide");
            blockUI();
        }else{
            swal("Error", "Elija un archivo a cargar", "warning");
        }
    });

});

function blockUI(){
    $.blockUI({
        message:"<h2>Importando datos ...</h2>",
        css: { 
        border: 'none', 
        padding: '15px', 
        backgroundColor: '#000', 
        '-webkit-border-radius': '10px', 
        '-moz-border-radius': '10px',
        opacity: .5, 
        color: '#fff',
        baseZ: 5000
    } });
}

function unblockUI(){
    $.unblockUI();
}

function loadtable() {
	table.destroy();
	table = $('#data-tables').DataTable({
		stateSave: true,
		responsive: !0,
		"bProcessing": true,
		"serverSide": true,
		"ajax": {
			"url": base_url + "Insumos_almacen/getlistinsumo",
			type: "post",
		},
		"columns": [{
				"data": "insumosId"
			},
			{
				"data": "insumo"
			},
			{
				"data": "bodega"
			},
			{
				"data": "stockmin"
			},
			{
				"data": null,
				render: function (row) {
					var html = '<div class="btn-group mr-1 mb-1">\
                                    <button type="button" class="btn btn-raised btn-outline-warning"><i class="fa fa-cog"></i></button>\
                                    <button type="button" class="btn btn-raised btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
                                        <span class="sr-only">Toggle Dropdown</span>\
                                    </button>\
                                    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(84px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">\
																			<a class="dropdown-item" href="' + base_url + 'Insumos_almacen/insumosadd/' + row.insumosId + '">Editar</a>\
																			<a class="dropdown-item" onclick="insumodelete(' + row.insumosId + ');"href="#">Eliminar</a>\
                                    </div>\
                                </div>';
					return html;
				}
			},
		],
		"order": [
			[0, "desc"]
		],
		"lengthMenu": [
			[25, 50, 100],
			[25, 50, 100]
		],
	});
}

function insumodelete(id) {
	$.confirm({
		boxWidth: '30%',
		useBootstrap: false,
		icon: 'fa fa-warning',
		title: 'Atención!',
		content: '¿Está seguro de Eliminar este Insumo?',
		type: 'red',
		typeAnimated: true,
		buttons: {
			confirmar: function () {
				$.ajax({
					type: 'POST',
					url: base_url + 'Insumos_almacen/deleteinsumos',
					data: {
						id: id
					},
					async: false,
					statusCode: {
						404: function (data) {
							toastr.error('Error!', 'No Se encuentra el archivo');
						},
						500: function () {
							toastr.error('Error', '500');
						}
					},
					success: function (data) {
						toastr.success('Hecho!', 'eliminado Correctamente');
						loadtable();
					}
				});
			},
			cancelar: function () {

			}
		}
	});
}
