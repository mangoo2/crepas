var base_url = $('#base_url').val();

$(document).ready(function () {
	loadtable();
	//console.log('JS');
});

function loadtable() {
	tabla = $('#data-tables').DataTable({
		stateSave: true,
		responsive: !0,
		"bProcessing": true,
		"serverSide": true,
		"ajax": {
			"url": base_url + "Gastos/getlistado",
			type: "post",
		},
		"columns": [{
				"data": "id"
			},
			{
				"data": null,
				render: function (row) {
					var html = '';
					if (row.tipo == 0) {
						html = row.motivo;
					} else {
						html = row.insumo;
					}
					return html;
				}
			},
			{
				"data": "cantidad"
			},
			{
				"data": "monto"
			},
			{
				"data": "personal"
			},
			
			{
				"data": "reg"
			},
			{
				"data": null,
				render: function (row) {
					//console.log(row);
					/*if(row.modalFlag > 0){
						var html = '<div class="btn-group mr-1 mb-1">\
													<button type="button" class="btn btn-raised btn-outline-warning disabled"><i class="fa fa-cog"></i></button>\
													<button type="button" class="btn btn-raised btn-warning dropdown-toggle disabled" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
															<span class="sr-only">Toggle Dropdown</span>\
													</button>\
											</div>';
					}else{*/
						var html = '<div class="btn-group mr-1 mb-1">\
													<button type="button" class="btn btn-raised btn-outline-warning "><i class="fa fa-cog"></i></button>\
													<button type="button" class="btn btn-raised btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
															<span class="sr-only">Toggle Dropdown</span>\
													</button>\
													<div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(84px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">\
														<a class="dropdown-item" href="' + base_url + 'Gastos/gastoadd/' + row.id + '">Editar</a>\
														<a class="dropdown-item" onclick="gastodelete(' + row.id + ');"href="#">Eliminar</a>\
													</div>\
											</div>';
					/*}*/
					
					return html;
				}
			},
		],
		"order": [
			[0, "desc"]
		],
		"lengthMenu": [
			[25, 50, 100],
			[25, 50, 100]
		],
	});
}

function gastodelete(id) {
	$.confirm({
		boxWidth: '30%',
		useBootstrap: false,
		icon: 'fa fa-warning',
		title: 'Atención!',
		content: '¿Está seguro de Eliminar este Gasto?',
		type: 'red',
		typeAnimated: true,
		buttons: {
			confirmar: function () {
				$.ajax({
					type: 'POST',
					url: base_url + 'Gastos/deletegasto',
					data: {
						id: id
					},
					async: false,
					statusCode: {
						404: function (data) {
							toastr.error('Error!', 'No Se encuentra el archivo');
						},
						500: function () {
							toastr.error('Error', '500');
						}
					},
					success: function (data) {
						toastr.success('Hecho!', 'eliminado Correctamente');
						tabla.ajax.reload();
					}
				});
			},
			cancelar: function () {

			}
		}
	});
}
