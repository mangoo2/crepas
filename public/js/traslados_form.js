var base_url=$("#base_url").val();
$(document).ready(function() {

	$("#cantidad").keypress(function(e) {
	  	if(e.which == 13) {
	  		return false;
    	}
	});

	$('#id_insumo').select2({
		width: 'resolve',
		minimumInputLength: 2,
		minimumResultsForSearch: 10,
		placeholder: 'Buscar un Insumo',
	  	ajax: {
	    	url: base_url+'Traslados/searchInsumo',
	    	dataType: "json",
	    	data: function (params) {
	      	var query = {
	        	search: params.term,
	        	type: 'public',
	        	id_origen:$("#id_origen option:selected").val(),
	      	}
	      	return query;
	    },
	    processResults: function(data){
	    	var clientes=data;
	    	var itemscli = [];
	    	data.forEach(function(element) {
                itemscli.push({
                    id: element.insumosId,
                    text: element.nom_insumo,
                    precio: element.preciocompra,
                    insumoId: element.insumosId,
                    stock:element.existencia,
                    bodega:element.bodega
                });
            });
            return {
                results: itemscli
            };	    	
	    },  
	}
	}).on('select2:select', function (e) {
	    var data = e.params.data;
	    if($('#id_origen option:selected').val()=="1" || $('#id_origen option:selected').val()=="2" || $('#id_origen option:selected').val()=="3"){
	    	$('#id_insumo option:selected').attr("data-stock",data.stock);
	    }if($('#id_origen option:selected').val()=="4"){
	    	$('#id_insumo option:selected').attr("data-stock",data.bodega);
	    }
	});

	$("#id_origen,#id_destino").on("change",function(){
		if($("#id_origen option:selected").val()==$("#id_destino option:selected").val()){
			swal("Álerta", "Origen y Destino no pueden ser el mismo", "warning");
			$("#id_destino").val("0");
			return;
		}
	});

	$('#save_traslado').click(function(event) { 
		if($("#productos tbody > tr").length==0){
			swal("Álerta", "Debe elegir al menos un producto al traslado", "warning");
			return;
		}
        $.ajax({
            type:'POST',
            url: base_url+'Traslados/save_traslado',
            data: $("#form_tras").serialize(),
            async: false,
            beforeSend: function(){
                $("#save_traslado").attr("disabled",true);
            },
            success:function(data){
            	var id_traslado=data;
            	var DATA  = [];
    			var TABLA   = $("#productos tbody > tr");
				TABLA.each(function(){         
	                item = {};
	                item ["id_traslado"] = id_traslado;
	                item ["id_insumo"]   = $(this).find("input[id*='idproducto']").val();
	                item ["insumo"]   = $(this).find("input[id*='insumo']").val();
	                item ["id_origen"]   = $(this).find("input[id*='idorigen']").val();
	                item ["id_destino"]  = $(this).find("input[id*='iddestino']").val();
	                item ["cant"]  = $(this).find("input[id*='cant']").val();
	                DATA.push(item);
	            });
				INFO  = new FormData();
				aInfo   = JSON.stringify(DATA);
				INFO.append('data', aInfo);
	            $.ajax({
	                data: INFO,
	                type: 'POST',
	                url : base_url+'Traslados/ingresarDetalles',
	                processData: false, 
	                contentType: false,
	                success: function(data){
	                	swal("Exito", "Traslado guardado correctamente", "success");
	                	setTimeout(function(){ 
							location.href = base_url+'Traslados';
			            }, 1500);
	                }
	            });
            }
        });
	});

});

function addproducto(){
	var id=$('#id_insumo option:selected').val();
	var id_origen=$('#id_origen option:selected').val();
	var id_destino=$('#id_destino option:selected').val();
	var repetido=0;
    var TABLA   = $("#productos tbody > tr");
    TABLA.each(function(){         
        var prodexi = $(this).find("input[id*='idinsumo']").val(); 
        if (id == prodexi){
            repetido=1;
        }   
    });
    if(repetido==1){
    	swal("Álerta", "Insumo agregado previamente", "warning");
    	return;
    }
    if($("#id_origen option:selected").val()==0 || $("#id_destino option:selected").val()==0){
    	swal("Álerta", "Elige un almacén origen y destino", "warning");
    	return;
    }
		if($("#id_origen option:selected").val()==2 && $("#id_destino option:selected").val()==4){
    	swal("Álerta", "Esta sucursal no se permite enviar a bodega", "warning");
    	return;
    }
		if($("#id_origen option:selected").val()==3 && $("#id_destino option:selected").val()==4){
    	swal("Álerta", "Esta sucursal no se permite enviar a bodega", "warning");
    	return;
    }
    if(Number($("#id_insumo option:selected").data("stock"))<Number($('#cantidad').val())){
    	swal("Álerta", "Cantidad mayor a la de stock", "warning");
    	return;
    }

	if (Number($('#cantidad').val())>0) {
		var producto=$('#id_insumo option:selected').text();
        var producto='<tr class="producto_'+id+'">\
        <td><input type="hidden" name="idproducto" id="idproducto" value="'+id+'">\
        <input type="hidden" name="idorigen" id="idorigen" value="'+id_origen+'">\
        <input type="hidden" name="iddestino" id="iddestino" value="'+id_destino+'">\
        <input type="hidden" name="insumo" id="insumo" value="'+producto+'">'+producto+'\
        </td>\
        <td><input type="number" name="cant" id="cant" value="'+$('#cantidad').val()+'" readonly style="background: transparent;border: 0px;width: 100px;">\
        <td><a class="danger" data-original-title="Eliminar" title="Eliminar" onclick="deletepro('+id+')"><i class="ft-trash font-medium-3"></i></a></td></tr>';
	            
        $('#class_productos').append(producto);     
		$('#cantidad').val('');
	    $("#id_insumo").val(0).change();
	}
}

function deletepro(id){
	$('.producto_'+id).remove();
}