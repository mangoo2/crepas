/*-----------------02-01-2024-------------------*/
ALTER TABLE `insumos` ADD `bodega` FLOAT NOT NULL AFTER `stockmin`;
CREATE TABLE `mrcrepe`.`gastos` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `insumoId` INT(11) NOT NULL , `motivo` VARCHAR(300) NOT NULL , `cantidad` FLOAT NOT NULL , `monto` FLOAT NOT NULL , `tipo` TINYINT(1) NOT NULL , `usuarioId` INT(11) NOT NULL , `reg` TIMESTAMP NOT NULL , `activo` TINYINT(1) NOT NULL DEFAULT '1' , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `gastos` CHANGE `tipo` `tipo` TINYINT(1) NOT NULL COMMENT '1 = Insumo, 0 = motivo';
ALTER TABLE mrcrepe.compras DROP FOREIGN KEY fk_compra_proveedor;
ALTER TABLE `compras` ADD `tipo` INT(1) NOT NULL DEFAULT '1' COMMENT '1 = compra, 2 = gasto ' AFTER `monto_total`;
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`) VALUES (NULL, '1', 'InsumosAlmacen', 'Insumos_almacen', 'fa fa-truck');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '15');
INSERT INTO `personal_menu` (`personalmenuId`, `personalId`, `MenuId`) VALUES (NULL, '1', '15');

INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`) VALUES (NULL, '2', 'Gastos', 'Gastos', 'fa fa-cogs');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '16');
INSERT INTO `personal_menu` (`personalmenuId`, `personalId`, `MenuId`) VALUES (NULL, '1', '16');

ALTER TABLE `gastos` CHANGE `reg` `reg` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `gastos` ADD `modalFlag` TINYINT(1) NOT NULL AFTER `usuarioId`;

/* ********************17-01-24***************** */
ALTER TABLE `insumos` ADD `conteo_fisico` FLOAT NOT NULL AFTER `existencia`;
ALTER TABLE `insumos` ADD `conteo_fisco_bod` FLOAT NOT NULL AFTER `bodega`;
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`) VALUES (NULL, '2', 'Traslados', 'Traslados', 'fa fa-balance-scale');
INSERT INTO `personal_menu` (`personalmenuId`, `personalId`, `MenuId`) VALUES (NULL, '1', '17');
ALTER TABLE `insumos` ADD `de_bodega` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no,1=si' AFTER `bodega`;

/* agregar tabla traslados y traslados_detalles */
DROP TABLE IF EXISTS `traslados`;
CREATE TABLE IF NOT EXISTS `traslados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_origen` int(11) NOT NULL COMMENT '1=crepas,2=crepas2,3=crepas3,4=bodega',
  `id_destino` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `reg` datetime NOT NULL,
  `estatus` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
COMMIT;

DROP TABLE IF EXISTS `traslados_detalles`;
CREATE TABLE IF NOT EXISTS `traslados_detalles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_traslado` int(11) NOT NULL,
  `id_insumo` int(11) NOT NULL,
  `id_origen` int(11) NOT NULL,
  `id_destino` int(11) NOT NULL,
  `cant` float NOT NULL,
  `fecha_reg` datetime NOT NULL,
  `estatus` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

ALTER TABLE `traslados_detalles` ADD `id_insumo_destino` INT NOT NULL AFTER `id_destino`;

ALTER TABLE `insumos` ADD `activo_bodega` VARCHAR(1) NOT NULL DEFAULT '1' COMMENT '0=no, 1=activo' AFTER `activo`;